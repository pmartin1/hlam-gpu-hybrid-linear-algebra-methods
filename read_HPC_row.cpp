//@HEADER
// ************************************************************************
//
//                     HLAM: Hybrid Linear Algebra Methods
//                 Copyright (c) 2022, Pedro J. Martinez-Ferrer
//               HPCCG: Simple Conjugate Gradient Benchmark Code
//                 Copyright (2006) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// BSD 3-Clause License
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)
//
// Questions? Contact Michael A. Heroux (maherou@sandia.gov)
//
// ************************************************************************
//@HEADER


/////////////////////////////////////////////////////////////////////////

// Routine to read a sparse matrix, right hand side, initial guess,
// and exact solution (as computed by a direct solver).

/////////////////////////////////////////////////////////////////////////

// nrow - number of rows of matrix (on this processor)

#include <iostream>
#include <string>
using std::cout;
using std::cerr;
using std::endl;
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include "read_HPC_row.hpp"

#include "PedroParameters.hpp"
#include "PedroFunctions.hpp"
#include <sys/mman.h>

void read_HPC_row(char * data_file, char * ext, intT ntasksUser,  intT nx, intT ny, intT nz, HPC_Sparse_Matrix ** A,floatT ** x, floatT ** b, floatT ** xexact)

{

#ifdef DEBUG
  intT debug = 0;
#else
  intT debug = 0;
#endif

  intT init = 0; //initial values
  FILE *in_file ;
  FILE *in_file2;
  FILE *in_file3;
  FILE *in_file4;

  intT i,j;
  intT total_nrow;
  intT total_nnz;
  intT local_nnz_upper;
  intT local_nnz_lower;

  intT r;
  intT * rp = &r;
  intT rw;
  intT * rwp = &rw;
  intT c;
  intT * cp = &c;
  int8_t o;
  int8_t * op = &o;
  floatT v;
  floatT * vp = &v;

  floatT d;
  floatT * dp = &d;

  intT ru;
  intT * rup = &ru;
  intT rl;
  intT * rlp = &rl;
  intT cu;
  intT * cup = &cu;
  intT cl;
  intT * clp = &cl;
  floatT vu;
  floatT * vup = &vu;
  floatT vl;
  floatT * vlp = &vl;


 std::string extension;
 extension=ext;

 char * data_file2= data_file;
 char * data_file3= data_file;
 char * data_file4= data_file;


std::string str1=data_file;
if(extension=="csr")
{
 str1 +=".csr";
}

else
{
cout << "Error: wrong file extension " << endl;
exit(1);
}

 std::string str2=data_file2;
 str2 += ".b";

 std::string str3=data_file3;
 str3 += ".ex";

 std::string str4=data_file4;
 str4 += ".doff";

 in_file  = fopen(str1.c_str(), "r");
 in_file2 = fopen(str2.c_str(), "r");
 in_file3 = fopen(str3.c_str(), "r");
 in_file4 = fopen(str4.c_str(), "r");


  if (in_file == NULL)
    {
      printf("Error: Cannot open file: %s\n",data_file);
      exit(1);
    }
   if (in_file2 == NULL)
    {
      printf("Error: There is no file for b vector: %s\n",data_file2);
      exit(1);
    }
   if (in_file4 == NULL)
    {
      printf("Error: There is no file for doff vector: %s\n",data_file4);
      exit(1);
    }    
   if (in_file3 == NULL)
    {
      printf("There is no file for exact values, it is assumed that xexact=1: %s\n",data_file3);
    }

  printf("Reading matrix info from %s...\n",data_file);

  fscanf(in_file,"%ld",&total_nrow);
  fscanf(in_file,"%ld",&total_nnz);
  int total_nIndex=total_nrow+1;
  int true_local_nnz = total_nnz;

  local_nnz_upper=(total_nnz-total_nrow)/2;
  local_nnz_lower=(total_nnz-total_nrow)/2;




#ifdef USING_MPI
  int  dummy;
  intT size , rank; // Number of MPI processes, My process ID

  MPI_Comm_size(MPI_COMM_WORLD, & dummy); size = dummy;
  MPI_Comm_rank(MPI_COMM_WORLD, & dummy); rank = dummy;
#else
  intT size = 1; // Serial case (not using MPI)
  intT rank = 0;
#endif
  intT n = total_nrow;
  intT chunksize = n/size;
  intT remainder = n%size;

  intT mp = chunksize;
  if (rank<remainder) mp++;
  intT local_nrow = mp;

  intT off = rank*(chunksize+iOne);
  if (rank>remainder) off -= (rank - remainder);
  intT start_row = off;
  intT stop_row = off + mp -iOne;

  floatT * xexact0 = MMAPALLOC(local_nrow, floatT, thp); *xexact = MMAPALIGN(xexact0, floatT, thp);
  floatT * x0      = MMAPALLOC(local_nrow, floatT, thp); *x      = MMAPALIGN(x0     , floatT, thp);
  floatT * b0      = MMAPALLOC(local_nrow, floatT, thp); *b      = MMAPALIGN(b0     , floatT, thp);

  // Find nnz for this processor
//  intT local_nnz     = iZero;
  intT cur_local_row = iZero;
  intT pointerNumber = iZero;

  intT local_nnz=total_nnz;

  // CSR
  intT   * rowIndex0      = MMAPALLOC(total_nIndex   , intT  , thp); intT   * rowIndex      = MMAPALIGN(rowIndex0     , intT  , thp);
  intT   * list_of_cols0  = MMAPALLOC(local_nnz      , intT  , thp); intT   * list_of_cols  = MMAPALIGN(list_of_cols0 , intT  , thp);
  floatT * list_of_vals0  = MMAPALLOC(local_nnz      , floatT, thp); floatT * list_of_vals  = MMAPALIGN(list_of_vals0 , floatT, thp);
  int8_t * list_of_dOff0  = MMAPALLOC(local_nnz      , int8_t, thp); int8_t * list_of_dOff  = MMAPALIGN(list_of_dOff0 , int8_t, thp);
/*
  // COO
  intT   * list_of_rows0  = MMAPALLOC(local_nnz      , intT  , thp); intT   * list_of_rows  = MMAPALIGN(list_of_rows0 , intT  , thp);

  // MSR
  intT   * rowIndexM0      = MMAPALLOC(total_nIndex   , intT  , thp); intT   * rowIndexM      = MMAPALIGN(rowIndexM0     , intT  , thp);
  intT   * list_of_colsM0  = MMAPALLOC(local_nnz      , intT  , thp); intT   * list_of_colsM  = MMAPALIGN(list_of_colsM0 , intT  , thp);
  floatT * list_of_valsM0  = MMAPALLOC(local_nnz      , floatT, thp); floatT * list_of_valsM  = MMAPALIGN(list_of_valsM0 , floatT, thp);

  // LDUCSR
  intT   * rowIndex_l0     = MMAPALLOC(total_nIndex   , intT  , thp); intT   * rowIndex_l     = MMAPALIGN(rowIndex_l0    , intT  , thp);
  intT   * list_of_cols_l0 = MMAPALLOC(local_nnz_lower, intT  , thp); intT   * list_of_cols_l = MMAPALIGN(list_of_cols_l0, intT  , thp);
  floatT * list_of_vals_u0 = MMAPALLOC(local_nnz_upper, floatT, thp); floatT * list_of_vals_u = MMAPALIGN(list_of_vals_u0, floatT, thp);

  intT   * rowIndex_u0     = MMAPALLOC(total_nIndex   , intT  , thp); intT   * rowIndex_u     = MMAPALIGN(rowIndex_u0    , intT  , thp);
  intT   * list_of_cols_u0 = MMAPALLOC(local_nnz_upper, intT  , thp); intT   * list_of_cols_u = MMAPALIGN(list_of_cols_u0, intT  , thp);
  floatT * list_of_vals_l0 = MMAPALLOC(local_nnz_lower, floatT, thp); floatT * list_of_vals_l = MMAPALIGN(list_of_vals_l0, floatT, thp);

  // Diagonal
  floatT * list_of_diags0  = MMAPALLOC(local_nrow     , floatT, thp); floatT * list_of_diags  = MMAPALIGN(list_of_diags0 , floatT, thp);
*/
  // CSR READING
  if(extension=="csr")
  {
  for (i=iZero; i<total_nIndex; i++)
    {
      fscanf(in_file, "%ld",rp);
      if (debug) printf("%ld\n",r);
        {
         rowIndex[i] = r;
        }
    }


  cur_local_row = iZero;

        for (i=iZero; i<total_nnz; i++)
         {
          fscanf(in_file, "%lf %ld",vp,cp);
          if (debug)    printf("%lf %ld\n",v,c);
          list_of_cols[i]=c;
          list_of_vals[i]=v;
         }
       for (i=iZero; i<local_nrow; i++)
         {
          fscanf(in_file4,"%hhd",op);
          if (debug) printf("%hhd\n",o);
          list_of_dOff[i] = o;
         }      
 cout <<"Format: "  <<extension << endl;
 }

 else
  {
   cout << " wrong SP format" << endl;
   exit(1);
  }

 // RIGHT-HAND SIDE and EXACT VALUES READING
  cur_local_row = iZero;
  floatT xt, bt, xxt;

  if (in_file3 == NULL) {

  for (i=iZero; i<total_nrow; i++)
    {
          fscanf(in_file2, "%lf\n",&bt);
          (*x)[cur_local_row] = init;
          (*b)[cur_local_row] = bt;
          (*xexact)[cur_local_row] = 1;
          cur_local_row++;
    }

  }
  else
  {

   for (i=iZero; i<total_nrow; i++)
    {
          fscanf(in_file2, "%lf\n",&bt);
          fscanf(in_file3, "%lf\n",&xxt);
          (*x)[cur_local_row] = init;
          (*b)[cur_local_row] = bt;
          (*xexact)[cur_local_row] = xxt;
          cur_local_row++;
    }
   fclose(in_file3);
  }


  fclose(in_file);
  fclose(in_file2);

  if (debug) cout << "Process "<<rank<<" of "<<size<<" has "<<local_nrow;

  if (debug) cout << " rows. Global rows "<< start_row
                  <<" through "<< stop_row <<endl;

  if (debug) cout << "Process "<<rank<<" of "<<size
                  <<" has "<<local_nnz<<" nonzeros."<<endl;

  *A = new HPC_Sparse_Matrix; // Allocate matrix struct and fill it

  (*A)->title = 0;
  (*A)->start_row = start_row ;
  (*A)->stop_row = stop_row;
  (*A)->total_nrow = total_nrow;
  (*A)->total_nnz = total_nnz;
  (*A)->true_local_nnz = true_local_nnz;
  (*A)->local_nrow = local_nrow;
  (*A)->local_ncol = local_nrow;
  (*A)->local_nnz = local_nnz;
  // (*A)->nnz_in_row = nnz_in_row;
  (*A)->ntasksUser = ntasksUser;
//  (*A)->rowIndexM   = rowIndexM;
//  (*A)->list_of_valsM = list_of_valsM;
//  (*A)->list_of_colsM = list_of_colsM;
  (*A)->rowIndex   = rowIndex;
  (*A)->list_of_vals = list_of_vals;
  (*A)->list_of_cols = list_of_cols;
//  (*A)->list_of_rows = list_of_rows;
//  (*A)->list_of_diags = list_of_diags;
  (*A)->list_of_dOff = list_of_dOff;
//  (*A)->rowIndex_l  = rowIndex_l;
//  (*A)->rowIndex_u  = rowIndex_u;
//  (*A)->local_nnz_lower =local_nnz_lower;
//  (*A)->local_nnz_upper =local_nnz_upper;
//  (*A)->list_of_vals_l = list_of_vals_l;
//  (*A)->list_of_cols_l = list_of_cols_l;
//  (*A)->list_of_vals_u = list_of_vals_u;
//  (*A)->list_of_cols_u = list_of_cols_u;
  (*A)->num_send_neighbors = iZero;
  (*A)->ratio 		   =  7;		// maximum value of nnz per row is defined

  if (debug) cout << " Read function is returned "<<endl;
  return;
}
