//@HEADER
// ************************************************************************
//
//                     HLAM: Hybrid Linear Algebra Methods
//                 Copyright (c) 2022, Pedro J. Martinez-Ferrer
//               HPCCG: Simple Conjugate Gradient Benchmark Code
//                 Copyright (2006) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// BSD 3-Clause License
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)
//
// Questions? Contact Michael A. Heroux (maherou@sandia.gov)
//
// ************************************************************************
//@HEADER


/////////////////////////////////////////////////////////////////////////

// Routine to compute an approximate solution to Ax = b where:

// A - known matrix stored as an HPC_Sparse_Matrix struct

// b - known right hand side vector

// x - On entry is initial guess, on exit new approximate solution

// max_iter - Maximum number of iterations to perform, even if
//            tolerance is not met.

// tolerance - Stop and assert convergence if norm of residual is <=
//             to tolerance.

// niters - On output, the number of iterations actually performed.

/////////////////////////////////////////////////////////////////////////

#include <sys/mman.h>
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
#include <cmath>

#include "mytimer.hpp"
#include "HPCCG.hpp"

#ifdef USING_CUDA
#include <cuda_runtime.h>
#include <cusparse.h>         // cusparseSpMV
#include <cublas_v2.h>
#include "scalarAndVectorOp.hpp"
#endif

#if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
#define TICK( )
#define TOCK(t)
#else
#define TICK( ) t0  = mytimer()      // Use TICK and TOCK to time a code section
#define TOCK(t) t  += mytimer() - t0
#endif


int HPCCG
(
  #ifdef USING_CUDA
  cusparseHandle_t & handle_csp, cublasHandle_t & handle_cbl, cudaStream_t * __restrict__ const streamArr,
  cusparseSpMatDescr_t & matA, floatT * d_x,  floatT * d_b, intT * d_elements_to_send, floatT * d_send_buffer,
  #endif
  const HPC_Sparse_Matrix * __restrict__ const A,
  floatT * __restrict__ const b, floatT * __restrict__ const x,
  const intT max_iter, const double tolerance,
  intT & niters, double & normr, double * __restrict__ const times
)
{
  #ifndef USING_MKL
  void * Amkl, * descr; // Dummy arguments
  #endif

  const double t_begin = mytimer(); // Start timing after MKL

  const intT MPItag = 99           ,
             nrow   = A->local_nrow,
             ncol   = A->local_ncol;

  #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
  const intT rowBs  = splitInterval(nrow, A->ntasksUser, simdSize),
             ntasks = CEILDIV(nrow, rowBs);

  const intT ** __restrict__ const depMVidx  = (const intT **) A->depMVidxPtr ,
             ** __restrict__ const depMVsize = (const intT **) A->depMVsizePtr;
  const intT *  __restrict__ const depMVite  = A->depMVite;
  #else
  const intT rowBs = nrow, ntasks = iOne;
  #endif

  floatT t0 = zero, t1 = zero, t2 = zero, t3 = zero, t4 = zero;

  #ifdef USING_MPI
  floatT t5 = zero;
  #endif

  floatT * __restrict__ const Ap0 = MMAPALLOC(nrow, floatT, thp), // Has "nrow" length
         * __restrict__ const p0  = MMAPALLOC(ncol, floatT, thp); //     "ncol"

  floatT * __restrict__ const Ap  = MMAPALIGN(Ap0 , floatT, thp),
         * __restrict__ const p   = MMAPALIGN(p0  , floatT, thp);

  #ifdef USING_MPI
  int  dummy;
  intT size , rank;

  MPI_Comm_size(MPI_COMM_WORLD, & dummy); size = dummy;
  MPI_Comm_rank(MPI_COMM_WORLD, & dummy); rank = dummy;
  

    #if defined(USING_NCCL)
    ncclUniqueId nccl_uid;
    if (rank == 0) NCCLCHECK(ncclGetUniqueId(&nccl_uid));
    MPI_Bcast(&nccl_uid, sizeof(nccl_uid), MPI_BYTE, 0, MPI_COMM_WORLD);
  
    ncclUniqueId nccl_uidD[iTwo];
    for(intT i = iZero; i < iTwo; ++i) {
    if (rank == 0) NCCLCHECK(ncclGetUniqueId(&nccl_uidD[i]));
    MPI_Bcast(&nccl_uidD[i], sizeof(nccl_uidD[i]), MPI_BYTE, 0, MPI_COMM_WORLD);
    }

    ncclUniqueId nccl_uidN[iTwo];
    for(intT i = iZero; i < iTwo; ++i) {
    if (rank == 0) NCCLCHECK(ncclGetUniqueId(&nccl_uidN[i]));
    MPI_Bcast(&nccl_uidN[i], sizeof(nccl_uidN[i]), MPI_BYTE, 0, MPI_COMM_WORLD);
    }

    ncclComm_t nccl_comm;
    NCCLCHECK(ncclCommInitRank(&nccl_comm, size, nccl_uid, rank));
  
    ncclComm_t nccl_commD[iTwo];
    for(intT i = iZero; i < iTwo; ++i) {NCCLCHECK(ncclCommInitRank(&nccl_commD[i], size, nccl_uidD[i], rank));}
  
    ncclComm_t nccl_commN[iTwo];
    for(intT i = iZero; i < iTwo; ++i) {NCCLCHECK(ncclCommInitRank(&nccl_commN[i], size, nccl_uidN[i], rank));}
    #else
    MPI_Comm MPIcommD[iTwo], MPIcommN[iTwo];
    for(intT i = iZero; i < iTwo; ++i) {MPI_Comm_dup(MPI_COMM_WORLD, & MPIcommD[i]); MPI_Comm_dup(MPI_COMM_WORLD, & MPIcommN[i]);}
    #endif
  #else
  const intT rank = iZero, size = iOne;
  #endif

  intT print_freq = max_iter/10;       // max_iter = 150, so that print_freq = 15
  if(print_freq > 50) print_freq = 50;
  if(print_freq <  1) print_freq =  1;

  floatT * __restrict__ const r = b;                                                                // r <- b (b is overrided due to reusing)

  floatT alphaNL = zero, alphaN = zero, alphaNold = zero,
         alphaDL = zero, alphaD = zero;

  #if defined(USING_CUDA)

  floatT * __restrict__ const d_r = d_b; 

  floatT *d_alphaNL    		;
  floatT *d_alphaN    		;
  floatT *d_alphaNold    	;
  floatT *d_alphaD    		;
  floatT *d_alphaDL    		;
  floatT *d_one    		;
  floatT *d_oneM   		;
  floatT *d_zero   		;
  floatT *d_alphaP, *d_alphaM	;
  floatT *d_beta   		;
  floatT *d_betaM		;
  floatT *d_Ap			;
  floatT *d_p	       		;

  cudaStream_t & stream0 = streamArr[0];
  
  void*                dBuffer    = NULL;
  size_t               bufferSize = 0;   
  
  cudaEvent_t event;
  cudaEventCreateWithFlags(& event, cudaEventDisableTiming);


  CHECK_CUDA( cudaMalloc((void**) &d_Ap	  	,  nrow      * sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**) &d_p	 	,  ncol      * sizeof(floatT))  )
  // Create dense vector Ap
  cusparseDnVecDescr_t vecAp;
  CHECK_CUSPARSE( cusparseCreateDnVec(&vecAp, nrow, d_Ap, CUDA_R_64F) )
  // Create dense vector p
  cusparseDnVecDescr_t vecP;
  CHECK_CUSPARSE( cusparseCreateDnVec(&vecP, ncol, d_p, CUDA_R_64F) )
  // Create dense vector X
  cusparseDnVecDescr_t vecX;
  CHECK_CUSPARSE( cusparseCreateDnVec(&vecX, ncol, d_x, CUDA_R_64F) )

  CHECK_CUDA( cudaMalloc((void**)(&d_one)	,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_oneM)   	,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_zero)   	,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_alphaN)    ,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_alphaNL)   ,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_alphaNold) ,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_alphaD )   ,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_alphaDL)   ,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_alphaP)   	,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_alphaM)   	,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_beta)   	,                       sizeof(floatT))  )
  CHECK_CUDA( cudaMalloc((void**)(&d_betaM)   	,                       sizeof(floatT))  )   

  CHECK_CUDA( cudaMemcpyAsync(d_one  , &one     ,         sizeof(floatT), cudaMemcpyHostToDevice  , stream0) )
  CHECK_CUDA( cudaMemcpyAsync(d_oneM , &oneM    ,         sizeof(floatT), cudaMemcpyHostToDevice  , stream0) )
  CHECK_CUDA( cudaMemset(d_zero,      0, sizeof(floatT)) )
  CHECK_CUDA( cudaMemset(d_alphaN,    0, sizeof(floatT)) )
  CHECK_CUDA( cudaMemset(d_alphaNold, 0, sizeof(floatT)) )
  CHECK_CUDA( cudaMemset(d_alphaNL,   0, sizeof(floatT)) )
  CHECK_CUDA( cudaMemset(d_alphaD,    0, sizeof(floatT)) )
  CHECK_CUDA( cudaMemset(d_alphaDL,   0, sizeof(floatT)) )
  cudaStreamSynchronize(stream0);
  #endif

  #ifdef USING_MPI
   #ifdef USING_CUDA
     if(size > iOne) {exchange_externals_gpu(
        #if defined(USING_NCCL)
        nccl_comm,
        #else
        MPItag+iOne,
        #endif        
        A, d_x, d_elements_to_send, d_send_buffer, &stream0); }
   #else
     if(size > iOne) {TICK(); exchange_externals(MPItag+iOne, A, x); TOCK(t5);}                        // Communicate x before SpMV
   #endif
  #endif

  #ifdef USING_OMP_TASK
  #pragma omp taskgroup task_reduction(+:alphaNL)
  #endif
  for(intT i = iZero; i < ntasks; ++i)
  {
    const intT rowIdx = i*rowBs                  ,
               rowBsA = MIN(rowBs, nrow - rowIdx);

    #if   defined(USING_OSS_TASK)
    #pragma oss task default(none) label("SpMV-Ax0") firstprivate(i, rowIdx, rowBsA)   \
                     firstprivate(iOne, zero, one, A, Amkl, descr, x, Ap, r, p)        \
                     in       ({x[depMVidx[i][j];depMVsize[i][j]], j=0:depMVite[i]-1}) \
                     out      (Ap[rowIdx;rowBsA])                                      \
                     out      (r [rowIdx;rowBsA])                                      \
                     out      (p [rowIdx;rowBsA])                                      \
                     reduction(+:alphaNL)
    #elif defined(USING_OMP_TASK)
    #pragma omp task default(none)                   firstprivate(i, rowIdx, rowBsA)         \
                     firstprivate(iOne, zero, one, A, Amkl, descr, x, Ap, r, p)              \
                     depend(iterator(j=0:depMVite[i]), in:x[depMVidx[i][j]:depMVsize[i][j]]) \
                     depend(out:Ap[rowIdx:rowBsA])                                           \
                     depend(out:r [rowIdx:rowBsA])                                           \
                     depend(out:p [rowIdx:rowBsA])                                           \
                     in_reduction(+:alphaNL)
    #endif
    {
      #ifdef USING_CUDA
      cusparseSetStream(handle_csp, stream0);
      CHECK_CUSPARSE(  cusparseSpMV_bufferSize(
                                 handle_csp, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 d_one, matA, vecX, d_zero, vecAp, CUDA_R_64F,
                                 CUSPARSE_MV_ALG_DEFAULT, &bufferSize) )                   
      CHECK_CUDA(  cudaMalloc(&dBuffer, bufferSize) )     
      CHECK_CUSPARSE( cusparseSpMV(handle_csp, CUSPARSE_OPERATION_NON_TRANSPOSE,	        // Ap[ ] = A[ ][ ]*x[ ]
                                 d_one, matA, vecX, d_zero, vecAp, CUDA_R_64F,
                                 CUSPARSE_MV_ALG_DEFAULT, dBuffer) )                       		              
      cublasSetStream(handle_cbl, stream0);
      CHECK_CUBLAS( cublasDaxpy(handle_cbl, rowBsA, d_oneM, d_Ap, iOne, d_r, iOne         ) )    // r = b - Ap
      CHECK_CUBLAS( cublasDcopy(handle_cbl, rowBsA,         d_r,  iOne, d_p, iOne           ) )  // p = r
      CHECK_CUBLAS( cublasDdot (handle_cbl, rowBsA,         d_r,  iOne, d_r, iOne, d_alphaNL) )  // r*r
      #else
      TICK();            sparse_d_mv(rowIdx, rowBsA, A, x, Ap+rowIdx);               TOCK(t3);      // Ap[ ] = A[ ][ ]*x[ ]
      TICK();                  daxpy(rowBsA, -one, Ap+rowIdx, iOne, r+rowIdx, iOne); TOCK(t2);      // r = b - Ap
      TICK();                  dcopy(rowBsA,       r +rowIdx, iOne, p+rowIdx, iOne); TOCK(t2);      // p = r
      TICK(); alphaNL +=       ddot (rowBsA,       r +rowIdx, iOne, r+rowIdx, iOne); TOCK(t1);      // r*r
      #endif
    }
  }

  if(size == iOne)
  {
    #if   defined(USING_OSS_TASK)
    #pragma oss task default(none) label("alphaN") in(alphaNL) out(alphaN)
    #elif defined(USING_OMP_TASK)
    #pragma omp task default(none)                 shared(alphaNL, alphaN) depend(in: alphaNL) depend(out: alphaN)
    #endif
    #ifdef USING_CUDA
    CHECK_CUDA( cudaMemcpyAsync(d_alphaN, d_alphaNL, sizeof(floatT), cudaMemcpyDeviceToDevice, stream0) )
    #else
    alphaN = alphaNL;
    #endif
  }
  else
  {
    #ifdef USING_MPI
      #if defined(USING_NCCL)
      NCCL_Sum_GPU(nccl_commN[iOne], d_alphaNL, d_alphaN, t4, &stream0 );         
      #else
      MPI_Sum(MPIcommN[iOne], alphaNL, alphaN, t4);                                                   // Global sum of r*r
      #endif  
    #endif
  }

  #if   defined(USING_OSS_TASK)
  #pragma oss taskwait in(alphaN)
  #elif defined(USING_OMP_TASK)
  #pragma omp task depend(in: alphaN) if(0)
  {}
  #endif

  #ifdef USING_CUDA
  CHECK_CUDA(cudaMemcpyAsync(&alphaN, d_alphaN, sizeof(floatT), cudaMemcpyDeviceToHost, stream0) ) // Necessary for calculation of normr
  cudaStreamSynchronize(stream0);
  #endif

  alphaNold =      alphaN ;
  normr     = sqrt(alphaN);

  if  (normr <= tolerance) {return(0);}                                                             // Check convergence
  else
  {
    if(rank == iZero) cout << "Initial residual = " << normr << endl;
  }

    #ifdef USING_CUDA
    CHECK_CUDA( cudaMemcpyAsync(d_alphaNold, d_alphaN   , sizeof(floatT), cudaMemcpyDeviceToDevice  , stream0) ) 
    CHECK_CUDA( cudaMemcpyAsync(&alphaNold , d_alphaNold, sizeof(floatT), cudaMemcpyDeviceToHost, stream0) ) // Necessary for calculation of normr
    
    cudaEventRecord(event, stream0);
    #endif

  // Main loop
  niters = iZero;
  for(intT k = iZero; k < max_iter; ++k)
  {
    ++niters;

    #ifdef USING_MPI
      #ifdef USING_CUDA
        if(size > iOne) {TICK() ; exchange_externals_gpu(
         #if defined(USING_NCCL)
         nccl_comm,
         #else        
         MPItag+ISODD(k),
         #endif 
         A, d_p, d_elements_to_send, d_send_buffer, &stream0); TOCK(t5); }
      #else
        if(size > iOne) {TICK(); exchange_externals(MPItag+ISODD(k), A, p); TOCK(t5);}            // Communicate p before sparse_d_mv
      #endif
    #endif

    #if   defined(USING_OSS_TASK)
    #pragma oss task default(none) label("alphaDL=0") firstprivate(zero) out(alphaDL)
    #elif defined(USING_OMP_TASK)
    #pragma omp task default(none)                    firstprivate(zero) shared(alphaDL) depend(out: alphaDL)
    #endif
    #ifdef USING_CUDA
    CHECK_CUDA( cudaMemset(d_alphaDL,    0, sizeof(floatT)) )
    #else
    alphaDL = zero;
    #endif 

    #ifdef USING_OMP_TASK
    #pragma omp taskgroup task_reduction(+:alphaDL)
    #endif
    for(intT i = iZero; i < ntasks; ++i)
    {
      const intT rowIdx = i*rowBs                  ,
                 rowBsA = MIN(rowBs, nrow - rowIdx);

      #if   defined(USING_OSS_TASK)
      #pragma oss task default(none) label("alphaDL") firstprivate(i, rowIdx, rowBsA)    \
                       firstprivate(iOne, zero, one, A, Amkl, descr, x, p, Ap)           \
                       in       ({p[depMVidx[i][j];depMVsize[i][j]], j=0:depMVite[i]-1}) \
                       out      (Ap[rowIdx;rowBsA])                                      \
                       reduction(+:alphaDL)
      #elif defined(USING_OMP_TASK)
      #pragma omp task default(none)                  firstprivate(i, rowIdx, rowBsA)          \
                       firstprivate(iOne, zero, one, A, Amkl, descr, x, p, Ap)                 \
                       depend(iterator(j=0:depMVite[i]), in:p[depMVidx[i][j]:depMVsize[i][j]]) \
                       depend(out:Ap[rowIdx:rowBsA])                                           \
                       in_reduction(+:alphaDL)
      #endif
      {
        #ifdef USING_CUDA
        cusparseSetStream(handle_csp, stream0);
        CHECK_CUSPARSE(  cusparseSpMV_bufferSize(
                                 handle_csp, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 d_one, matA, vecP, d_zero, vecAp, CUDA_R_64F,
                                 CUSPARSE_MV_ALG_DEFAULT, &bufferSize) )                   
        CHECK_CUDA(  cudaMalloc(&dBuffer, bufferSize) )        
        CHECK_CUSPARSE( cusparseSpMV(handle_csp, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 d_one, matA, vecP, d_zero, vecAp, CUDA_R_64F,
                                 CUSPARSE_MV_ALG_DEFAULT, dBuffer) )    		        ;    // Ap[ ] = A[ ][ ]*p[ ]
        cublasSetStream(handle_cbl, stream0);
        CHECK_CUBLAS( cublasDdot (handle_cbl, rowBsA, d_p, iOne, d_Ap, iOne, d_alphaDL) )	;    // p*Ap
        #else
        TICK();           sparse_d_mv(rowIdx, rowBsA, A, p, Ap+rowIdx);         TOCK(t3);           // Ap[ ] = A[ ][ ]*p[ ]
        TICK(); alphaDL +=       ddot(rowBsA, p+rowIdx, iOne, Ap+rowIdx, iOne); TOCK(t1);           // p*Ap
        #endif
      }
    }

    if(size == iOne)
    {
      #if   defined(USING_OSS_TASK)
      #pragma oss task default(none) label("alphaD") in(alphaDL) out(alphaD)
      #elif defined(USING_OMP_TASK)
      #pragma omp task default(none)                 shared(alphaDL, alphaD) depend(in: alphaDL) depend(out: alphaD)
      #endif
      #ifdef USING_CUDA
      CHECK_CUDA( cudaMemcpyAsync(d_alphaD, d_alphaDL, sizeof(floatT), cudaMemcpyDeviceToDevice, stream0) )      
      #else
      alphaD = alphaDL;
      #endif
    }
    else
    {
      #ifdef USING_MPI
        #if defined(USING_NCCL)
         NCCL_Sum_GPU(nccl_commD[ISODD(k)], d_alphaDL, d_alphaD, t4, &stream0 );         
         #else
         MPI_Sum(MPIcommD[ISODD(k)], alphaDL, alphaD, t4); 			                  // Global sum of r*r
        #endif
      #endif
    }

    #if   defined(USING_OSS_TASK)
    #pragma oss taskwait in(alphaNold)
    #elif defined(USING_OMP_TASK)
    #pragma omp task depend(in: alphaNold) if(0)
    {}
    #endif
    #ifdef USING_CUDA
    cudaEventSynchronize(event);
    #endif
    normr = sqrt(alphaNold);

    if  (normr <= tolerance) {break;}                                                               // Check convergence
    else
    {
      if((rank == iZero) && (k % print_freq) == iZero)
      {
        cout << "Iteration = " << k << ", residual = " << normr << endl;
      }
    }

    #if   defined(USING_OSS_TASK)
    #pragma oss task default(none) label("alphaNL=0") firstprivate(zero) in(alphaN) out(alphaNL, alphaNold)
    #elif defined(USING_OMP_TASK)
    #pragma omp task default(none)                    firstprivate(zero) shared(alphaN, alphaNL, alphaNold) depend(in: alphaN) depend(out: alphaNL, alphaNold)
    #endif
    #ifdef USING_CUDA
    {
    CHECK_CUDA( cudaMemcpyAsync(d_alphaNold, d_alphaN   , sizeof(floatT), cudaMemcpyDeviceToDevice  , stream0) ) 
    CHECK_CUDA( cudaMemcpyAsync(&alphaNold , d_alphaNold, sizeof(floatT), cudaMemcpyDeviceToHost, stream0) ) // Necessary for calculation of normr
    CHECK_CUDA( cudaMemset(d_alphaNL,    0, sizeof(floatT)) )
    }
    
    cudaEventRecord(event, stream0);						    		          //Create an event to mark copying of d_alphaNold  
    #else
    {alphaNold = alphaN; alphaNL = zero;}
    #endif

    #ifdef USING_OMP_TASK
    #pragma omp taskgroup task_reduction(+:alphaNL)
    #endif
    for(intT i = iZero; i < ntasks; ++i)
    {
      const intT rowIdx = i*rowBs                  ,
                 rowBsA = MIN(rowBs, nrow - rowIdx);

      #if   defined(USING_OSS_TASK)
      #pragma oss task default(none) label("alphaNL") firstprivate(iOne, rowIdx, rowBsA) \
                       firstprivate(p, Ap, x, r)                                         \
                       in       (p [rowIdx;rowBsA], alphaD, alphaNold)                   \
                       in       (Ap[rowIdx;rowBsA])                                      \
                       inout    (x [rowIdx;rowBsA])                                      \
                       inout    (r [rowIdx;rowBsA])                                      \
                       reduction(+:alphaNL)
      #elif defined(USING_OMP_TASK)
      #pragma omp task default(none)                  firstprivate(iOne, rowIdx, rowBsA) \
                       firstprivate(p, Ap, x, r) shared(alphaD, alphaNold)               \
                       depend(in:   p [rowIdx:rowBsA], alphaD, alphaNold)                \
                       depend(in:   Ap[rowIdx:rowBsA])                                   \
                       depend(inout:x [rowIdx:rowBsA])                                   \
                       depend(inout:r [rowIdx:rowBsA])                                   \
                       in_reduction(+:alphaNL)
      #endif
      {
        const floatT alpha = alphaNold/alphaD;

        #ifdef USING_CUDA
        divide_cuda( stream0, d_alphaNold, d_alphaD, d_alphaP, d_alphaM );
        cublasSetStream(handle_cbl, stream0);
        CHECK_CUBLAS( cublasDaxpy(handle_cbl, rowBsA, d_alphaP, d_p , iOne, d_x , iOne     ) );// x = x + alpha* p
        cublasSetStream(handle_cbl, stream0);
        CHECK_CUBLAS( cublasDaxpy(handle_cbl, rowBsA, d_alphaM, d_Ap, iOne, d_r , iOne     ) );// r = r - alpha*Ap
        cublasSetStream(handle_cbl, stream0);
        CHECK_CUBLAS( cublasDdot (handle_cbl, rowBsA, d_r     , iOne, d_r , iOne, d_alphaNL) );// r*r
        #else
        TICK();                  daxpy(rowBsA, +alpha, p +rowIdx, iOne, x+rowIdx, iOne); TOCK(t2);  // x = x + alpha* p
        TICK();                  daxpy(rowBsA, -alpha, Ap+rowIdx, iOne, r+rowIdx, iOne); TOCK(t2);  // r = r - alpha*Ap
        TICK(); alphaNL +=       ddot (rowBsA,         r +rowIdx, iOne, r+rowIdx, iOne); TOCK(t1);  // r*r
        #endif
      }
    }

    if(size == iOne)
    {
      #if   defined(USING_OSS_TASK)
      #pragma oss task default(none) label("alphaN") in(alphaNL) out(alphaN)
      #elif defined(USING_OMP_TASK)
      #pragma omp task default(none)                 shared(alphaNL, alphaN) depend(in: alphaNL) depend(out: alphaN)
      #endif
      #ifdef USING_CUDA
      CHECK_CUDA( cudaMemcpyAsync(d_alphaN, d_alphaNL, sizeof(floatT), cudaMemcpyDeviceToDevice, stream0) ) 
      #else
      alphaN = alphaNL;
      #endif
    }
    else
    {
      #ifdef USING_MPI
        #if defined(USING_NCCL)
        NCCL_Sum_GPU(nccl_commN[ISODD(k)], d_alphaNL, d_alphaN, t4, &stream0 );         
        #else      
        MPI_Sum(MPIcommN[ISODD(k)], alphaNL, alphaN, t4);                                    // Global sum of r*r
        #endif
      #endif
    }

    for(intT i = iZero; i < ntasks; ++i)
    {
      const intT rowIdx = i*rowBs                  ,
                 rowBsA = MIN(rowBs, nrow - rowIdx);

      #if   defined(USING_OSS_TASK)
      #pragma oss task default(none) label("p") firstprivate(iOne, one, rowIdx, rowBsA) \
                       firstprivate(r, p)                                               \
                       in   (r[rowIdx;rowBsA], alphaN, alphaNold)                       \
                       inout(p[rowIdx;rowBsA])
      #elif defined(USING_OMP_TASK)
      #pragma omp task default(none)            firstprivate(iOne, one, rowIdx, rowBsA) \
                       firstprivate(r, p) shared(alphaN, alphaNold)                     \
                       depend(in:   r[rowIdx:rowBsA], alphaN, alphaNold)                \
                       depend(inout:p[rowIdx:rowBsA])
      #endif
      {
      #ifdef USING_CUDA
        divide_cudaP( stream0, d_alphaN, d_alphaNold, d_beta );
        cublasSetStream(handle_cbl, stream0);
        CHECK_CUBLAS( cublasDscal(handle_cbl, rowBsA, d_beta , d_p, iOne          ) )	            // p = beta*p
        cublasSetStream(handle_cbl, stream0);
	CHECK_CUBLAS( cublasDaxpy(handle_cbl, rowBsA, d_one , d_r, iOne, d_p, iOne) )               // p = p + 1*r
       #else
        TICK();
              daxpby(rowBsA, one, r+rowIdx, iOne, alphaN/alphaNold, p+rowIdx, iOne);                // p = r + beta*p
        TOCK(t2);
       #endif
      }
    }
  } // End of main loop


  for(intT i = iZero; i < ntasks; ++i)
  {
    const intT rowIdx = i*rowBs                  ,
               rowBsA = MIN(rowBs, nrow - rowIdx);

    #if   defined(USING_OSS_TASK)
    #pragma oss task default(none) label("x_final") firstprivate(iOne, rowIdx, rowBsA) \
                     firstprivate(p, x)                                                \
                     in   (p[rowIdx;rowBsA], alphaD, alphaN)                           \
                     inout(x[rowIdx;rowBsA])
    #elif defined(USING_OMP_TASK)
    #pragma omp task default(none)                  firstprivate(iOne, rowIdx, rowBsA) \
                     firstprivate(p, x) shared(alphaD, alphaN)                         \
                     depend(in:   p[rowIdx:rowBsA], alphaD, alphaN)                    \
                     depend(inout:x[rowIdx:rowBsA])
    #endif
    {
      #ifdef USING_CUDA
       divide_cudaP( stream0, d_alphaN, d_alphaD, d_alphaP );
       cublasSetStream(handle_cbl, stream0);
       CHECK_CUBLAS( cublasDaxpy(handle_cbl, rowBsA,   d_alphaP,  d_p , iOne, d_x, iOne) )     // x = x + alpha* p
      #else
      TICK();
            daxpy(rowBsA, alphaN/alphaD, p+rowIdx, iOne, x+rowIdx, iOne);                           // x = x + alpha*p
      TOCK(t2);
      #endif
    }
  }


  #if   defined(USING_OSS_TASK)
  #pragma oss taskwait
  #elif defined(USING_OMP_TASK)
  #pragma omp taskwait
  #endif

  #ifdef USING_CUDA
  CHECK_CUSPARSE( cusparseDestroyDnVec(vecAp) )
  CHECK_CUSPARSE( cusparseDestroyDnVec(vecP)  )
  CHECK_CUSPARSE( cusparseDestroyDnVec(vecX)  )
  cudaFree(d_Ap) ;
  cudaFree(d_r)  ;
  cudaFree(d_p)  ;
  #endif

  // Store times
  times[1] = t1; // ddot      time
  times[2] = t2; // waxpby    time
  times[3] = t3; // sparsemv  time
  times[4] = t4; // allreduce time
  #ifdef USING_MPI
  times[5] = t5; // exchange  time
  #endif

  #ifdef USING_CUDA
  cudaStreamSynchronize(stream0);
  cudaEventDestroy(event) ;
//  cudaStreamDestroy(stream0);
  #else
  MMAPUNALLOC(Ap0, nrow, floatT, thp);
  MMAPUNALLOC(p0 , ncol, floatT, thp);
  #endif

  times[0] = mytimer() - t_begin; // Total time. All done...

  return(0);
}
