int element_buffer(cudaStream_t & stream, double * d_send_buffer, double * d_x, int64_t * d_elements_to_send, int64_t d_idxS, int64_t  d_n_send);
int mult_cuda     (cudaStream_t & stream, double * d_a,   double * d_b,   double * d_multP ,  double * d_multM	);
int divide_cuda   (cudaStream_t & stream, double * d_a,   double * d_b,   double * d_ratioP,  double * d_ratioM	);
int divide_cudaP  (cudaStream_t & stream, double * d_a,   double * d_b,   double * d_ratioP			);
int divide_cudaM  (cudaStream_t & stream, double * d_a,   double * d_b,   double * d_ratioM			);
int sqrt_inv_cuda (cudaStream_t & stream, double * d_a,   double * d_sqrt_inv				  	);
int sqrt_cuda     (cudaStream_t & stream, double * d_a,   double * d_sqrt					);
int vecDot2       (cudaStream_t & stream, double * dev_a, double * dev_b, double * dev_c, int N		  	);
int jac          (cudaStream_t & stream, double * dev_a, double * dev_b, double * dev_c, double * dev_d, int N	);
int vecAdd       (cudaStream_t & stream, double * dev_a, double * dev_b, double * dev_c, int N		       	);
//int jacGS_old    (cudaStream_t & stream, double * d_a,   double * d_b,   double * d_c,   double * d_diff, double * d_x, double * d_xNew, int N	);
//int dot_product  (cudaStream_t & stream, double * d_x,   double * d_y,   double * d_prod,int N);
int jacobi_csr   (cudaStream_t & stream, double * d_a,   double * d_b,   double * d_c,   double * d_diff, double * d_x, double * d_xNew, double * dot, int N);
int jacobi_diag  (cudaStream_t & stream, double * d_a,   double * d_b,   double * d_c,   double * d_diff, double * d_x, double * d_xNew, double * dot, int N);
int GS_csr       (cudaStream_t & stream, double * d_a,   double * d_b,   double * d_c,   double * d_diff, double * d_x,  	         double * dot, int N);
int GS_diag      (cudaStream_t & stream, double * d_a,   double * d_b,   double * d_c,   double * d_diff, double * d_x,  	         double * dot, int N);
int gs_csr2 (cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_x, double * d_xNew, double * d_prod, int N) ;






