//@HEADER
// ************************************************************************
//
//                     HLAM: Hybrid Linear Algebra Methods
//                 Copyright (c) 2022, Pedro J. Martinez-Ferrer
//               HPCCG: Simple Conjugate Gradient Benchmark Code
//                 Copyright (2006) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// BSD 3-Clause License
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)
//
// Questions? Contact Michael A. Heroux (maherou@sandia.gov)
//
// ************************************************************************
//@HEADER


// Main routine of a program that reads a sparse matrix, right side
// vector, solution vector and initial guess from a file  in HPC
// format.  This program then calls the HPCCG conjugate gradient
// solver to solve the problem, and then prints results.

// Calling sequence:

// test_HPCCG linear_system_file

// Routines called:

// read_HPC_row - Reads in linear system

// mytimer - Timing routine (compile with -DWALL to get wall clock
//           times

// HPCCG - CG Solver

// compute_residual - Compares HPCCG solution to known solution.

#include <sys/mman.h>
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cassert>
#include <string>
#include <cmath>
#ifdef USING_MPI
#include <mpi.h> // If this routine is compiled with -DUSING_MPI
                 // then include mpi.h
#include "make_local_matrix.hpp" // Also include this function
#endif
#if defined(USING_OMP_FORK) || defined(USING_OMP_TASK)
#include <omp.h>
#endif
#ifdef USING_MKL
#include "mkl.h"
#include <mkl_spblas.h>
#endif
#ifdef USING_CUDA
#include <cuda_runtime.h>
#include <cusparse.h>         // cusparseSpMV
#include <cublas_v2.h>
#endif
#include "generate_matrix.hpp"
#include "read_HPC_row.hpp"
#include "mytimer.hpp"
#include "HPC_sparsemv.hpp"
#include "compute_residual.hpp"
#include "HPC_Sparse_Matrix.hpp"
#include "dump_matlab_matrix.hpp"

#include "YAML_Element.hpp"
#include "YAML_Doc.hpp"

#include "HPCCG.hpp"

#undef DEBUG

int main(int argc, char *argv[])
{

  HPC_Sparse_Matrix * A;
  floatT * x0, * x, * b0, * b, * xexact0, * xexact;
  intT ierr = 0;
  intT i, j;
  intT ione = 1;
  double norm, d;
  double times[7] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  double t6 = 0.0;
  intT nx,ny,nz;
  intT ntasksUser;

#ifdef USING_MPI

  int  dummy;
  intT size , rank; // Number of MPI processes, My process ID

  #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
  MPI_Init_thread(& argc, & argv, MPI_THREAD_MULTIPLE, & dummy);
  assert(dummy == MPI_THREAD_MULTIPLE);
  #else
  MPI_Init(& argc, & argv);
  #endif

  MPI_Comm_size(MPI_COMM_WORLD, & dummy); size = dummy;
  MPI_Comm_rank(MPI_COMM_WORLD, & dummy); rank = dummy;

#ifdef USING_CUDA
  // Bind MPI processes to CUDA devices
  MPI_Comm MPICUDAcomm;
  MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, rank, MPI_INFO_NULL, & MPICUDAcomm);
  MPI_Comm_rank      (MPICUDAcomm, & dummy); const intT MPICUDArank = dummy;
  MPI_Comm_size      (MPICUDAcomm, & dummy); const intT MPICUDAsize = dummy;
  
  cudaSetDevice(MPICUDArank % MPICUDAsize);
#endif

  //  if (size < 100) cout << "Process "<<rank<<" of "<<size<<" is alive." <<endl;

#else

  intT size = 1; // Serial case (not using MPI)
  intT rank = 0;

#endif


#ifdef DEBUG
  if (rank==0)
   {
    intT junk = 0;
    cout << "Press enter to continue"<< endl;
    cin >> junk;
   }

  MPI_Barrier(MPI_COMM_WORLD);
#endif


  if(argc != 7 && argc!=5) {
    if (rank==0)
      cerr << "Usage:" << endl
           << "Mode 1: " << argv[0] << " nx ny nz" << endl
           << "     where nx, ny and nz are the local sub-block dimensions, or" << endl
           << "Mode 2: " << argv[0] << " HPC_data_file " << endl
           << "     where HPC_data_file is a globally accessible file containing matrix data." << endl;
    exit(1);
  }

  if(argc==5)
  {
    nx = atoi(argv[1]);
    ny = atoi(argv[2]);
    nz = atoi(argv[3]);
    ntasksUser = atoi(argv[4]);

    generate_matrix(ntasksUser, nx, ny, nz, & A, & x, & b, & xexact); // Generate matrix
  }
  else
  {
    nx = atoi(argv[2]);
    ny = atoi(argv[3]);
    nz = atoi(argv[4]);
    ntasksUser = atoi(argv[5]);

    read_HPC_row(argv[1], argv[6], ntasksUser, nx, ny, nz, & A, & x, & b, & xexact);
  }

  bool dump_matrix = false;
  if (dump_matrix && size<=4) dump_matlab_matrix(A, b, rank);

#ifdef USING_MPI

  // Transform matrix indices from global to local values.
  // Define number of columns for the local matrix.
  t6 = mytimer(); make_local_matrix(A); t6 = mytimer() - t6; // External/ghost additional points for MPI communication
  times[6] = t6;

#endif

  // Initialise xexact, x & b arrays after generate_matrix and make_local_matrix functions
  if(argc==5)
  {
    xexact0 = MMAPALLOC(A->local_nrow, floatT, thp); xexact = MMAPALIGN(xexact0, floatT, thp);
    x0      = MMAPALLOC(A->local_ncol, floatT, thp); x      = MMAPALIGN(x0     , floatT, thp); // Use ncol for Jacobi/Gauss-Seidel algorithms
    b0      = MMAPALLOC(A->local_ncol, floatT, thp); b      = MMAPALIGN(b0     , floatT, thp); // Use ncol to save memory on BiCGStab algorithm

     HPCCGinit(A->local_nrow, A->local_ncol, rank, A->rowIndex, xexact, x, b);
  }

  const intT nrow   = A->local_nrow,
             ncol   = A->local_ncol,
             nnz    = A->local_nnz ,
             rowBs  = splitInterval(nrow, A->ntasksUser, simdSize),
             ntasks = CEILDIV(nrow, rowBs);

  #ifdef USING_MPI
  const intT ntasksExtra = A->num_send_neighbors;
  #else
  const intT ntasksExtra = iZero;
  #endif

  #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
  const intT nxy = nx*ny                     ,
             tkI = CEILDIV(nxy, rowBs) + iOne; // Task interval to go find the firsts neighbours along +/- z-direction

  intT   countIte, idx;
  double tmd;

  tmd = mytimer();

  // Allocate SpMV dependencies
  const intT lNtasks = ntasks, lNtasks2 = ntasks*10; // Choose enough number of additional tasks: it could be up to 27*ntasks

  // Create arrays
  A->depMVidxPtr0  = MMAPALLOC(lNtasks, intT *, thp); A->depMVidxPtr  = MMAPALIGN(A->depMVidxPtr0 , intT *, thp);
  A->depMVsizePtr0 = MMAPALLOC(lNtasks, intT *, thp); A->depMVsizePtr = MMAPALIGN(A->depMVsizePtr0, intT *, thp);

  A->depMVite0  = MMAPALLOC(lNtasks , intT, thp); A->depMVite  = MMAPALIGN(A->depMVite0 , intT, thp);
  A->depMVidx0  = MMAPALLOC(lNtasks2, intT, thp); A->depMVidx  = MMAPALIGN(A->depMVidx0 , intT, thp);
  A->depMVsize0 = MMAPALLOC(lNtasks2, intT, thp); A->depMVsize = MMAPALIGN(A->depMVsize0, intT, thp);

  intT * __restrict__ depMVidx  = A->depMVidx ,
       * __restrict__ depMVsize = A->depMVsize;

  countIte = iZero;

  if(rank == 0) {cout << "Calculate SpMV dependencies" << endl;}

  // // Calculate SpMV dependencies (general case)
  // for(intT i0 = iZero; i0 < ntasks; ++i0) // Loop of tasks/blocks covering all rows ("row task")
  // {
  //   const intT rowIdx = i0*rowBs                 ,
  //              rowBsA = MIN(rowBs, nrow - rowIdx);

  //   intT & depMVite = A->depMVite[i0]; depMVite = iZero;

  //   A->depMVidxPtr [i0] = depMVidx ;
  //   A->depMVsizePtr[i0] = depMVsize;

  //   intT accum_j0 = nrow;

  //   for(intT j0 = iZero; j0 < ntasks+ntasksExtra; ++j0) // Loop of tasks/blocks covering all columns: rows + extra_points ("column task")
  //   {
  //     const intT beg_j0 = (j0 < ntasks) ? j0*rowBs                  : accum_j0                   , // Must use rowBs
  //                siz_j0 = (j0 < ntasks) ? MIN(rowBs, nrow - beg_j0) : A->recv_length[j0 - ntasks], // Must use rowBs
  //                end_j0 = beg_j0 + siz_j0 - iOne;

  //     if(j0 >= ntasks) {accum_j0 += siz_j0;}

  //     bool belongs = false;

  //     for(intT i = rowIdx; i < rowIdx+rowBsA; ++i) // Loop along every row within a "row task"
  //     {
  //       const intT   nnz = A->rowIndex[i+iOne] - A->rowIndex[i];
  //       const intT * __restrict__ const pCol = A->list_of_cols + A->rowIndex[i];

  //       for(intT j = iZero; j < nnz; ++j) // Loop along every column for that particular row
  //       {
  //         const intT colIdx = pCol[j];
  //         if(colIdx >= beg_j0 && colIdx <= end_j0) {belongs = true;}
  //       }

  //       if(belongs) {break;}
  //     }

  //     if(belongs)
  //     {
  //       // cout << "rank = " << rank << ", SpMV task/block i0 = " << i0 << " depends on task/block j0 = " << j0
  //       //      << " which starts at " << beg_j0 << " with size = " << siz_j0 << endl;

  //       ++depMVite; ++countIte;

  //       *depMVidx++  = beg_j0;
  //       *depMVsize++ = siz_j0;
  //     }
  //   }
  // }

  // Calculate SpMV dependencies (particular case of HPCCG)
  for(intT i0 = iZero; i0 < ntasks; ++i0) // Loop of tasks/blocks covering all rows ("row task")
  {
    const intT rowIdx = i0*rowBs                 ,
               rowBsA = MIN(rowBs, nrow - rowIdx);

    intT & depMVite = A->depMVite[i0]; depMVite = iZero;

    A->depMVidxPtr [i0] = depMVidx ;
    A->depMVsizePtr[i0] = depMVsize;

    for(intT j0 = MAX(iZero, i0-tkI); j0 < MIN(ntasks, i0+tkI+iOne); ++j0) // Loop of tasks/blocks covering internal points
    {                                                                      // but only those within the task interval
      const intT beg_j0 = j0*rowBs                 , // Must use rowBs
                 siz_j0 = MIN(rowBs, nrow - beg_j0), // Must use rowBs
                 end_j0 = beg_j0 + siz_j0 - iOne   ;

      bool belongs = false;

      for(intT i = rowIdx; i < rowIdx+rowBsA; ++i) // Loop along every row within a "row task"
      {
        const intT   nnz = A->rowIndex[i+iOne] - A->rowIndex[i];
        const intT * __restrict__ const pCol = A->list_of_cols + A->rowIndex[i];

        for(intT j = iZero; j < nnz; ++j) // Loop along every column for that particular row
        {
          const intT colIdx = pCol[j];
          if(colIdx >= beg_j0 && colIdx <= end_j0) {belongs = true;}
        }

        if(belongs) {break;}
      }

      if(belongs)
      {
        // if(rank == 1)
        // {
        //   cout << "rank = " << rank << ", SpMV task/block i0 = " << i0 << " depends on task/block j0 = " << j0
        //        << " which starts at " << beg_j0 << " with size = " << siz_j0 << endl;
        // }

        ++depMVite; ++countIte;
        *depMVidx++  = beg_j0;
        *depMVsize++ = siz_j0;
      }
    }

    if(i0 <= tkI || i0 >= ntasks-tkI) // Only those within the task interval around the first and last task
    {
      idx = nrow;

      for(intT j0 = iZero; j0 < ntasksExtra; ++j0) // Loop of tasks/blocks covering external points
      {
        const intT beg_j0 = idx                   ,
                   siz_j0 = A->recv_length[j0]    ,
                   end_j0 = beg_j0 + siz_j0 - iOne;

        bool belongs = false;

        for(intT i = rowIdx; i < rowIdx+rowBsA; ++i) // Loop along every row within a "row task"
        {
          const intT   nnz = A->rowIndex[i+iOne] - A->rowIndex[i];
          const intT * __restrict__ const pCol = A->list_of_cols + A->rowIndex[i];

          for(intT j = iZero; j < nnz; ++j) // Loop along every column for that particular row
          {
            const intT colIdx = pCol[j];
            if(colIdx >= beg_j0 && colIdx <= end_j0) {belongs = true;}
          }

          if(belongs) {break;}
        }

        if(belongs)
        {
          // if(rank == 1)
          // {
          //   cout << "rank = " << rank << ", SpMV task/block i0 = " << i0 << " depends on task/block j0 = " << j0
          //        << " which starts at " << beg_j0 << " with size = " << siz_j0 << endl;
          // }

          ++depMVite; ++countIte;
          *depMVidx++  = beg_j0;
          *depMVsize++ = siz_j0;
        }

        idx += siz_j0;
      }
    }
  }

  assert(countIte <= lNtasks2); // Check whether enough dependencies have been allocated

  // Allocate send buffer dependencies
  const intT lNtaskscomm  = 2    ,
             lNtaskscomm2 = 2*tkI, // Choose enough number of additional tasks
             bksize       = 256  ;

  A->depSBidxPtr0  = MMAPALLOC(lNtaskscomm, intT *, thp); A->depSBidxPtr  = MMAPALIGN(A->depSBidxPtr0 , intT *, thp);
  A->depSBsizePtr0 = MMAPALLOC(lNtaskscomm, intT *, thp); A->depSBsizePtr = MMAPALIGN(A->depSBsizePtr0, intT *, thp);

  A->depSBite0  = MMAPALLOC(lNtaskscomm , intT, thp); A->depSBite  = MMAPALIGN(A->depSBite0 , intT, thp);
  A->depSBidx0  = MMAPALLOC(lNtaskscomm2, intT, thp); A->depSBidx  = MMAPALIGN(A->depSBidx0 , intT, thp);
  A->depSBsize0 = MMAPALLOC(lNtaskscomm2, intT, thp); A->depSBsize = MMAPALIGN(A->depSBsize0, intT, thp);

  intT * __restrict__ depSBidx  = A->depSBidx ,
       * __restrict__ depSBsize = A->depSBsize;

  countIte = iZero; idx = iZero;

  if(rank == 0) {cout << "Calculate send buffer dependencies" << endl;}

  // Calculate send buffer dependencies
  for(intT i0 = iZero; i0 < ntasksExtra; ++i0) // Loop of tasks/blocks covering send buffers (# send neighbours)
  {
    const intT n_send = A->send_length[i0];

    intT & depSBite = A->depSBite[i0]; depSBite = iZero;

    A->depSBidxPtr [i0] = depSBidx ;
    A->depSBsizePtr[i0] = depSBsize;

    for(intT j0 = iZero; j0 < ntasks; ++j0) // Loop of tasks/blocks covering all rows
    {
      const intT beg_j0 = j0*rowBs                 ,
                 siz_j0 = MIN(rowBs, nrow - beg_j0),
                 end_j0 = beg_j0 + siz_j0 - iOne   ;

      bool belongs = false;

      for(intT i = idx; i < idx+n_send; i += bksize) // Go in blocks of bksize elements and, if possible, break early
      {
        const intT bs = MIN(bksize, idx+n_send - i);

        for(intT ii = i; ii < i+bs; ++ii)
        {
          const intT elIdx = A->elements_to_send[ii];
          if(elIdx >= beg_j0 && elIdx <= end_j0) {belongs = true;}
        }

        if(belongs) {break;}
      }

      if(belongs)
      {
        // if(rank == 1)
        // {
        //   cout << "rank = " << rank << ", sendBuff task/block i0 = " << i0 << " depends on task/block j0 = " << j0
        //        << " which starts at " << beg_j0 << " with size = " << siz_j0 << endl;
        // }

        ++depSBite; ++countIte;
        *depSBidx++  = beg_j0;
        *depSBsize++ = siz_j0;
      }
    }

    idx += n_send;
  }

  assert(countIte <= lNtaskscomm2); // Check whether enough dependencies have been allocated

  tmd = mytimer() - tmd;
  #endif

  // MKL matrices
  #ifdef USING_MKL
  intT   * __restrict__ const pointerB = A->rowIndex       ;
  intT   * __restrict__ const pointerE = A->rowIndex + iOne;
  intT   * __restrict__ const columns  = A->list_of_cols   ;
  floatT * __restrict__ const values   = A->list_of_vals   ;

  struct matrix_descr descr;
  // descr.type = SPARSE_MATRIX_TYPE_SYMMETRIC;
  // descr.mode = SPARSE_FILL_MODE_LOWER      ;
  // descr.diag = SPARSE_DIAG_NON_UNIT        ;
  descr.type = SPARSE_MATRIX_TYPE_GENERAL;

  sparse_matrix_t * __restrict__ const Amkl0 = MMAPALLOC(ntasks, sparse_matrix_t, thp),
                  * __restrict__ const Amkl  = MMAPALIGN(Amkl0 , sparse_matrix_t, thp);

  for(intT i = iZero; i < ntasks; ++i)
  {
    const intT rowIdx = i*rowBs                  ,
               rowBsA = MIN(rowBs, nrow - rowIdx);

    mkl_sparse_d_create_csr(& Amkl[i], SPARSE_INDEX_BASE_ZERO, rowBsA, ncol,
                            pointerB+rowIdx, pointerE+rowIdx, columns, values);

    mkl_sparse_set_mv_hint    (Amkl[i], SPARSE_OPERATION_NON_TRANSPOSE, descr, iOne);
    mkl_sparse_set_memory_hint(Amkl[i], SPARSE_MEMORY_AGGRESSIVE);
    mkl_sparse_optimize       (Amkl[i]);
  }
  #endif

  #ifdef USING_CUDA
    cudaStream_t   streamArr[0];
    cudaStreamCreateWithFlags(& streamArr[0], cudaStreamNonBlocking);
    cudaStream_t & stream0 = streamArr[0];

    cusparseHandle_t handle_csp;
    CHECK_CUSPARSE(cusparseCreate(& handle_csp))
    cusparseSetPointerMode(handle_csp, CUSPARSE_POINTER_MODE_DEVICE);

    cublasHandle_t handle_cbl;
    CHECK_CUBLAS(cublasCreate(& handle_cbl))
    cublasSetPointerMode(handle_cbl, CUBLAS_POINTER_MODE_DEVICE);

    // Device memory management
    intT   *dA_csrOffsets, *dA_columns	;
    floatT *dA_values			;
    floatT *d_x				;
    floatT *d_xexact			;
    floatT *d_b				;


    CHECK_CUDA( cudaMalloc((void**) &dA_csrOffsets, (nrow + 1)  * sizeof(intT))    ) 
    CHECK_CUDA( cudaMalloc((void**) &dA_columns   ,  nnz 	* sizeof(intT))    )
    CHECK_CUDA( cudaMalloc((void**) &dA_values    ,  nnz	* sizeof(floatT))  )
    CHECK_CUDA( cudaMalloc((void**) &d_x 	  ,  ncol	* sizeof(floatT))  )
    CHECK_CUDA( cudaMalloc((void**) &d_xexact 	  ,  ncol	* sizeof(floatT))  )
    CHECK_CUDA( cudaMalloc((void**) &d_b 	  ,  ncol	* sizeof(floatT))  )

    CHECK_CUDA( cudaMemcpyAsync(dA_csrOffsets , A->rowIndex     , (nrow+1)	* sizeof(intT)  , cudaMemcpyHostToDevice, stream0) )
    CHECK_CUDA( cudaMemcpyAsync(dA_columns    , A->list_of_cols ,  nnz		* sizeof(intT)  , cudaMemcpyHostToDevice, stream0) )
    CHECK_CUDA( cudaMemcpyAsync(dA_values     , A->list_of_vals ,  nnz		* sizeof(floatT), cudaMemcpyHostToDevice, stream0) )
    CHECK_CUDA( cudaMemcpyAsync(d_x           , x               ,  ncol		* sizeof(floatT), cudaMemcpyHostToDevice, stream0) )
    CHECK_CUDA( cudaMemcpyAsync(d_xexact      , xexact          ,  ncol		* sizeof(floatT), cudaMemcpyHostToDevice, stream0) )
    CHECK_CUDA( cudaMemcpyAsync(d_b           , b               ,  ncol		* sizeof(floatT), cudaMemcpyHostToDevice, stream0) )

    cudaStreamSynchronize(stream0);

  //--------------------------------------------------------------------------
    // CUSPARSE APIs
//    void*                     dBuffer    = NULL;
//    size_t                    bufferSize = 0;
//    bufferSize = std::max(bufferSize, (size_t) 64); // CUDA does not like 0-size allocations


    // Create sparse matrix A in CSR format
    cusparseSpMatDescr_t matA = NULL;
    CHECK_CUSPARSE( cusparseCreateCsr(&matA,  nrow,  ncol, nnz, dA_csrOffsets, dA_columns, dA_values,
                                      CUSPARSE_INDEX_64I, CUSPARSE_INDEX_64I, CUSPARSE_INDEX_BASE_ZERO, CUDA_R_64F) )
    // Create dense vector X
    cusparseDnVecDescr_t vecX;
    CHECK_CUSPARSE( cusparseCreateDnVec(&vecX,  ncol, d_x, CUDA_R_64F) )

    // Create dense vector Xexact
    cusparseDnVecDescr_t vecXexact;
    CHECK_CUSPARSE( cusparseCreateDnVec(&vecXexact,  ncol, d_xexact, CUDA_R_64F) )

    // Create dense vector B
    cusparseDnVecDescr_t vecB;
    CHECK_CUSPARSE( cusparseCreateDnVec(&vecB,  nrow, d_b, CUDA_R_64F) )


    #ifdef USING_MPI
    floatT   *d_send_buffer;
    CHECK_CUDA( cudaMalloc((void**)&d_send_buffer      ,                      A->total_to_be_sent      * sizeof(floatT))  )
    CHECK_CUDA( cudaMemcpyAsync(d_send_buffer          , A->send_buffer     , A->total_to_be_sent      * sizeof(floatT), cudaMemcpyHostToDevice, stream0) )
    intT	   *d_elements_to_send;
    CHECK_CUDA( cudaMalloc((void**)&d_elements_to_send ,                       A->total_to_be_sent * sizeof(intT))    )
    CHECK_CUDA( cudaMemcpyAsync(d_elements_to_send     , A->elements_to_send,  A->total_to_be_sent * sizeof(intT)  , cudaMemcpyHostToDevice, stream0) ) 
    #endif

  #endif

  intT   niters, max_iter;
  double normr, tolerance;

  if(rank == 0) {cout << "Enter main loop" << endl;}

  #ifdef USING_OMP_TASK
  #pragma omp parallel
  #pragma omp single
  #endif

//  Updating B Vector based on the CSR matrix format with B=A*[1]
   #ifdef USING_CUDA
      cusparseSetStream(handle_csp, stream0);
      CHECK_CUSPARSE( cusparseSpMV(handle_csp, CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 &one, matA, vecXexact, &zero, vecB, CUDA_R_64F,
                                 CUSPARSE_MV_ALG_DEFAULT, NULL) )     		       ;      // B[ ] = A[ ][ ]*x[ ]
      CHECK_CUDA( cudaMemcpyAsync(b  , d_b  ,  nrow * sizeof(floatT), cudaMemcpyDeviceToHost, stream0) )
      cudaStreamSynchronize(stream0);
    #endif
  
  for(intT gIter = iZero; gIter < iOne; ++gIter)
  {
    if(rank == iZero) cout << "gIter = " << gIter << endl;

//    t1 = mytimer();  // Initialize it (if needed)
    niters    = 0     ;
    normr     = 0.0   ;
    max_iter = 15;
    tolerance = 1.0e-6; // Set tolerance to zero to make all runs do max_iter iterations

//    HPCCGinit(A->local_nrow, A->local_ncol, rank, A->rowIndex, xexact, x, b);

    ierr = HPCCG // Conjugate gradient kernel
    (
      #ifdef USING_CUDA
      handle_csp, handle_cbl, streamArr, matA, d_x, d_b, d_elements_to_send, d_send_buffer, 
      #endif
      A, b, x, max_iter, tolerance, niters, normr, times
    );

    // const double wrk = mytimer(); sparse_d_mv(iZero, nrow, A, xexact, x); times[3] += (mytimer() - wrk);
  }

  if(ierr) cerr << "Error in call to CG: " << ierr << endl;

  #ifdef USING_MPI
  double t4 = times[4];
  double t4min = 0.0;
  double t4max = 0.0;
  double t4avg = 0.0;
  MPI_Allreduce(& t4, & t4min, 1, MPI_FLOATT, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(& t4, & t4max, 1, MPI_FLOATT, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(& t4, & t4avg, 1, MPI_FLOATT, MPI_SUM, MPI_COMM_WORLD);
  t4avg = t4avg/((double) size);

  double t5 = times[5];
  double t5min = 0.0;
  double t5max = 0.0;
  double t5avg = 0.0;
  MPI_Allreduce(& t5, & t5min, 1, MPI_FLOATT, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(& t5, & t5max, 1, MPI_FLOATT, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(& t5, & t5avg, 1, MPI_FLOATT, MPI_SUM, MPI_COMM_WORLD);
  t5avg = t5avg/((double) size);
  #endif


#ifdef USING_CUDA
 // destroy matrix/vector descriptors
  CHECK_CUSPARSE( cusparseDestroySpMat (matA)    )
  CHECK_CUSPARSE( cusparseDestroyDnVec (vecX)    )  
  CHECK_CUSPARSE( cusparseDestroy      (handle_csp) )
  CHECK_CUBLAS  ( cublasDestroy        (handle_cbl) )

    // device memory deallocation
   CHECK_CUDA( cudaFree(dA_csrOffsets) )
   CHECK_CUDA( cudaFree(dA_columns)    )
   CHECK_CUDA( cudaFree(dA_values)     )
   #endif


// initialize YAML doc

  if (rank == iZero)  // Only PE 0 needs to compute and report timing results
    {
      double fniters = niters;
      double fnrow = A->total_nrow;
      double fnnz = A->total_nnz;
      double fnops_ddot = fniters*4*fnrow;
      double fnops_waxpby = fniters*6*fnrow;
      double fnops_sparsemv = fniters*2*fnnz;
      double fnops = fnops_ddot+fnops_waxpby+fnops_sparsemv;

      YAML_Doc doc("hpccg", "1.0");

      doc.add("Parallelism","");

#ifdef USING_MPI
          doc.get("Parallelism")->add("Number of MPI ranks",size);
#else
          doc.get("Parallelism")->add("MPI not enabled","");
#endif

#if defined(USING_OMP_FORK) || defined(USING_OMP_TASK)
          int nthreads = 1;
#pragma omp parallel
          nthreads = omp_get_num_threads();
          doc.get("Parallelism")->add("Number of OpenMP threads",nthreads);
#else
          doc.get("Parallelism")->add("OpenMP not enabled","");
#endif

#if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
          doc.get("Parallelism")->add("Number of OmpSs-2 tasks",ntasks);
#else
          doc.get("Parallelism")->add("OmpSs-2 not enabled","");
#endif

#ifdef USING_CUDA
          doc.get("Parallelism")->add("CUDA is enabled","");
#endif

      doc.add("Dimensions","");
      doc.get("Dimensions")->add("nx",nx);
      doc.get("Dimensions")->add("ny",ny);
      doc.get("Dimensions")->add("nz",nz);



      doc.add("Number of iterations", niters);
      doc.add("Final residual", normr);
      doc.add("*********** Summary (times in sec) ***********","");

      doc.add("Time Summary","");
      doc.get("Time Summary")->add("Total   ",times[0]);
      doc.get("Time Summary")->add("DDOT    ",times[1]);
      doc.get("Time Summary")->add("DAXPBY  ",times[2]);
      doc.get("Time Summary")->add("SPARSEMV",times[3]);

      doc.add("FLOPS Summary","");
      doc.get("FLOPS Summary")->add("Total   ",fnops);
      doc.get("FLOPS Summary")->add("DDOT    ",fnops_ddot);
      doc.get("FLOPS Summary")->add("DAXPBY  ",fnops_waxpby);
      doc.get("FLOPS Summary")->add("SPARSEMV",fnops_sparsemv);

      doc.add("GFLOPS Summary","");
      doc.get("GFLOPS Summary")->add("Total   ",fnops/times[0]/1.0E9);
      doc.get("GFLOPS Summary")->add("DDOT    ",fnops_ddot/times[1]/1.0E9);
      doc.get("GFLOPS Summary")->add("DAXPBY  ",fnops_waxpby/times[2]/1.0E9);
      doc.get("GFLOPS Summary")->add("SPARSEMV",fnops_sparsemv/(times[3])/1.0E9);

#ifdef USING_MPI
      doc.add("DDOT Timing Variations","");
      doc.get("DDOT Timing Variations")->add("Min DDOT MPI_Allreduce time",t4min);
      doc.get("DDOT Timing Variations")->add("Max DDOT MPI_Allreduce time",t4max);
      doc.get("DDOT Timing Variations")->add("Avg DDOT MPI_Allreduce time",t4avg);

      doc.add("P2P Timing Variations","");
      doc.get("P2P Timing Variations")->add("Min P2P MPI_Allreduce time",t5min);
      doc.get("P2P Timing Variations")->add("Max P2P MPI_Allreduce time",t5max);
      doc.get("P2P Timing Variations")->add("Avg P2P MPI_Allreduce time",t5avg);

      double totalSparseMVTime = times[3] + times[5]+ times[6];
      doc.add("SPARSEMV OVERHEADS","");
      doc.get("SPARSEMV OVERHEADS")->add("SPARSEMV GFLOPS W OVERHEAD",fnops_sparsemv/(totalSparseMVTime)/1.0E9);
      doc.get("SPARSEMV OVERHEADS")->add("SPARSEMV PARALLEL OVERHEAD Time", (times[5]+times[6]));
      doc.get("SPARSEMV OVERHEADS")->add("SPARSEMV PARALLEL OVERHEAD Pct", (times[5]+times[6])/totalSparseMVTime*100.0);
      doc.get("SPARSEMV OVERHEADS")->add("SPARSEMV PARALLEL OVERHEAD Setup Time", (times[6]));
      doc.get("SPARSEMV OVERHEADS")->add("SPARSEMV PARALLEL OVERHEAD Setup Pct", (times[6])/totalSparseMVTime*100.0);
      doc.get("SPARSEMV OVERHEADS")->add("SPARSEMV PARALLEL OVERHEAD Bdry Exch Time", (times[5]));
      doc.get("SPARSEMV OVERHEADS")->add("SPARSEMV PARALLEL OVERHEAD Bdry Exch Pct", (times[5])/totalSparseMVTime*100.0);
#endif

      if (rank == 0) { // only PE 0 needs to compute and report timing results
        std::string yaml = doc.generateYAML();
        cout << yaml;
       }
    }

  #ifdef USING_CUDA
  CHECK_CUDA( cudaMemcpyAsync(x, d_x, ncol * sizeof(floatT), cudaMemcpyDeviceToHost, stream0) )
  CHECK_CUDA( cudaFree(d_x) )
  cudaStreamDestroy(stream0);
  #endif

  // Compute difference between known exact solution and computed solution
  // All processors are needed here.
  double residual1 = zero, residual2 = zero, residualInf = zero;
  L1norm       (A->local_nrow, x, xexact, residual1  );
  L2norm       (A->local_nrow, x, xexact, residual2  );
  LinfinityNorm(A->local_nrow, x, xexact, residualInf);
  // L1norm       (A->local_nrow, b, x, residual1  );
  // L2norm       (A->local_nrow, b, x, residual2  );
  // LinfinityNorm(A->local_nrow, b, x, residualInf);


  if(rank == iZero) {cout << "Difference between computed and exact  = " << residual1 << ", " << residual2 << ", " << residualInf << endl;}

  // Total time
  if(rank == iZero)
  {
    #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
    printf("multideps %e\n"  , tmd     );
    #endif
    printf("time %e\n"       , times[0]);
    printf("iterations %li\n", niters  );
    printf("relResidual %e\n", normr   );
    printf("absResidual %e, %e, %e\n", residual1, residual2, residualInf);
  }

  // Finish up
  #ifdef USING_MPI
  MPI_Finalize();
  #endif

  return 0;
}
