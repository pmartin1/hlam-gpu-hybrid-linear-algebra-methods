//@HEADER
// ************************************************************************
//
//                     HLAM: Hybrid Linear Algebra Methods
//                 Copyright (c) 2022, Pedro J. Martinez-Ferrer
//               HPCCG: Simple Conjugate Gradient Benchmark Code
//                 Copyright (2006) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// BSD 3-Clause License
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)
//
// Questions? Contact Michael A. Heroux (maherou@sandia.gov)
//
// ************************************************************************
//@HEADER


#ifndef HPC_SPARSE_MATRIX_H
#define HPC_SPARSE_MATRIX_H

#include "PedroParameters.hpp"

// These constants are upper bounds that might need to be changes for
// pathological matrices, e.g., those with nearly dense rows/columns.

const intT max_external      =          10000000;
const intT max_num_messages  =              1000;
const intT max_num_neighbors = max_num_messages;


struct HPC_Sparse_Matrix_struct
{
  intT start_row;
  intT stop_row;
  intT total_nrow;

  intT local_nrow;
  intT local_ncol; // Must be defined in make_local_matrix

  intT true_local_nnz;

  intT local_nnz;
  intT local_nnz_upper;
  intT local_nnz_lower;

  intT total_nnz;

  intT ntasksUser;

  intT ratio;

  char * title;

  // int    *  nnz_in_row0        , *  nnz_in_row        ;
  intT   ** ptr_to_cols_in_row0, ** ptr_to_cols_in_row; // It cannot be deleted because it is used to generate the local matrix (MPI)
  // floatT ** ptr_to_vals_in_row0, ** ptr_to_vals_in_row;

  // CSR
  intT   * rowIndex0      , * rowIndex      ; // MKL CSR array "rowIndex"
  intT   * list_of_cols0  , * list_of_cols  ; // MKL CSR array "columns"
  floatT * list_of_vals0  , * list_of_vals  ; // MKL CSR array "values"

  // COO
  intT   * list_of_rows0  , * list_of_rows  ; // COO array "rows"

  // MSR
  intT   * rowIndexM0     , * rowIndexM     ; // MSR array "rowIndex"
  intT   * list_of_colsM0 , * list_of_colsM ; // MSR array "columns"
  floatT * list_of_valsM0 , * list_of_valsM ; // MSR array "values"

  // LDU/LDUCSR
  intT   * rowIndex_l0    , * rowIndex_l    ;
  intT   * list_of_cols_l0, * list_of_cols_l;
  floatT * list_of_vals_l0, * list_of_vals_l;

  intT   * rowIndex_u0    , * rowIndex_u    ;
  intT   * list_of_cols_u0, * list_of_cols_u;
  floatT * list_of_vals_u0, * list_of_vals_u;

  // Diagonal
  int8_t *  list_of_dOff0 , *  list_of_dOff ; // Diagonal offsets
  floatT ** ptr_to_diags0 , ** ptr_to_diags ; // Diagonal addresses
  floatT *  list_of_diags0, *  list_of_diags; // Diagonal values

  #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
  intT *  depMVidx0    , *  depMVidx    ; // For each subomdain/task, array containing the indexes to the first element of all the subdomains with dependences to this one (MV: matrix-vector multiplication)
  intT *  depMVsize0   , *  depMVsize   ; // For each subomdain/task, array containing the sizes of all the subdomains with dependences to this one (MV: matrix-vector multiplication)
  intT *  depMVite0    , *  depMVite    ; // For each subomdain/task, array containing the total number of subdomains with dependences to this one  (MV: matrix-vector multiplication)

  intT ** depMVidxPtr0 , ** depMVidxPtr ; // Auxiliary array of pointers for indexes
  intT ** depMVsizePtr0, ** depMVsizePtr; // Auxiliary array of pointers for sizes
  #endif

  #ifdef USING_MPI
  intT num_external;
  intT num_send_neighbors;

  intT total_to_be_sent;

  intT * external_index;
  intT * external_local_index;

  intT * neighbors  ;
  intT * recv_length;
  intT * send_length;

  intT   * elements_to_send0, * elements_to_send;
  floatT * send_buffer0     , * send_buffer     ;

  #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
  intT * depSBidx0 , * depSBidx ; // For each subomdain/task, array containing the indexes to the first element of all the subdomains       (SB: send buffer)
  intT * depSBsize0, * depSBsize; // For each subomdain/task, array containing the sizes of all the subdomains with dependences to this one (SB: send buffer)
  intT * depSBite0 , * depSBite ; // For each subomdain/task, array containing the total number of subdomains with dependences to this one  (SB: send buffer)

  intT ** depSBidxPtr0 , ** depSBidxPtr ; // Auxiliary array of pointers for indexes
  intT ** depSBsizePtr0, ** depSBsizePtr; // Auxiliary array of pointers for sizes
  #endif
  #endif
};


typedef struct HPC_Sparse_Matrix_struct HPC_Sparse_Matrix;


void destroyMatrix(HPC_Sparse_Matrix * & A);

#ifdef USING_SHAREDMEM_MPI
#ifndef SHAREDMEM_ALTERNATIVE
void destroySharedMemMatrix(HPC_Sparse_Matrix * & A);
#endif
#endif

#endif
