# @HEADER
# ***********************************************************************
#
#                      HLAM: Hybrid Linear Algebra Methods
#                  Copyright (c) 2022, Pedro J. Martinez-Ferrer
#                HPCCG: Simple Conjugate Gradient Benchmark Code
#                  Copyright (2006) Sandia Corporation
#
#  Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
#  license for use of this work by or on behalf of the U.S. Government.
#
#  BSD 3-Clause License
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
#  * Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#  Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
#                       pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)
#
#  Questions? Contact Michael A. Heroux (maherou@sandia.gov)
#
#  ************************************************************************
# @HEADER

# Simple hand-tuned makefile.  Modify as necessary for your environment.
# Questions? Contact Mike Heroux (maherou@sandia.gov).
#

# Compiling the Scalar operations library with NVIDIA compiler
INCDIR  := $(CUDA_INC)
LDLIBS   = -lculibos -lcudart_static -lpthread -lrt -ldl
NXX      = nvcc
NXXFLAGS = -gencode=arch=compute_70,code=sm_70 --use_fast_math -ccbin /usr/bin/gcc
NPPFLAGS = -I$(INCDIR)

libscalarAndVectorOp.a: scalarAndVectorOp.cu scalarAndVectorOp.hpp
	$(NXX) -c $< -o $@ $(NXXFLAGS) $(NPPFLAGS)


#
# 0) Specify compiler and linker:

# Intel compilation without MPI
# CXX=icpc
# LINKER=icpc

# Intel compilation with MPI
# CXX=mpiicpc
# LINKER=mpiicpc

# OmpSs-2 compilation with Mercurium (Intel native compiler and MPI)
# CXX=I_MPI_CXX=imcxx mpiicpc
# LINKER=I_MPI_CXX=imcxx mpiicpc

# OmpSs-2 compilation with Clang (Intel MPI)
# CXX=I_MPI_CXX=clang++ mpiicpc
# LINKER=I_MPI_CXX=clang++ mpiicpc

# OmpSs-2 compilation with Clang (OpenMPI)
 CXX=OMPI_CXX=clang++ mpic++
 LINKER=OMPI_CXX=clang++ mpic++

#TAMPI_INC=-I$(TAMPI_HOME)/include
#TAMPI_LIB=$(TAMPI_HOME)/lib/libtampi.a


# 1) Build with MPI or not?
#    If you want to run the program with MPI, make sure USE_MPI is set
#    to -DUSING_MPI

USE_MPI = -DUSING_MPI


# 2) MPI headers:
#    If you:
#    - Are building MPI mode (-DUSING_MPI is set above).
#    - Do not have the MPI headers installed in a default search directory and
#    - Are not using MPI compiler wrappers
#    Then specify the path to your MPI header file (include a -I)

#MPI_INC = -I/usr/MPICH/SDK.gcc/include


# 3) Specify C++ compiler optimization flags (if any)
#    Typically some reasonably high level of optimization should be used to
#    enhance performance.

#IA32 with GCC:
#CPP_OPT_FLAGS = -O3 -funroll-all-loops -malign-double
#CPP_OPT_FLAGS = -Ofast -xHOST -qopt-zmm-usage=high -inline-forceinline

#
# 4) MPI library:
#    If you:
#    - Are building MPI mode (-DUSING_MPI is set above).
#    - Do not have the MPI library installed a default search directory and
#    - Are not using MPI compiler wrappers for linking
#    Then specify the path to your MPI library (include -L and -l directives)

#MPI_LIB = -L/usr/MPICH/SDK.gcc/lib -lmpich

#
# 5) Build with OpenMP or not?
#    If you want to run the program with OpenMP, make sure USING_OMP is set
#    to -DUSING_OMP

#USE_MKL = -DUSING_MKL
#USE_OMP = -DUSING_OMP_FORK
#USE_OMP = -DUSING_OMP_TASK
#USE_OSS = -DUSING_OSS_TASK
USE_CUDA = -DUSING_CUDA
USE_NCCL = -DUSING_NCCL

#
# 6) OpenMP Compiler argument
#    GCC and Intel compilers require -fopenmp and -openmp, resp.  Other compilers may differ.

# MPI-only compilation
#OMP_FLAGS = -DWALL -march=native -Ofast -mprefer-vector-width=512 -fopenmp-simd

# Clang compiler
#OMP_FLAGS = -DWALL -march=native -Ofast -mprefer-vector-width=512 -fopenmp $(TAMPI_INC)
#OSS_FLAGS = -DWALL -march=native -Ofast -mprefer-vector-width=512 -fopenmp-simd -fompss-2 $(TAMPI_INC)

# Mercurium compiler
#OMP_FLAGS = -DWALL -Ofast -xHOST -qopenmp -qopt-zmm-usage=high $(TAMPI_INC)
#OSS_FLAGS = -DWALL --Wn,-Ofast --Wn,-xHOST --Wn,-qopt-zmm-usage=high --Wn,-qopenmp-simd --ompss-2 --no-copy-deps $(TAMPI_INC)

#MKL_FLAGS = -I${MKLROOT}/include -L${MKLROOT}/lib/intel64 -lmkl_core -lmkl_sequential -lmkl_intel_ilp64 -DMKL_ILP64 -DMKL_INT=int64_t -DMKL_ILP64

#
# 7) System libraries: (May need to add -lg2c before -lm)

SYS_LIB =-lm


#
# 8) Specify name if executable (optional):

# Pedro stuff
SIMD_FLAGS=-DSIMD=512

THP_FLAGS=-DTHP=2097152

# 9) CUDA

CUDA_FLAGS = -I$(CUDA_HOME)/include -L$(CUDA_HOME)/lib64 -L. -lcusparse -lcublas -lculibos -lcudart_static -lpthread -lrt -ldl -lscalarAndVectorOp
NCCL_FLAGS = -I$(NCCL_ROOT)/include -L$(NCCL_ROOT)/lib  -lnccl


################### Derived Quantities (no modification required) ##############

CXXFLAGS = $(CPP_OPT_FLAGS) $(OMP_FLAGS) $(MKL_FLAGS) $(OSS_FLAGS) $(SIMD_FLAGS) $(THP_FLAGS) $(USE_OMP) $(USE_MKL) $(USE_CUDA) $(USE_NCCL) $(USE_OSS) $(USE_MPI) $(MPI_INC)

LIB_PATHS= $(SYS_LIB)

CPP =   generate_matrix.cpp read_HPC_row.cpp \
	compute_residual.cpp mytimer.cpp dump_matlab_matrix.cpp \
	HPC_sparsemv.cpp waxpby.cpp ddot.cpp \
	HPCCG.cpp \
	make_local_matrix.cpp \
	exchange_externals.cpp  \
	exchange_externals_gpu.cpp \
	YAML_Element.cpp YAML_Doc.cpp

OBJ = $(CPP:.cpp=.o)

CG.bin: CG.o $(OBJ)
	$(LINKER) $(CPP_OPT_FLAGS) $(OMP_FLAGS) $(MKL_FLAGS) $(OSS_FLAGS) CG.o $(OBJ) $(TAMPI_LIB) $(LIB_PATHS) $(CUDA_FLAGS) $(NCCL_FLAGS) -o $@

.PHONY: clean
clean:
	rm -f *.o  *~ *.bin *.yaml 
