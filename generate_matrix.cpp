//@HEADER
// ************************************************************************
//
//                     HLAM: Hybrid Linear Algebra Methods
//                 Copyright (c) 2022, Pedro J. Martinez-Ferrer
//               HPCCG: Simple Conjugate Gradient Benchmark Code
//                 Copyright (2006) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// BSD 3-Clause License
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)
//
// Questions? Contact Michael A. Heroux (maherou@sandia.gov)
//
// ************************************************************************
//@HEADER


/////////////////////////////////////////////////////////////////////////

// Routine to read a sparse matrix, right hand side, initial guess,
// and exact solution (as computed by a direct solver).

/////////////////////////////////////////////////////////////////////////

// nrow - number of rows of matrix (on this processor)

#include <sys/mman.h>
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include "generate_matrix.hpp"


#include "PedroParameters.hpp"
#include "PedroFunctions.hpp"


void generate_matrix(intT ntasksUser, intT nx, intT ny, intT nz,
                     HPC_Sparse_Matrix ** A, floatT ** x, floatT ** b, floatT ** xexact)

{
#ifdef DEBUG
  intT debug = 1;
#else
  intT debug = 0;
#endif

#ifdef USING_MPI
  int  dummy;
  intT size , rank; // Number of MPI processes, My process ID

  MPI_Comm_size(MPI_COMM_WORLD, & dummy); size = dummy;
  MPI_Comm_rank(MPI_COMM_WORLD, & dummy); rank = dummy;
#else
  intT size = 1; // Serial case (not using MPI)
  intT rank = 0;
#endif

  *A = new HPC_Sparse_Matrix; // Allocate matrix struct and fill it

  (*A)->title = 0;

  // Set this bool to true if you want a 7-pt stencil instead of a 27 pt stencil
  bool use_7pt_stencil = true;
  if  (use_7pt_stencil) {(*A)->ratio =  7;}
  else                  {(*A)->ratio = 27;}
  const intT ratio = (*A)->ratio;

  intT local_nrow = nx*ny*nz; // This is the size of our subblock
  assert(local_nrow > iZero); // Must have something to work with

  // assert(local_nrow % simdSize == iZero); // local_nrow must be multiple of simdSize

  intT local_nnz  = ratio*local_nrow; // Approximately 27 nonzeros per row (except for boundary nodes)
  intT total_nrow = local_nrow*size ; // Total number of grid points in mesh
  intT total_nnz  = ratio*total_nrow; // Approximately 27 nonzeros per row (except for boundary nodes)

  intT start_row = local_nrow*rank          ; // Each processor gets a section of a chimney stack domain
  intT stop_row  = start_row+local_nrow-iOne;

  // Allocate arrays
  // (*A)->nnz_in_row0         = MMAPALLOC(local_nrow, intT   *, thp); (*A)->nnz_in_row         = MMAPALIGN((*A)->nnz_in_row0        , intT   *, thp);
  (*A)->ptr_to_cols_in_row0 = MMAPALLOC(local_nrow, intT   *, thp); (*A)->ptr_to_cols_in_row = MMAPALIGN((*A)->ptr_to_cols_in_row0, intT   *, thp); // It cannot be deleted because it is used to generate the local matrix (MPI)
  // (*A)->ptr_to_vals_in_row0 = MMAPALLOC(local_nrow, floatT *, thp); (*A)->ptr_to_vals_in_row = MMAPALIGN((*A)->ptr_to_vals_in_row0, floatT *, thp);
  // (*A)->ptr_to_diags0       = MMAPALLOC(local_nrow, floatT *, thp); (*A)->ptr_to_diags       = MMAPALIGN((*A)->ptr_to_diags0      , floatT *, thp);

  // *xexact = (floatT *) aligned_alloc(simdSize, local_nrow*floatTsize); // Done in main.cpp
  // *x      = (floatT *) aligned_alloc(simdSize, local_nrow*floatTsize); // Done in main.cpp
  // *b      = (floatT *) aligned_alloc(simdSize, local_nrow*floatTsize); // Done in main.cpp

  (*A)->rowIndex0     = MMAPALLOC(local_nrow + iOne, intT  , thp); (*A)->rowIndex     = MMAPALIGN((*A)->rowIndex0    , intT  , thp);
  (*A)->list_of_cols0 = MMAPALLOC(local_nnz        , intT  , thp); (*A)->list_of_cols = MMAPALIGN((*A)->list_of_cols0, intT  , thp);
  (*A)->list_of_vals0 = MMAPALLOC(local_nnz        , floatT, thp); (*A)->list_of_vals = MMAPALIGN((*A)->list_of_vals0, floatT, thp);
  (*A)->list_of_dOff0 = MMAPALLOC(local_nrow       , int8_t, thp); (*A)->list_of_dOff = MMAPALIGN((*A)->list_of_dOff0, int8_t, thp);

  #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
  (*A)->ntasksUser = ntasksUser;
  #else
  (*A)->ntasksUser = iOne;
  #endif

  // Main loop
  intT     true_nnz  = iZero;
  intT   * curindptr = (*A)->list_of_cols;
  floatT * curvalptr = (*A)->list_of_vals;

  intT nnzrow_accu = iZero;
  for (intT iz=iZero; iz<nz; iz++) {
    for (intT iy=iZero; iy<ny; iy++) {
      for (intT ix=iZero; ix<nx; ix++) {
        intT curlocalrow = iz*nx*ny+iy*nx+ix;
        intT currow = start_row+iz*nx*ny+iy*nx+ix;
        intT nnzrow = iZero;
        //(*A)->ptr_to_vals_in_row[curlocalrow] = curvalptr;
        (*A)->ptr_to_cols_in_row[curlocalrow] = curindptr;
        for (intT sz=-iOne; sz<=iOne; sz++) {
          for (intT sy=-iOne; sy<=iOne; sy++) {
            for (intT sx=-iOne; sx<=iOne; sx++) {
              intT curcol = currow+sz*nx*ny+sy*nx+sx;
//            Since we have a stack of nx by ny by nz domains , stacking in the z direction, we check to see
//            if sx and sy are reaching outside of the domain, while the check for the curcol being valid
//            is sufficient to check the z values:
              // This is because the MPI decomposition is carried out only along the z-direction
              if ((ix+sx>=iZero) && (ix+sx<nx) && (iy+sy>=iZero) && (iy+sy<ny) && (curcol>=iZero && curcol<total_nrow)) {
                if (!use_7pt_stencil || (sz*sz+sy*sy+sx*sx<=iOne)) { // This logic will skip points that are not part of a 7-pt or 27-pt stencil
                  if (curcol==currow) {
                    // (*A)->ptr_to_diags  [curlocalrow] = curvalptr; // Old code
                    (*A)->list_of_dOff  [curlocalrow] = nnzrow   ;
                    *curvalptr++ = 27.0;
                  }
                  else {
                    *curvalptr++ = -1.0;
                  }
                  *curindptr++ = curcol;
                  nnzrow++  ;
                  true_nnz++;
                }
              }
            } // end sx loop
          } // end sy loop
        } // end sz loop
        // (*A)->nnz_in_row[curlocalrow] = nnzrow;

        nnzrow_accu += nnzrow;
        (*A)->rowIndex[curlocalrow + iOne] = nnzrow_accu;

        // (*x)[curlocalrow] = 0.0;                             // Done in main.cpp
        // (*b)[curlocalrow] = 27.0 - ((double) (nnzrow-iOne)); // Done in main.cpp
        // (*xexact)[curlocalrow] = 1.0;                        // Done in main.cpp
      } // end ix loop
     } // end iy loop
  } // end iz loop

  (*A)->rowIndex[iZero] = iZero;
  (*A)->true_local_nnz = true_nnz;

  if (debug) cout << "Process "<<rank<<" of "<<size<<" has "<<local_nrow;

  if (debug) cout << " rows. Global rows "<< start_row
                  <<" through "<< stop_row <<endl;

  if (debug) cout << "Process "<<rank<<" of "<<size
                  <<" has "<<local_nnz<<" nonzeros."<<endl;

  (*A)->start_row  = start_row ;
  (*A)->stop_row   = stop_row  ;
  (*A)->total_nrow = total_nrow;
  (*A)->total_nnz  = total_nnz ;
  (*A)->local_nrow = local_nrow;
  (*A)->local_ncol = local_nrow; // For MPI problems, ncol > nrow: check "make_local_matrix.cpp"
  (*A)->local_nnz  = local_nnz ;

  return;
}
