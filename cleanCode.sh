#!/bin/bash

for file in `find . -name '*.cpp'`;
do
    emacs -Q --batch --eval \
        "(progn (find-file \"$file\") (whitespace-cleanup) (untabify (point-min) (point-max)) (save-buffer))"
done

for file in `find . -name '*.hpp'`;
do
    emacs -Q --batch --eval \
        "(progn (find-file \"$file\") (whitespace-cleanup) (untabify (point-min) (point-max)) (save-buffer))"
done
