//@HEADER
// ************************************************************************
//
//                     HLAM: Hybrid Linear Algebra Methods
//                 Copyright (c) 2022, Pedro J. Martinez-Ferrer
//               HPCCG: Simple Conjugate Gradient Benchmark Code
//                 Copyright (2006) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// BSD 3-Clause License
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)
//
// Questions? Contact Michael A. Heroux (maherou@sandia.gov)
//
// ************************************************************************
//@HEADER


#ifdef USING_MPI  // Compile this routine only if running in parallel
#include <sys/mman.h>
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
#include <map>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include "HPC_Sparse_Matrix.hpp"
#include "read_HPC_row.hpp"
#include "make_local_matrix.hpp"
#include "mytimer.hpp"


#include "PedroParameters.hpp"
#include "PedroFunctions.hpp"


//#define DEBUG
void make_local_matrix(HPC_Sparse_Matrix * __restrict__ const A)
{
  // Get MPI process info
  int  dummy;
  intT size , rank; // Number of MPI processes, My process ID
  MPI_Comm_size(MPI_COMM_WORLD, & dummy); size = dummy;
  MPI_Comm_rank(MPI_COMM_WORLD, & dummy); rank = dummy;

  // Needed variables
  std::map<intT, intT> externals;
  intT i, j, k;
  intT num_external = 0;
  double t0;

  int debug_details = 0; // Set to 1 for voluminous output
  #ifdef DEBUG
  int debug = 1;
  #else
  int debug = 0;
  #endif


  // Extract Matrix pieces
  intT start_row = A->start_row;
  intT stop_row = A->stop_row;
  intT total_nrow = A->total_nrow;
  intT total_nnz = A->total_nnz;
  intT local_nrow = A->local_nrow;
  intT local_nnz = A->local_nnz;
  intT *  rowIndex = A->rowIndex;
  intT ** ptr_to_cols_in_row = A->ptr_to_cols_in_row;


  // We need to convert the index values for the rows on this processor
  // to a local index space. We need to:
  // - Determine if each index reaches to a local value or external value
  // - If local, subtract start_row from index value to get local index
  // - If external, find out if it is already accounted for.
  //   - If so, "just" negate the index
  //   - otherwise, negate the index and also
  //     - add it to the list of external indices,
  //     - find out which processor owns the value.
  //     - Set up communication for sparse MV operation.


  ///////////////////////////////////////////
  // Scan the indices and transform to local
  ///////////////////////////////////////////

  if(debug) t0 = mytimer();


  // IntT constant for aligned memory allocation expressed in bytes
  const intT lMax_external = (intT) max_external;

  intT * external_index       = (intT *) aligned_alloc(simdSize, lMax_external*intTsize);
  intT * external_local_index = (intT *) aligned_alloc(simdSize, lMax_external*intTsize);

  A->external_index       = external_index      ;
  A->external_local_index = external_local_index;

  for (i=iZero; i<local_nrow; i++)
    {
      const intT nnz_in_row = rowIndex[i+iOne] - rowIndex[i];

      for (j=iZero; j<nnz_in_row; j++)
        {
          intT cur_col = ptr_to_cols_in_row[i][j];
          if (debug_details)
            cout << "Process "<<rank<<" of "<<size<<" getting index "
                 <<cur_col<<" in local row "<<i<<endl;
          if (start_row <= cur_col && cur_col <= stop_row)
            {
              ptr_to_cols_in_row[i][j] -= start_row; // Local value: substract start_row for local index
            }
          else // External value: must find out if we have already set up this point
            {
              if (externals.find(cur_col)==externals.end()) // cur_col not found in externals
                {
                    externals[cur_col] = num_external;

                    external_index[num_external] = cur_col;
                    // Mark index as external by (adding 1 and) negating it
                    ptr_to_cols_in_row[i][j] = - (ptr_to_cols_in_row[i][j] + iOne); // always a negative number

                    num_external++;

                    if(num_external > max_external)
                    {
                      cerr << "Must increase max_external in HPC_Sparse_Matrix.hpp"
                           <<endl;
                      abort();

                    }
                }
              else // cur_col already in external
                {
                  // Mark index as external by adding 1 and negating it
                  ptr_to_cols_in_row[i][j] = - (ptr_to_cols_in_row[i][j] + iOne); // always a negative number
                }
            }
        }
    }

  A->num_external = num_external;

  if (debug) {
    t0 = mytimer() - t0;
    cout << "            Time in transform to local phase = " << t0 << endl;
    cout << "Processor " << rank << " of " << size <<
               ": Number of external equations = " << num_external << endl;
  }

  ////////////////////////////////////////////////////////////////////////////
  // Go through list of externals to find out which processors must be accessed.
  ////////////////////////////////////////////////////////////////////////////

  if(debug) t0 = mytimer();


  // IntT constant for aligned memory allocation expressed in bytes
  const intT lSize = (intT) size;


  intT * tmp_buffer           = (intT *) aligned_alloc(simdSize, lSize*intTsize); // Temporal
  intT * global_index_offsets = (intT *) aligned_alloc(simdSize, lSize*intTsize); // Temporal

  for (i=iZero;i<size; i++) tmp_buffer[i] = iZero;  // First zero out

  tmp_buffer[rank] = start_row; // This is my start row


  // This call sends the start_row of each ith processor to the ith
  // entry of global_index_offset on all processors.
  // Thus, each processor know the range of indices owned by all
  // other processors.
  // Note:  There might be a better algorithm for doing this, but this
  //        will work...

  MPI_Allreduce(tmp_buffer, global_index_offsets, size, MPI_INTT,
                MPI_SUM, MPI_COMM_WORLD);


  // IntT constant for aligned memory allocation expressed in bytes
  const intT lNum_external = (intT) num_external;


  // Go through list of externals and find the processor that owns each
  intT * external_processor     = (intT *) aligned_alloc(simdSize, lNum_external*intTsize); // Temporal
  intT * new_external_processor = (intT *) aligned_alloc(simdSize, lNum_external*intTsize); // Temporal

  for (i=iZero; i< num_external; i++)
    {
      intT cur_col = external_index[i];
      for (intT j=size-iOne; j>=iZero; j--)
        if (global_index_offsets[j] <= cur_col)
          {
            external_processor[i] = j;
            break;
          }
    }


  free(global_index_offsets);

  if (debug) {
    t0 = mytimer() - t0;
    cout << "          Time in finding processors phase = " << t0 << endl;
  }


  ////////////////////////////////////////////////////////////////////////////
  // Sift through the external elements. For each newly encountered external
  // point assign it the next index in the sequence. Then look for other
  // external elements which are updated by the same node/processor and assign them the next
  // set of index numbers in the sequence (ie. elements updated by the same node/processor
  // have consecutive indices).
  ////////////////////////////////////////////////////////////////////////////

  if (debug) t0 = mytimer();

  intT count = local_nrow;
  for (i = iZero; i < num_external; i++) external_local_index[i] = -iOne;

  for (i = iZero; i < num_external; i++)
  {
    if(external_local_index[i] == -iOne)
    {
      external_local_index[i] = count++; // external_local_index starts at local_nrow = nx*ny*nz (instead of 0)

      for (j = i + iOne; j < num_external; j++)
      {
        if(external_processor[j] == external_processor[i])
          external_local_index[j] = count++;
      }
    }
  }

  if (debug) {
    t0 = mytimer() - t0;
    cout << "           Time in scanning external indices phase = " << t0 << endl;
  }
  if (debug) t0 = mytimer();


  for (i=iZero; i<local_nrow; i++)
    {
      const intT nnz_in_row = rowIndex[i+iOne] - rowIndex[i];

      for (j=iZero; j<nnz_in_row; j++)
        {
          if (ptr_to_cols_in_row[i][j]<iZero) // Change index values of externals (always negative by construction)
            {
              intT cur_col = - ptr_to_cols_in_row[i][j] - iOne; // this returns the original value of ptr_to_cols_in_row[i][j]
              ptr_to_cols_in_row[i][j] = external_local_index[externals[cur_col]];
            }
        }
    }


  for (i = iZero ; i < num_external; i++) new_external_processor[i] = iZero;

  for (i = iZero; i < num_external; i++)
      new_external_processor[external_local_index[i] - local_nrow] =
        external_processor[i]; //     external_processor[i] = f(external_index[i]      )
                               // new_external_processor[i] = f(external_local_index[i])
  if (debug) {
    t0 = mytimer() - t0;
    cout << "           Time in assigning external indices phase = " << t0 << endl;
  }

  if (debug_details)
    {
      for (i = iZero; i < num_external; i++)
        {
          cout << "Processor " << rank << " of " << size <<
            ": external processor["<< i<< "] = " << external_processor[i]<< endl;
          cout << "Processor " << rank << " of " << size <<
            ": new external processor["<< i<< "] = "
               << new_external_processor[i]<< endl;
        }
    }

  free(external_processor);

  ////////////////////////////////////////////////////////////////////////////
  ///
  // Count the number of neighbors from which we receive information to update
  // our external elements. Additionally, fill the array tmp_neighbors in the
  // following way:
  //      tmp_neighbors[i] = 0   ==>  No external elements are updated by
  //                              processor i.
  //      tmp_neighbors[i] = x   ==>  (x-1)/size elements are updated from
  //                              processor i.
  ///
  ////////////////////////////////////////////////////////////////////////////

  t0 = mytimer();

  intT * tmp_neighbors = (intT *) aligned_alloc(simdSize, lSize*intTsize); // Temporal

  for (i = iZero ; i < size ; i++) tmp_neighbors[i] = iZero;

  intT num_recv_neighbors = iZero;

  for (i = iZero; i < num_external; i++)
    {
      if (tmp_neighbors[new_external_processor[i]] == iZero)
        {
          num_recv_neighbors++;
          tmp_neighbors[new_external_processor[i]] = iOne;
        }
      tmp_neighbors[new_external_processor[i]] += size;
    }

  /// sum over all processors all the tmp_neighbors arrays ///

  MPI_Allreduce(tmp_neighbors, tmp_buffer, size, MPI_INTT, MPI_SUM,
                MPI_COMM_WORLD);

  /// decode the combined 'tmp_neighbors' (stored in tmp_buffer)
  //  array from all the processors

  intT num_send_neighbors = tmp_buffer[rank] % size;

  /// decode 'tmp_buffer[rank] to deduce total number of elements
  //  we must send

  intT total_to_be_sent = (tmp_buffer[rank] - num_send_neighbors) / size;

  //
  // Check to see if we have enough workspace allocated.  This could be
  // dynamically modified, but let's keep it simple for now...
  //

  if (num_send_neighbors > max_num_messages)
    {
      cerr << "Must increase max_num_messages in HPC_Sparse_Matrix.hpp"
           <<endl;
      cerr << "Must be at least " <<  num_send_neighbors <<endl;
      abort();
    }

  if (total_to_be_sent > max_external )
    {
      cerr << "Must increase max_external in HPC_Sparse_Matrix.hpp"
           <<endl;
      cerr << "Must be at least " << total_to_be_sent <<endl;
      abort();
    }


  free(tmp_neighbors);

  if (debug) {
    t0 = mytimer() - t0;
    cout << "           Time in finding neighbors phase = " << t0 << endl;
  }
  if (debug) cout << "Processor " << rank << " of " << size <<
               ": Number of send neighbors = " << num_send_neighbors << endl;

  if (debug) cout << "Processor " << rank << " of " << size <<
               ": Number of receive neighbors = " << num_recv_neighbors << endl;

  if (debug) cout << "Processor " << rank << " of " << size <<
               ": Total number of elements to send = " << total_to_be_sent << endl;

  if (debug) MPI_Barrier(MPI_COMM_WORLD);

  /////////////////////////////////////////////////////////////////////////
  ///
  // Make a list of the neighbors that will send information to update our
  // external elements (in the order that we will receive this information).
  ///
  /////////////////////////////////////////////////////////////////////////

  intT* recv_list = (intT*) aligned_alloc(simdSize, lMax_external*intTsize); // Temporal

  j = iZero;
  recv_list[j++] = new_external_processor[iZero];
  for (i = iOne; i < num_external; i++) {
    if (new_external_processor[i] != new_external_processor[i - iOne]) {
      recv_list[j++] = new_external_processor[i];
    }
  }

  //
  // Send a 0 length message to each of our recv neighbors
  //


  // IntT constant for aligned memory allocation expressed in bytes
  const intT lNum_send_neighbors = (intT) num_send_neighbors;


  intT * send_list = (intT *) aligned_alloc(simdSize, lNum_send_neighbors*intTsize); // Temporal
  for (i = iZero ; i < num_send_neighbors; i++) send_list[i] = iZero;

  //
  //  first post receives, these are immediate receives
  //  Do not wait for result to come, will do that at the
  //  wait call below.
  //
  intT MPI_MY_TAG = 99;


  // IntT constant for aligned memory allocation expressed in bytes
  const intT lMax_num_messages = (intT) max_num_messages;


  MPI_Request * request = (MPI_Request *) aligned_alloc(simdSize, lMax_num_messages*sizeof(MPI_Request)); // Tepmporal: it is not num_send_neighbours
  for (i = iZero; i < num_send_neighbors; i++)                                                              // because it can change (see below)
    {
      MPI_Irecv(tmp_buffer+i, iOne, MPI_INTT, MPI_ANY_SOURCE, MPI_MY_TAG,
                MPI_COMM_WORLD, request+i);
    }

  // send messages

  for (i = iZero; i < num_recv_neighbors; i++)
      MPI_Send(tmp_buffer+i, iOne, MPI_INTT, recv_list[i], MPI_MY_TAG,
               MPI_COMM_WORLD);
  ///
   // Receive message from each send neighbor to construct 'send_list'.
   ///

  MPI_Status status;
  for (i = iZero; i < num_send_neighbors; i++)
    {
      if ( MPI_Wait(request+i, &status) )
        {
          cerr << "MPI_Wait error\n"<<endl;
          exit(-1);
        }
      send_list[i] = status.MPI_SOURCE;
  }


  free(tmp_buffer);



  /////////////////////////////////////////////////////////////////////////
  ///
  //  Compare the two lists. In most cases they should be the same.
  //  However, if they are not then add new entries to the recv list
  //  that are in the send list (but not already in the recv list).
  ///
  /////////////////////////////////////////////////////////////////////////

  for (j = iZero; j < num_send_neighbors; j++)
    {
      intT found = iZero;
      for (i = iZero; i < num_recv_neighbors; i++)
        {
          if (recv_list[i] == send_list[j])
            found = iOne;
        }

      if (found == iZero) {
        if (debug) cout << "Processor " << rank << " of " << size <<
                     ": recv_list[" << num_recv_neighbors <<"] = "
                        << send_list[j] << endl;
        recv_list[num_recv_neighbors] = send_list[j];
        (num_recv_neighbors)++;

      }
    }

  free(send_list);
  num_send_neighbors = num_recv_neighbors;

  if (num_send_neighbors > max_num_messages)
    {
      cerr << "Must increase max_external in HPC_Sparse_Matrix.hpp"
           <<endl;
      abort();
    }

  /////////////////////////////////////////////////////////////////////////
  /// Start filling HPC_Sparse_Matrix struct
  /////////////////////////////////////////////////////////////////////////

  //
  // Create 'new_external' which explicitly put the external elements in the
  // order given by 'external_local_index'
  //

  intT * new_external = (intT *) aligned_alloc(simdSize, lNum_external*intTsize); // Temporary

  for (i = iZero; i < num_external; i++) {
    new_external[external_local_index[i] - local_nrow] = external_index[i]; // as in new_external_processor
  }

  /////////////////////////////////////////////////////////////////////////
  //
  // Send each processor the global index list of the external elements in the
  // order that I will want to receive them when updating my external elements
  //
  /////////////////////////////////////////////////////////////////////////


  // IntT constant for aligned memory allocation expressed in bytes
  const intT lNum_recv_neighbors = (intT) num_recv_neighbors;


  intT * lengths = (intT *) aligned_alloc(simdSize, lNum_recv_neighbors*intTsize); // Temporal

  MPI_MY_TAG++;

  // First post receives

  for (i = iZero; i < num_recv_neighbors; i++)
    {
      intT partner    = recv_list[i];
      MPI_Irecv(lengths+i, iOne, MPI_INTT, partner, MPI_MY_TAG, MPI_COMM_WORLD,
                request+i);
    }


  // IntT constant for aligned memory allocation expressed in bytes
  const intT lMax_num_neighbors = (intT) max_num_neighbors;


  intT * neighbors   = (intT *) aligned_alloc(simdSize, lMax_num_neighbors*intTsize);
  intT * recv_length = (intT *) aligned_alloc(simdSize, lMax_num_neighbors*intTsize);
  intT * send_length = (intT *) aligned_alloc(simdSize, lMax_num_neighbors*intTsize);

  A->neighbors   = neighbors  ;
  A->recv_length = recv_length;
  A->send_length = send_length;

  j = iZero;
  for (i = iZero; i < num_recv_neighbors; i++)
    {
      intT start     = j    ;
      intT newlength = iZero;

      // go through list of external elements until updating
      // processor changes

      while ((j < num_external) &&
             (new_external_processor[j] == recv_list[i]))
        {
          newlength++;
          j++;
          if (j == num_external) break;
        }

      recv_length[i] = newlength;
      neighbors[i]  = recv_list[i];

      intT length = j - start;
      MPI_Send(&length, iOne, MPI_INTT, recv_list[i], MPI_MY_TAG, MPI_COMM_WORLD);
    }

  // Complete the receives of the number of externals

  for (i = iZero; i < num_recv_neighbors; i++)
    {
      if ( MPI_Wait(request+i, &status) )
        {
          cerr << "MPI_Wait error\n"<<endl;
          exit(-1);
        }
      send_length[i] = lengths[i];
    }
  free(lengths);


  ///////////////////////////////////////////////////////////////////
  // Build "elements_to_send" list.  These are the x elements I own
  // that need to be sent to other processors.
  ///////////////////////////////////////////////////////////////////


  A->total_to_be_sent = total_to_be_sent;


  // IntT constant for aligned memory allocation expressed in bytes
  const intT lTotal_to_be_sent = (intT) total_to_be_sent;

  A->elements_to_send0 = MMAPALLOC(lTotal_to_be_sent, intT  , thp); A->elements_to_send = MMAPALIGN(A->elements_to_send0, intT  , thp);
  A->send_buffer0      = MMAPALLOC(lTotal_to_be_sent, floatT, thp); A->send_buffer      = MMAPALIGN(A->send_buffer0     , floatT, thp);

  intT * elements_to_send = A->elements_to_send;

  for (i = iZero ; i < total_to_be_sent; i++ ) elements_to_send[i] = iZero;

  MPI_MY_TAG++;

  j = iZero;
  for (i = iZero; i < num_recv_neighbors; i++)
    {
      MPI_Irecv(elements_to_send+j, send_length[i], MPI_INTT, neighbors[i],
                MPI_MY_TAG, MPI_COMM_WORLD, request+i);
      j += send_length[i];
    }

  j = iZero;
  for (i = iZero; i < num_recv_neighbors; i++)
    {
      intT start     = j    ;
      intT newlength = iZero;

      // Go through list of external elements
      // until updating processor changes.  This is redundant, but
      // saves us from recording this information.

      while ((j < num_external) &&
             (new_external_processor[j] == recv_list[i])) {

        newlength++;
        j++;
        if (j == num_external) break;
      }
      MPI_Send(new_external+start, j-start, MPI_INTT, recv_list[i],
               MPI_MY_TAG, MPI_COMM_WORLD);
    }

  // receive from each neighbor the global index list of external elements

  for (i = iZero; i < num_recv_neighbors; i++)
    {
      if ( MPI_Wait(request+i, &status) )
        {
          cerr << "MPI_Wait error\n"<<endl;
          exit(-1);
        }
  }

  /// replace global indices by local indices ///
  for (i = iZero; i < total_to_be_sent; i++) elements_to_send[i] -= start_row;


  ////////////////
  // Finish up !!
  ////////////////

  A->num_send_neighbors = num_send_neighbors       ;
  A->local_ncol         = local_nrow + num_external;

  free(recv_list);
  free(new_external);
  free(new_external_processor);
  free(request);

  return;
}
#endif // USING_MPI
