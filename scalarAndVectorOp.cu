#include <sys/mman.h>
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
#include <cstdio>
#include <cstdlib>

#include <cmath>

#include <cuda_runtime_api.h> // cudaMalloc, cudaMemcpy, etc.
#include <cuda_runtime.h>
#include <cuda.h>
#include <math.h>
#include "scalarAndVectorOp.hpp"

/*__global__ void kernel_element_buffer_serial(double * d_send_buffer, double * d_x, int64_t * d_elements_to_send, int64_t d_idxS, int64_t d_n_send)
    {
    for(int j = 0; j < d_n_send; ++j)
      { 
      d_send_buffer[d_idxS+j] = d_x[d_elements_to_send[d_idxS+j]] ;
      }
    }
*/    
__global__ void kernel_element_buffer(double * d_send_buffer, double * d_x, int64_t * d_elements_to_send, int64_t d_idxS, int64_t d_n_send)
    {
     int j = blockIdx.x * blockDim.x + threadIdx.x;
     
      if (j < d_n_send)
      {
      d_send_buffer[d_idxS+j] = d_x[d_elements_to_send[d_idxS+j]] ;
      }  
    }

__global__ void kernel_mult(double * d_a, double * d_b, double *  d_multP, double * d_multM)
    {
    *d_multP =   *d_a * *d_b   ;
    *d_multM = - *d_multP     ;
    }    

__global__ void kernel_divide(double * d_a, double * d_b, double *  d_ratioP, double * d_ratioM)
    {
    *d_ratioP =   *d_a / *d_b   ;
    *d_ratioM = - *d_ratioP     ;
    }
    
 __global__ void kernel_divideM(double * d_a, double * d_b, double * d_ratioM)
    {
    *d_ratioM = -1.0 * (*d_a / *d_b)   ;
    }

__global__ void kernel_divideP(double * d_a, double * d_b, double *  d_ratioP)
    {
    *d_ratioP =   *d_a / *d_b   ;
    }
    
__global__ void kernel_sqrt_inv(double * d_a, double * d_sqrt_inv)
    {
    *d_sqrt_inv = 1.0 / sqrt(*d_a)  ;
    }         

__global__ void kernel_sqrt(double * d_a, double * d_sqrt)
    {
    *d_sqrt = sqrt(*d_a)  ;
    }
   

__global__ void kernel_jac(double* a, double* b, double* c,  double * d, int N)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (idx < N)
    {
    d[idx] =  ( b[idx] - c[idx] ) / a[idx]  ;
    }    
}
/* 
__global__ void kernel_jacGS(double* a, double* b, double* c,  double * diff, double * res, int N)
{
        *res  = 0;
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (idx < N)
    {
    diff[idx]  =  ( b[idx] - c[idx] ) / a[idx]  ;
    }
    if (idx < N)
    {    
       *res   +=    diff[idx] * diff[idx] 	;
    }   
} 


__global__ void kernel_jacGS_old(double* a, double* b, double* c,  double * diff,  double * x , double * xNew, int N)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (idx < N)
    {
    diff[idx] 	 =  ( b[idx] - c[idx] ) / a[idx]  ;
    xNew[idx ]   = 	x[idx] + diff[idx]        ;	
    }
  
}

__global__ void kernel_dot_product(double * x, double * y, double * dot, unsigned int N)
{
	unsigned int index = threadIdx.x + blockDim.x*blockIdx.x;
	unsigned int stride = blockDim.x*gridDim.x;

	__shared__ float cache[256];

	double temp = 0.0;
	while(index < N){
		temp += x[index]*y[index];

		index += stride;
	}

	cache[threadIdx.x] = temp;

	__syncthreads();

	// reduction
	unsigned int i = blockDim.x/2;
	while(i != 0){
		if(threadIdx.x < i){
			cache[threadIdx.x] += cache[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}


	if(threadIdx.x == 0){
		atomicAdd(dot, cache[0]);
	}
}
*/

__global__ void kernel_vecAdd(double* a, double* b, double* c, int N)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (idx < N)
    {
    c[idx] = a[idx] + b[idx]; // Perform the multiplication and accumulation
    }    
}


__global__ void kernel_vecDot2(double* a, double* b, double* c, int N)  //d_x , d_diag , d_Ax , N
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (idx < N)
    {
    c[idx] += a[idx] * b[idx]; // Perform the multiplication and accumulation for diagonal of MSR ( d_Ax += d_x * d_diag )
    }    
}

__global__ void kernel_jacGS_csr(double * a, double * b, double * c,  double * diff,  double * x , double * xNew, int N)
{
    int idx    = threadIdx.x + blockDim.x*blockIdx.x;
		
    if (idx < N)
    {
       	diff[idx]  =  ( b[idx] - c[idx] ) / a[idx];			// diff[ ]  = ( b[ ] - Ax[ ] ) / diag[ ] 
       	xNew[idx ] = 	x[idx] + diff[idx]        ;			// xNew[ ]   = x[ ] + diff[ ]
    }
}	

__global__ void kernel_residual(double * diff,  double * dot, int N )
{
	unsigned int  idx    = threadIdx.x + blockDim.x*blockIdx.x;
	unsigned int stride = blockDim.x*gridDim.x;
	 
	__shared__ float cache[256];
	
	double temp = 0.0;
	while(idx < N){
	
		temp  	  += 	diff[idx]*diff[idx]	  ;			// residual += diff[ ] * diff[ ]
		idx   	  += 	stride			  ;
	}

	cache[threadIdx.x] = temp;

	__syncthreads();

	// reduction
	unsigned int i = blockDim.x/2;
	while(i != 0){
		if(threadIdx.x < i){
			cache[threadIdx.x] += cache[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}


	if(threadIdx.x == 0){
		atomicAdd(dot, cache[0]);
	}
}


__global__ void kernel_jac_csr2(double * a, double * b, double * c,  double * x , double * xNew, double * dot, int N)
{
	unsigned int idx    = threadIdx.x + blockDim.x*blockIdx.x;
//	unsigned int stride = blockDim.x*gridDim.x;
	float diff ;
//	*dot = 0.0 ;
//	Calculation of residual 
	__shared__ float cache[512];

	double temp = 0.0;
	if(idx < N){
	
           	diff       =  ( b[idx] - c[idx] ) / a[idx]	;			// diff[ ]  = ( b[ ] - Ax[ ] ) / diag[ ] 
           	xNew[idx ] = 	x[idx] + diff		        ;			// xNew[ ]   = x[ ] + diff[ ]	
		temp  	  += 	diff*diff	 		;			// residual += diff[ ] * diff[ ]
//		idx   	  += 	stride			 	;
	}

	cache[threadIdx.x] = temp;

	__syncthreads();

	// reduction
	unsigned int stride = blockDim.x/2;
	while(stride != 0){
		if(threadIdx.x < stride){
			cache[threadIdx.x] += cache[threadIdx.x + stride];
		}
		__syncthreads();
		stride /= 2;
	}


	if(threadIdx.x == 0){
		atomicAdd(dot, cache[0]);
	}
}

__global__ void kernel_gs_csr2(double * a, double * b, double * c,  double * x , double * xNew, double * dot, int N)
{
	unsigned int idx    = threadIdx.x + blockDim.x*blockIdx.x;
//	unsigned int stride = blockDim.x*gridDim.x;
	float diff ;
	*dot = 0.0 ;
//	Calculation of residual 
	__shared__ float cache[512];

	double temp = 0.0;
	if(idx < N){
	
           	diff       =  ( b[idx] - c[idx] ) / a[idx]	;			// diff[ ]  = ( b[ ] - Ax[ ] ) / diag[ ] 
           	xNew[idx ] = 	x[idx] + diff		        ;			// xNew[ ]   = x[ ] + diff[ ]	
		temp  	  += 	diff*diff	 		;			// residual += diff[ ] * diff[ ]
//		idx   	  += 	stride			 	;
	}

	cache[threadIdx.x] = temp/2.0;

	__syncthreads();

	// reduction
	unsigned int stride = blockDim.x/2;
	while(stride != 0){
		if(threadIdx.x < stride){
			cache[threadIdx.x] += cache[threadIdx.x + stride];
		}
		__syncthreads();
		stride /= 2;
	}


	if(threadIdx.x == 0){
		atomicAdd(dot, cache[0]);
	}
//	*dot /= 2;
}

__global__ void kernel_gs_csr3(double * a, double * b, double * matA,  double * x , double * xNew, double * dot, int N)
{
	unsigned int idx    = threadIdx.x + blockDim.x*blockIdx.x;
//	unsigned int stride = blockDim.x*gridDim.x;
	float diff ;
	*dot = 0.0 ;
//	Calculation of residual 
	__shared__ float cache[512];

	double temp = 0.0;
	if(idx < N){
//	        c[idx]     =				;
           	diff       =  ( b[idx] - b[idx] ) / a[idx]	;			// diff[ ]  = ( b[ ] - Ax[ ] ) / diag[ ] 
           	xNew[idx ] = 	x[idx] + diff		        ;			// xNew[ ]   = x[ ] + diff[ ]	
		temp  	  += 	diff*diff	 		;			// residual += diff[ ] * diff[ ]
//		idx   	  += 	stride			 	;
	}

	cache[threadIdx.x] = temp/2.0;

	__syncthreads();

	// reduction
	unsigned int stride = blockDim.x/2;
	while(stride != 0){
		if(threadIdx.x < stride){
			cache[threadIdx.x] += cache[threadIdx.x + stride];
		}
		__syncthreads();
		stride /= 2;
	}


	if(threadIdx.x == 0){
		atomicAdd(dot, cache[0]);
	}
//	*dot /= 2;
}

__global__ void kernel_jacGS_diag(double * a, double * b, double * c,  double * diff,  double * x , double * xNew, double * dot, int N) // d_diags, d_b, d_Ax, d_diff, d_x, d_xNew, d_rTL, ncol
{
	unsigned int idx    = threadIdx.x + blockDim.x*blockIdx.x;
	unsigned int stride = blockDim.x*gridDim.x;

//	Calculation of residual 
	__shared__ float cache[256];
	
	double temp = 0.0;
	while(idx < N){
/*    		
    		c[idx] 	  +=   a[idx] * x[idx] 		  ;			//  Ax 	    += x * d_diag 
           	diff[idx]  =  ( b[idx] - c[idx] ) / a[idx];			//  diff[ ]  = ( b[ ] - Ax[ ] ) / diag[ ] 
           	xNew[idx ] = 	x[idx] + diff[idx]        ;			// xNew[ ]   = x[ ] + diff[ ]	
		temp  	  += 	diff[idx]*diff[idx]	  ;			// residual += diff[ ] * diff[ ]
		idx   	  += 	stride			  ;
*/		
           	// write a temp = x[idx ] 
           	xNew[idx]  =  ( b[idx] - c[idx] ) / a[idx];			//  xNew[ ]   = ( b[ ] - Ax[ ] ) / diag[ ] 
           	diff[idx]  = 	 xNew[idx]- x[idx ]   	  ; // -temp		//  diff[ ]   =  xNew[ ] -x[]	
		temp  	  += 	diff[idx]*diff[idx]	  ;			//  residual += diff[ ] * diff[ ]
		idx   	  += 	stride			  ;
		
	}

	cache[threadIdx.x] = temp;

	__syncthreads();

	// reduction
	unsigned int i = blockDim.x/2;
	while(i != 0){
		if(threadIdx.x < i){
			cache[threadIdx.x] += cache[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}


	if(threadIdx.x == 0){
		atomicAdd(dot, cache[0]);
	}

}


__global__ void kernel_jacobi_csr(double * a, double * b, double * c,  double * diff,  double * x , double * xNew, double * dot, int N)
{
	unsigned int idx    = threadIdx.x + blockDim.x*blockIdx.x;
	unsigned int stride = blockDim.x*gridDim.x;


//	Calculation of residual 
	__shared__ float cache[256];

	double temp = 0.0;
	while(idx < N){
	
           	diff[idx]  =  ( b[idx] - c[idx] ) / a[idx];			// diff[ ]  = ( b[ ] - Ax[ ] ) / diag[ ] 
           	xNew[idx ] = 	x[idx] + diff[idx]        ;			// xNew[ ]   = x[ ] + diff[ ]	
		temp  	  += 	diff[idx]*diff[idx]	  ;			// residual += diff[ ] * diff[ ]
		idx   	  += 	stride			  ;
	}

	cache[threadIdx.x] = temp;

	__syncthreads();

	// reduction
	unsigned int i = blockDim.x/2;
	while(i != 0){
		if(threadIdx.x < i){
			cache[threadIdx.x] += cache[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}


	if(threadIdx.x == 0){
		atomicAdd(dot, cache[0]);
	}

}

__global__ void kernel_jacobi_diag(double * a, double * b, double * c,  double * diff,  double * x , double * xNew, double * dot, int N) // d_diags, d_b, d_Ax, d_diff, d_x, d_xNew, d_rTL, ncol
{
	unsigned int idx    = threadIdx.x + blockDim.x*blockIdx.x;
	unsigned int stride = blockDim.x*gridDim.x;

//	Calculation of residual 
	__shared__ float cache[256];
	
	double temp = 0.0;
	while(idx < N){
    		
    		c[idx] 	  +=   a[idx] * x[idx] 		  ;			//  Ax 	    += x * d_diag 
           	diff[idx]  =  ( b[idx] - c[idx] ) / a[idx];			//  diff[ ]  = ( b[ ] - Ax[ ] ) / diag[ ] 
           	xNew[idx ] = 	x[idx] + diff[idx]        ;			// xNew[ ]   = x[ ] + diff[ ]	
		temp  	  += 	diff[idx]*diff[idx]	  ;			// residual += diff[ ] * diff[ ]
		idx   	  += 	stride			  ;
	}

	cache[threadIdx.x] = temp;

	__syncthreads();

	// reduction
	unsigned int i = blockDim.x/2;
	while(i != 0){
		if(threadIdx.x < i){
			cache[threadIdx.x] += cache[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}


	if(threadIdx.x == 0){
		atomicAdd(dot, cache[0]);
	}

}

/*
__global__ void kernel_GS_csr(double * a, double * b, double * c,  double * diff,  double * x , double * dot, int N)
{
	unsigned int idx    = threadIdx.x + blockDim.x*blockIdx.x;
	unsigned int stride = blockDim.x*gridDim.x;


//	Calculation of residual 
	__shared__ float cache[256];

	double temp = 0.0;
	while(idx < N){
	
           	diff[idx]  =  ( b[idx] - c[idx] ) / a[idx];			//  diff[ ]  = ( b[ ] - Ax[ ] ) / diag[ ] 
           	x[idx ]    = 	x[idx] + diff[idx]        ;			// xNew[ ]   = x[ ] + diff[ ]	
		temp      += 	diff[idx]*diff[idx]	  ;			// residual += diff[ ] * diff[ ]
		idx       += 	stride			  ;
	}

	cache[threadIdx.x] = temp*0.5;						//should be divide by two

	__syncthreads();

	// reduction
	unsigned int i = blockDim.x/2;
	while(i != 0){
		if(threadIdx.x < i){
			cache[threadIdx.x] += cache[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}


	if(threadIdx.x == 0){
		atomicAdd(dot, cache[0]);
	}

}
*/
__global__ void kernel_GS_diag(double * a, double * b, double * c,  double * diff,  double * x , double * dot, int N)
{
	unsigned int idx    = threadIdx.x + blockDim.x*blockIdx.x;
	unsigned int stride = blockDim.x*gridDim.x;


//	Calculation of residual 
	__shared__ float cache[256];

	double temp = 0.0;
	while(idx < N){
	
    		c[idx] 	  +=    a[idx] * x[idx]		  ;			//  Ax 	    += x * d_diag 
           	diff[idx]  =  ( b[idx] - c[idx] ) / a[idx];			//  diff[ ]  = ( b[ ] - Ax[ ] ) / diag[ ]
           	x[idx ]    = 	x[idx] + diff[idx]        ;			// xNew[ ]   = x[ ] + diff[ ]	
		temp      +=    diff[idx]*diff[idx]	  ;			// residual += diff[ ] * diff[ ]
		idx       +=    stride			  ;
	}

	cache[threadIdx.x] = temp*0.5;						//should be divide by two

	__syncthreads();

	// reduction
	unsigned int i = blockDim.x/2;
	while(i != 0){
		if(threadIdx.x < i){
			cache[threadIdx.x] += cache[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}


	if(threadIdx.x == 0){
		atomicAdd(dot, cache[0]);
	}

}


//FUNCTIONS

 int element_buffer(cudaStream_t & stream, double * d_send_buffer, double * d_x, int64_t * d_elements_to_send, int64_t  d_idxS, int64_t  d_n_send)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(d_n_send) / thr_per_blk );
     kernel_element_buffer<<<blk_in_grid, thr_per_blk, 0, stream>>>(d_send_buffer, d_x, d_elements_to_send, d_idxS, d_n_send);
 return (0);
 }

 int mult_cuda(cudaStream_t & stream, double * d_a, double * d_b, double * d_multP, double * d_multM)
 {
     kernel_mult<<<1, 1, 0, stream>>>(d_a, d_b, d_multP, d_multM);
 return (0);
 }

 int divide_cuda(cudaStream_t & stream, double * d_a, double * d_b, double * d_ratioP, double * d_ratioM)
 {
     kernel_divide<<<1, 1, 0, stream>>>(d_a, d_b, d_ratioP, d_ratioM);
 return (0);
 }

 int divide_cudaM(cudaStream_t & stream, double * d_a, double * d_b, double * d_ratioM)
 {
     kernel_divideM<<<1, 1, 0, stream>>>(d_a, d_b, d_ratioM);
 return (0);
 }
 
 int divide_cudaP(cudaStream_t & stream, double * d_a, double * d_b, double * d_ratioP)
 {
     kernel_divideP<<<1, 1, 0, stream>>>(d_a, d_b, d_ratioP);
 return (0);
 } 

  int sqrt_inv_cuda(cudaStream_t & stream, double * d_a, double * d_sqrt_inv)
 {
     kernel_sqrt_inv<<<1, 1, 0, stream>>>( d_a, d_sqrt_inv);
 return (0);
 }
 
  int sqrt_cuda(cudaStream_t & stream, double * d_a, double * d_sqrt)
 {
     kernel_sqrt_inv<<<1, 1, 0, stream>>>( d_a, d_sqrt);
 return (0);
 }
 
  int vecDot2(cudaStream_t & stream, double * dev_a, double * dev_b, double * dev_c, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_vecDot2<<< blk_in_grid, thr_per_blk, 0, stream>>>(dev_a, dev_b, dev_c, N);
  return (0);
 }

  int jac(cudaStream_t & stream, double * dev_a, double * dev_b, double * dev_c, double * dev_d, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_jac<<< blk_in_grid, thr_per_blk, 0, stream>>>(dev_a, dev_b, dev_c, dev_d, N);
  return (0);
 }
 /* 
  int jacGS_old(cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_diff,  double * d_x, double * d_xNew, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_jacGS_old<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_c, d_diff, d_x, d_xNew, N);
  return (0);
 } */
 
   int vecAdd(cudaStream_t & stream, double * dev_a, double * dev_b, double * dev_c, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_vecAdd<<< blk_in_grid, thr_per_blk, 0, stream>>>(dev_a, dev_b, dev_c, N);
  return (0);
 }
 
/*  int jacGS_csr (cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_diff,  double * d_x, double * d_xNew, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_jacGS_csr<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_c, d_diff, d_x, d_xNew, N);
  return (0);
 }
*/    
  int jacGS_csr (cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_diff,  double * d_x, double * d_xNew, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_jacGS_csr<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_c, d_diff, d_x, d_xNew, N);
  return (0);
 }
  
  int jac_csr2 (cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_x, double * d_xNew, double * d_prod, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_jac_csr2<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_c, d_x, d_xNew, d_prod, N);
  return (0);
 }   

  int gs_csr2 (cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_x, double * d_xNew, double * d_prod, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_gs_csr2<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_c, d_x, d_xNew, d_prod, N);
  return (0);
 }
 
  int gs_csr3 (cudaStream_t & stream, double * d_a, double * d_b, double * d_matA, double * d_x, double * d_xNew, double * d_prod, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_gs_csr3<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_matA, d_x, d_xNew, d_prod, N);
  return (0);
 } 

  int jacGS_diag(cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_diff,  double * d_x, double * d_xNew, double * d_prod, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_jacGS_diag<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_c, d_diff, d_x, d_xNew, d_prod, N);
  return (0);
 }

  int jacobi_csr (cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_diff,  double * d_x, double * d_xNew, double * d_prod, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_jacobi_csr<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_c, d_diff, d_x, d_xNew, d_prod, N);
  return (0);
 } 

  int jacobi_diag(cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_diff,  double * d_x, double * d_xNew, double * d_prod, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_jacobi_diag<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_c, d_diff, d_x, d_xNew, d_prod, N);
  return (0);
 } 
  int residual(cudaStream_t & stream, double * d_diff,  double * d_prod, int N )
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_residual<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_diff, d_prod, N);
  return (0);
 }   
/* 
  int GS_csr (cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_diff,  double * d_x, double * d_prod, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_GS_csr<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_c, d_diff, d_x, d_prod, N);
  return (0);
 }
*/ 
   int GS_diag (cudaStream_t & stream, double * d_a, double * d_b, double * d_c, double * d_diff,  double * d_x, double * d_prod, int N)
 {
     int thr_per_blk = 256;
     int blk_in_grid = ceil( float(N) / thr_per_blk );
     kernel_GS_diag<<< blk_in_grid, thr_per_blk, 0, stream>>>(d_a, d_b, d_c, d_diff, d_x, d_prod, N);
  return (0);
 }
 
/*  int dot_product(cudaStream_t & stream, double * d_x, double * d_y, double * d_prod, int N)
 {
    dim3 gridSize = 256;
    dim3 blockSize = 256;
    kernel_dot_product<<<gridSize, blockSize, 0, stream>>>(d_x, d_y, d_prod, N);
  return (0);
 } */

 
	
