//@HEADER
// ************************************************************************
//
//                     HLAM: Hybrid Linear Algebra Methods
//                 Copyright (c) 2022, Pedro J. Martinez-Ferrer
//               HPCCG: Simple Conjugate Gradient Benchmark Code
//                 Copyright (2006) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// BSD 3-Clause License
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)
//
// Questions? Contact Michael A. Heroux (maherou@sandia.gov)
//
// ************************************************************************
//@HEADER


#ifdef USING_MPI  // Compile this routine only if running in parallel
#include <iostream>
using std::cerr;
using std::cout;
using std::endl;
#include <cstdlib>
#include <cstdio>
#include "exchange_externals.hpp"

#if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
#include <TAMPI.h>
#endif

#include "PedroParameters.hpp"


#undef DEBUG
void exchange_externals(const intT MPItag                             ,
                        const HPC_Sparse_Matrix * __restrict__ const A,
                              floatT *            __restrict__ const x)
{
  const intT nrow          = A->local_nrow        ,
             num_send_recv = A->num_send_neighbors;

  const intT   * __restrict__ const neighbors        = A->neighbors       ,
               * __restrict__ const recv_length      = A->recv_length     ,
               * __restrict__ const send_length      = A->send_length     ,
               * __restrict__ const elements_to_send = A->elements_to_send;
        floatT * __restrict__ const send_buffer      = A->send_buffer     ;

  #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
  const intT *  __restrict__ const depSBite  = A->depSBite;
  const intT ** __restrict__ const depSBidx  = (const intT **) A->depSBidxPtr ,
             ** __restrict__ const depSBsize = (const intT **) A->depSBsizePtr;
  #else
  MPI_Request * reqR = new MPI_Request[num_send_recv];
  #endif

  intT idxR = nrow;
  for(intT i = iZero; i < num_send_recv; ++i)
  {
    const intT n_recv = recv_length[i], neigh = neighbors[i];

    #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
    #if   defined(USING_OSS_TASK)
    #pragma oss task default(none) label("MPI_Irecv") firstprivate(MPI_FLOATT, idxR, n_recv, neigh, MPItag, x) out(x[idxR;n_recv])
    #elif defined(USING_OMP_TASK)
    #pragma omp task default(none)                    firstprivate(MPI_FLOATT, idxR, n_recv, neigh, MPItag, x) depend(out:x[idxR:n_recv])
    #endif
    {
      MPI_Request reqR;
      MPI_Irecv  (x+idxR, n_recv, MPI_FLOATT, neigh, MPItag, MPI_COMM_WORLD, & reqR);
      TAMPI_Iwait(& reqR, MPI_STATUS_IGNORE);
    }
    #else
    MPI_Irecv(x+idxR, n_recv, MPI_FLOATT, neigh, MPItag, MPI_COMM_WORLD, reqR+i);
    #endif

    idxR += n_recv;
  }

  intT idxS = iZero;
  for(intT i = iZero; i < num_send_recv; ++i)
  {
    const intT n_send = send_length[i], neigh = neighbors[i];

    #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
    #if   defined(USING_OSS_TASK)
    #pragma oss task default(none) label("MPI_Isend") firstprivate(MPI_FLOATT, iZero, idxS, n_send, neigh, MPItag) \
                     firstprivate(elements_to_send, send_buffer, x)                                                \
                     in ({x[depSBidx[i][j];depSBsize[i][j]], j=iZero;depSBite[i]})                                 \
                     out(send_buffer[idxS;n_send])
    #elif defined(USING_OMP_TASK)
    #pragma omp task default(none)                    firstprivate(MPI_FLOATT, iZero, idxS, n_send, neigh, MPItag) \
                     firstprivate(elements_to_send, send_buffer, x)                                                \
                     depend(iterator (j=iZero:depSBite[i]), in:x[depSBidx[i][j]:depSBsize[i][j]])                  \
                     depend(out:send_buffer[idxS:n_send])
    #endif
    {
      for(intT j = iZero; j < n_send; ++j) {send_buffer[idxS+j] = x[elements_to_send[idxS+j]];}

      MPI_Request reqS;
      MPI_Isend  (send_buffer+idxS, n_send, MPI_FLOATT, neigh, MPItag, MPI_COMM_WORLD, & reqS);
      TAMPI_Iwait(& reqS, MPI_STATUS_IGNORE);
    }
    #else
    {
      for(intT j = iZero; j < n_send; ++j) {send_buffer[idxS+j] = x[elements_to_send[idxS+j]];}

      MPI_Request reqS ; int dummy;
      MPI_Isend(send_buffer+idxS, n_send, MPI_FLOATT, neigh, MPItag, MPI_COMM_WORLD, & reqS);
      MPI_Test (& reqS, & dummy, MPI_STATUS_IGNORE);
    }
    #endif

    idxS += n_send;
  }


  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  MPI_Status status;

  for(intT i = iZero; i < num_send_recv; ++i)
  {
    if(MPI_Wait(reqR+i, & status))
    {
      cerr << "MPI_Wait error\n"<<endl;
      exit(-1);
    }
  }

  delete [] reqR;
  #endif
}
#endif // USING_MPI
