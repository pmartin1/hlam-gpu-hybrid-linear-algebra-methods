// BSD 3-Clause License

// HLAM: Hybrid Linear Algebra Methods
// Copyright (c) 2022, Pedro J. Martinez-Ferrer

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:

// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)


#pragma once


#include <sys/mman.h>
#include <assert.h>
#include <algorithm>
#include "mytimer.hpp"

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#if defined(USING_NCCL)
#include <nccl.h>
#endif


// MACRO functions: note that they do only act as string replacements


// Unlikely of a boolean
#define UNLIKELY(B) (__builtin_expect(B, false))


// Minimum of two numbers (boosts performance)
#define MIN(X1, X2) ((X1) < (X2) ? (X1) : (X2))


// Maximum of two numbers (boosts performance)
#define MAX(X1, X2) ((X1) > (X2) ? (X1) : (X2))


// Ceiling integer division
#define CEILDIV(INUM, IDEN) ((INUM + IDEN - iOne)/(IDEN))


// Odd integer
#define ISODD(I) ((I) & iOne)


// Allocate using mmap function
#define MMAPALLOC(SIZE, TYPE, ALIGN) (static_cast<TYPE *>(mmap(NULL, (SIZE)*sizeof(TYPE) + (ALIGN), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0)))


// Align after using mmap function
#define MMAPALIGN(BUFFER, TYPE, ALIGN) ((TYPE *)(((uintptr_t) BUFFER + (ALIGN) - iOne) & ~(uintptr_t)((ALIGN) - iOne)))


// Unallocate using mmap function
#define MMAPUNALLOC(BUFFER, SIZE, TYPE, ALIGN) (munmap(BUFFER, (SIZE)*sizeof(TYPE) + (ALIGN)))


// Functions

// CUDA macros
#define CHECK_CUDA(f...)                                           \
{                                                                 \
  const cudaError_t __e = f;                                      \
                                                                  \
  if(__e != cudaSuccess)                                          \
  {                                                               \
    cerr << "CUDA error in: " << #f << " [" << __FILE__ << ","    \
         << __LINE__ << "], " << cudaGetErrorString(__e) << endl; \
    exit(1);                                                      \
  }                                                               \
}

#define NCCLCHECK(cmd) do {                         \
  ncclResult_t res = cmd;                           \
  if (res != ncclSuccess) {                         \
    printf("Failed, NCCL error %s:%d '%s'\n",       \
        __FILE__,__LINE__,ncclGetErrorString(res)); \
    exit(EXIT_FAILURE);                             \
  }                                                 \
} while(0)

#define CHECK_CUSPARSE(f...)                                         \
{                                                                 \
  const cusparseStatus_t __s = f;                                   \
                                                                  \
  if(__s != CUSPARSE_STATUS_SUCCESS)                                \
  {                                                               \
    cerr << "CUDA status in: " << #f << " [" << __FILE__ << ","   \
         << __LINE__ << "], " << endl;                            \
    exit(1);                                                      \
  }                                                               \
}

#define CHECK_CUBLAS(f...)                                         \
{                                                                 \
  const cublasStatus_t __s = f;                                   \
                                                                  \
  if(__s != CUBLAS_STATUS_SUCCESS)                                \
  {                                                               \
    cerr << "CUDA status in: " << #f << " [" << __FILE__ << ","   \
         << __LINE__ << "], " << endl;                            \
    exit(1);                                                      \
  }                                                               \
}

// Absolute value of a float
static inline floatT oabs(const floatT x)
{
  #ifdef SINGLE_FLOATS
  return fabsf(x);
  #else
  return fabs (x);
  #endif
}


// Split any given interval [start, stopPlusOne) in a family of
// intervals of sizes bs1 & bs2 multiple of number together with
// another family of intervals bsR1 & bsR2 multiple (or not) of number
// Here number can have any value and for this reason all the
// remainder operations '%' are always used
static inline intT splitInterval(const intT interval, const intT nb, const intT number)
{
  #ifdef DEBUG
  assert(interval >= iOne); assert(nb >= iOne); assert(number >= iOne);
  #endif

  if(nb == iOne    ) {return interval;}                            // One single block case

  if(nb >= interval) {return MIN(interval, number);}               // nb blocks of size 1

  const intT bsE = CEILDIV(interval, nb);                          // Estimated block size (bs) based on the estimated number of blocks (nb)

  const intT regInterval = interval - (interval % number);         // Regularise the interval in case it is not multiple of number

  const intT bs1 = bsE > number ? (bsE - (bsE % number)) : number, // number is the minimum block size
             nb1 = CEILDIV(interval, bs1);

  const intT bs2 = bs1 + number          ,                         // bs2 > bs1
             nb2 = CEILDIV(interval, bs2);

  if(nb1 == nb2)                                                   // Same number of blocks
  {
    const intT bsR1 = interval % bs1;
    if(bsR1 == iZero) {return bs1;}                                // 1) Avoid the remainder block (bs1)

    const intT bsR2 = interval % bs2;
    if(bsR1 >  bsR2 ) {return bs1;}                                // 2) Pick the largest remainder block (bs1)

    return bs2;                                                    // 3) Pick the largest main      block (bs2)
  }

  if(nb1 <= nb) {return bs1;}                                      // 4) Do not create more blocks than originally specified (bs1)

  return bs2;                                                      // 5) Do not create more blocks than originally specified (bs2)
}


// HPCCG array sequential initialisation
static inline void HPCCGinit(const intT     nrow, const intT ncol, const intT rank,
                             const intT   * __restrict__ const rowIdx,
                                   floatT * __restrict__ const xexact,
                                   floatT * __restrict__ const x     ,
                                   floatT * __restrict__ const b     )
{
  for(intT i = iZero; i < nrow; ++i)
  {
    xexact[i] = one ;
    x     [i] = zero;
    b     [i] = 27.0 - static_cast<floatT>(rowIdx[i+iOne] - rowIdx[i] - iOne);
  }

  // for(intT i = iZero; i < nrow; ++i) {x[i] = rank;} // Initialise with rank numbers
  // arrayRandom(nrow, 0.0, 10.0, rank, x);            // Initialise with random numbers

  for(intT i = nrow; i < ncol; ++i) {x[i] = zero; b[i] = zero; xexact[i] = one;} // Additional zero fill for Jacobi/Gauss-Seidel (x) and BiCGStab (b) kernels
}


// Array randomisation
static inline void arrayRandom(const intT n, const floatT low, const floatT high, const intT rank, floatT * __restrict__ const x)
{
  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, iOne, rank, n, bs, low, high) firstprivate(x)
  #endif
  for(intT ib = iZero; ib < n; ib += bs)
  {
    unsigned int seed = (time(0) + ib/bs)*(rank + iOne); // srand(time(0));

    const intT bsA = MIN(bs, n - ib);

    #pragma omp simd aligned(x: simdSize) simdlen(simdFloatT)
    for(intT i = ib; i < ib+bsA; ++i) {x[i] = low + static_cast<floatT>(rand_r(& seed))/(static_cast<floatT>(RAND_MAX/(high - low)));}
  }
}


// Copy kernel: y := x
static inline void dcopy(const intT n, const floatT * __restrict__ const x, const intT incx,
                                             floatT * __restrict__ const y, const intT incy)
{
  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, n, bs) firstprivate(x, y)
  #endif
  for(intT ib = iZero; ib < n; ib += bs)
  {
    const intT bsA = MIN(bs, n - ib);

    #pragma omp simd aligned(x, y: simdSize) simdlen(simdFloatT)
    for(intT i = ib; i < ib+bsA; ++i) {y[i] = x[i];}
  }
}


// Scale2 kernel: y := a*x; z := b*x
static inline void dscale2(const intT   n,
                           const floatT a, const floatT * __restrict__ const x, const intT incx,
                           const floatT b,       floatT * __restrict__ const y, const intT incy,
                                                 floatT * __restrict__ const z, const intT incz)
{
  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  if(a == one) // y := x; z := b*x
  {
    #ifdef USING_OMP_FORK
    #pragma omp parallel for default(none) firstprivate(iZero, n, bs, b) firstprivate(x, y, z)
    #endif
    for(intT ib = iZero; ib < n; ib += bs)
    {
      const intT bsA = MIN(bs, n - ib);

      #pragma omp simd aligned(x, y, z: simdSize) simdlen(simdFloatT)
      for(intT i = ib; i < ib+bsA; ++i) {const floatT xVal = x[i]; y[i] = xVal; z[i] = b*xVal;}
    }

    return;
  }

  if(b == one) // y := a*x; z := x
  {
    #ifdef USING_OMP_FORK
    #pragma omp parallel for default(none) firstprivate(iZero, n, bs, a) firstprivate(x, y, z)
    #endif
    for(intT ib = iZero; ib < n; ib += bs)
    {
      const intT bsA = MIN(bs, n - ib);

      #pragma omp simd aligned(x, y, z: simdSize) simdlen(simdFloatT)
      for(intT i = ib; i < ib+bsA; ++i) {const floatT xVal = x[i]; y[i] = a*xVal; z[i] = xVal;}
    }

    return;
  }

  // General case: y := a*x; z := b*x
  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, n, bs, a, b) firstprivate(x, y, z)
  #endif
  for(intT ib = iZero; ib < n; ib += bs)
  {
    const intT bsA = MIN(bs, n - ib);

    #pragma omp simd aligned(x, y, z: simdSize) simdlen(simdFloatT)
    for(intT i = ib; i < ib+bsA; ++i) {const floatT xVal = x[i]; y[i] = a*xVal; z[i] = b*xVal;}
  }
}


// Daxpy kernel: y := a*x + y
static inline void daxpy(const intT   n,
                         const floatT a, const floatT * __restrict__ const x, const intT incx,
                                               floatT * __restrict__ const y, const intT incy)
{
  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, n, bs, a) firstprivate(x, y)
  #endif
  for(intT ib = iZero; ib < n; ib += bs)
  {
    const intT bsA = MIN(bs, n - ib);

    #pragma omp simd aligned(x, y: simdSize) simdlen(simdFloatT)
    for(intT i = ib; i < ib+bsA; ++i) {y[i] += a*x[i];}
  }
}


// Daxpby kernel: y := a*x + b*y
static inline void daxpby(const intT   n,
                          const floatT a, const floatT * __restrict__ const x, const intT incx,
                          const floatT b,       floatT * __restrict__ const y, const intT incy)
{
  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  if(a == one) // y := x + b*y
  {
    #ifdef USING_OMP_FORK
    #pragma omp parallel for default(none) firstprivate(iZero, n, bs, b) firstprivate(x, y)
    #endif
    for(intT ib = iZero; ib < n; ib += bs)
    {
      const intT bsA = MIN(bs, n - ib);

      #pragma omp simd aligned(x, y: simdSize) simdlen(simdFloatT)
      for(intT i = ib; i < ib+bsA; ++i) {y[i] = x[i] + b*y[i];}
    }

    return;
  }

  if(b == one) // y := a*x + y (it is the daxpy kernel indeed)
  {
    #ifdef USING_OMP_FORK
    #pragma omp parallel for default(none) firstprivate(iZero, n, bs, a) firstprivate(x, y)
    #endif
    for(intT ib = iZero; ib < n; ib += bs)
    {
      const intT bsA = MIN(bs, n - ib);

      #pragma omp simd aligned(x, y: simdSize) simdlen(simdFloatT)
      for(intT i = ib; i < ib+bsA; ++i) {y[i] += a*x[i];}
    }

    return;
  }

  if(a == zero) // y := b*y (it is the dscale kernel indeed)
  {
    #ifdef USING_OMP_FORK
    #pragma omp parallel for default(none) firstprivate(iZero, n, bs, b) firstprivate(y)
    #endif
    for(intT ib = iZero; ib < n; ib += bs)
    {
      const intT bsA = MIN(bs, n - ib);

      #pragma omp simd aligned(y: simdSize) simdlen(simdFloatT)
      for(intT i = ib; i < ib+bsA; ++i) {y[i] = b*y[i];}
    }

    return;
  }

  if(b == zero) // y := a*x
  {
    #ifdef USING_OMP_FORK
    #pragma omp parallel for default(none) firstprivate(iZero, n, bs, a) firstprivate(x, y)
    #endif
    for(intT ib = iZero; ib < n; ib += bs)
    {
      const intT bsA = MIN(bs, n - ib);

      #pragma omp simd aligned(x, y: simdSize) simdlen(simdFloatT)
      for(intT i = ib; i < ib+bsA; ++i) {y[i] = a*x[i];}
    }

    return;
  }

  // General case: y := a*x + b*y
  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, n, bs, a, b) firstprivate(x, y)
  #endif
  for(intT ib = iZero; ib < n; ib += bs)
  {
    const intT bsA = MIN(bs, n - ib);

    #pragma omp simd aligned(x, y: simdSize) simdlen(simdFloatT)
    for(intT i = ib; i < ib+bsA; ++i) {y[i] = a*x[i] + b*y[i];}
  }
}


// Daxpbypcz kernel: z := a*x + b*y + c*z
static inline void daxpbypcz(const intT   n,
                             const floatT a, const floatT * __restrict__ const x, const intT incx,
                             const floatT b, const floatT * __restrict__ const y, const intT incy,
                             const floatT c,       floatT * __restrict__ const z, const intT incz)
{
  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  if(a == one && c == zero) // z := x + b*y
  {
    #ifdef USING_OMP_FORK
    #pragma omp parallel for default(none) firstprivate(iZero, n, bs, b) firstprivate(x, y, z)
    #endif
    for(intT ib = iZero; ib < n; ib += bs)
    {
      const intT bsA = MIN(bs, n - ib);

      #pragma omp simd aligned(x, y: simdSize) simdlen(simdFloatT)
      for(intT i = ib; i < ib+bsA; ++i) {z[i] = x[i] + b*y[i];}
    }

    return;
  }

  if(c == one) // z := a*x + b*y + z
  {
    #ifdef USING_OMP_FORK
    #pragma omp parallel for default(none) firstprivate(iZero, n, bs, a, b) firstprivate(x, y, z)
    #endif
    for(intT ib = iZero; ib < n; ib += bs)
    {
      const intT bsA = MIN(bs, n - ib);

      #pragma omp simd aligned(x, y: simdSize) simdlen(simdFloatT)
      for(intT i = ib; i < ib+bsA; ++i) {z[i] += a*x[i] + b*y[i];}
    }

    return;
  }

  // General case: z := a*x + b*y + c*z
  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, n, bs, a, b, c) firstprivate(x, y, z)
  #endif
  for(intT ib = iZero; ib < n; ib += bs)
  {
    const intT bsA = MIN(bs, n - ib);

    #pragma omp simd aligned(x, y, z: simdSize) simdlen(simdFloatT)
    for(intT i = ib; i < ib+bsA; ++i) {z[i] = a*x[i] + b*y[i] + c*z[i];}
  }
}


// Ddot kernel: result = sum(x*y);
static inline floatT ddot(const intT n, const floatT * __restrict__ const x, const intT incx,
                                        const floatT * __restrict__ const y, const intT incy)
{
  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  floatT result = zero;

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, zero, n, bs) firstprivate(x, y) reduction(+:result)
  #endif
  for(intT i0 = iZero; i0 < n; i0 += bs)
  {
    const intT bsA = MIN(bs, n - i0);

    floatT resultV = zero;

    #pragma omp simd aligned(x, y: simdSize) simdlen(simdFloatT) reduction(+:resultV)
    for(intT i = i0; i < i0+bsA; ++i) {resultV += x[i]*y[i];}

    result += resultV;
  }

  return result;
}


// Ddot2 kernel: [res0,res1] = [sum(x*y),sum(x*z)]; // It does two reductions at once (the first array "x" is common to both reductions)
static inline void ddot2(const intT n, const floatT * __restrict__ const x, const intT incx,
                                       const floatT * __restrict__ const y, const intT incy,
                                       const floatT * __restrict__ const z, const intT incz,
                                             floatT & result0, floatT & result1)
{
  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  if(x == y) // [res0,res1] = [sum(x*x),sum(x*z)]
  {
    #ifdef USING_OMP_FORK
    #pragma omp parallel for default(none) firstprivate(iZero, zero, n, bs) firstprivate(x, z) reduction(+:result0, result1)
    #endif
    for(intT i0 = iZero; i0 < n; i0 += bs)
    {
      const intT bsA = MIN(bs, n - i0);

      floatT resultV0 = zero,
             resultV1 = zero;

      #pragma omp simd aligned(x, z: simdSize) simdlen(simdFloatT) reduction(+:resultV0, resultV1)
      for(intT i = i0; i < i0+bsA; ++i)
      {
        const floatT xVal = x[i];

        resultV0 += xVal*xVal;
        resultV1 += xVal*z[i];
      }

      result0 += resultV0;
      result1 += resultV1;
    }

    return;
  }

  // General case: [res0,res1] = [sum(x*y),sum(x*z)]
  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, zero, n, bs) firstprivate(x, y, z) reduction(+:result0, result1)
  #endif
  for(intT i0 = iZero; i0 < n; i0 += bs)
  {
    const intT bsA = MIN(bs, n - i0);

    floatT resultV0 = zero,
           resultV1 = zero;

    #pragma omp simd aligned(x, y, z: simdSize) simdlen(simdFloatT) reduction(+:resultV0, resultV1)
    for(intT i = i0; i < i0+bsA; ++i)
    {
      const floatT xVal = x[i];

      resultV0 += xVal*y[i];
      resultV1 += xVal*z[i];
    }

    result0 += resultV0;
    result1 += resultV1;
  }
}


// Sparse matrix vector kernel: y = A*x (CSR)
static inline void sparse_d_mv(const intT start, const intT size,
                                   const HPC_Sparse_Matrix * __restrict__ const A,
                                   const floatT *            __restrict__ const x,
                                         floatT *            __restrict__ const y)
{
  const intT   * __restrict__ const rIdx = A->rowIndex + start; // Indexing at start
  const intT   * __restrict__ const pCol = A->list_of_cols    ;
  const floatT * __restrict__ const pVal = A->list_of_vals    ;

  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(size, omp_get_max_threads(), simdSize);
  #else
  const intT bs = size;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, zero, size, bs) firstprivate(rIdx, x, y, pCol, pVal)
  #endif
  for(intT ib = iZero; ib < size; ib += bs)
  {
    const intT bsA   = MIN(bs, size - ib);
          intT rIdx0 = rIdx[ib];

    for(intT i = ib; i < ib+bsA; ++i)
    {
      const intT   nnz = rIdx[i + iOne] - rIdx0;
            floatT wrk = zero;

      #pragma omp simd aligned(x, pVal, pCol: simdSize) simdlen(simdFloatT) reduction(+: wrk)
      for(intT j = iZero; j < nnz; ++j) {wrk += pVal[rIdx0 + j]*x[pCol[rIdx0 + j]];}

      y[i]   = wrk;
      rIdx0 += nnz;
    }
  }
}


// Sparse matrix vector kernel: y = A*x (COO)
static inline void sparse_d_mv_coo(const intT start, const intT size,
                                   const HPC_Sparse_Matrix * __restrict__ const A,
                                   const floatT *            __restrict__ const x,
                                         floatT *            __restrict__ const y)
{
  assert(start == iZero); assert(size == A->local_nrow);

  const intT   * __restrict__ const pCol = A->list_of_cols;
  const intT   * __restrict__ const pRow = A->list_of_rows;
  const floatT * __restrict__ const pVal = A->list_of_vals;

  const intT nrow = A->local_nrow, true_nnz = A->true_local_nnz;

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, nrow, zero) firstprivate(y)
  #endif
  for(intT i = iZero; i < nrow; ++i) {y[i] = zero;}

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, true_nnz) firstprivate(x, y, pRow, pVal, pCol)
  #endif
  for(intT i = iZero; i < true_nnz; ++i) {y[pRow[i]] += pVal[i]*x[pCol[i]];}
}


// Sparse matrix vector kernel: y = A*x (LDU)
static inline void sparse_d_mv_ldu(const intT start, const intT size,
                                   const HPC_Sparse_Matrix * __restrict__ const A,
                                   const floatT *            __restrict__ const x,
                                         floatT *            __restrict__ const y)
{
  assert(start == iZero); assert(size == A->local_nrow);

  const intT   * __restrict__ const pCol_l = A->list_of_cols_l;
  const floatT * __restrict__ const pVal_l = A->list_of_vals_l;

  const floatT * __restrict__ const pVal_d = A->list_of_diags ;

  const intT   * __restrict__ const pCol_u = A->list_of_cols_u;
  const floatT * __restrict__ const pVal_u = A->list_of_vals_u;

  const intT nrow = A->local_nrow, nnzTri = (A->true_local_nnz - nrow)/iTwo;

  for(intT i = iZero; i < nrow; ++i) {y[i] = pVal_d[i]*x[i];}

  for(intT i = iZero; i < nnzTri; ++i)
  {
    const intT col_l = pCol_l[i], col_u = pCol_u[i];

    y[col_l] += pVal_u[i]*x[col_u];
    y[col_u] += pVal_l[i]*x[col_l];
  }
}


// Sparse matrix vector kernel: y = A*x (MSR)
static inline void sparse_d_mv_msr(const intT start, const intT size,
                                   const HPC_Sparse_Matrix * __restrict__ const A,
                                   const floatT *            __restrict__ const x,
                                         floatT *            __restrict__ const y)
{
  const intT   * __restrict__ const rIdx  = A->rowIndexM + start; // Indexing at start
  const intT   * __restrict__ const pCol  = A->list_of_colsM    ;
  const floatT * __restrict__ const pVal  = A->list_of_valsM    ;
  const floatT * __restrict__ const pDiag = A->list_of_diags    ;

  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(size, omp_get_max_threads(), simdSize);
  #else
  const intT bs = size;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, zero, size, bs, start)  \
                                         firstprivate(rIdx, x, y, pDiag, pCol, pVal)
  #endif
  for(intT ib = iZero; ib < size; ib += bs)
  {
    const intT bsA   = MIN(bs, size - ib);
          intT rIdx0 = rIdx[ib];

    for(intT i = ib; i < ib+bsA; ++i)
    {
      const intT   nnz = rIdx[i + iOne] - rIdx0;
            floatT wrk = pDiag[i+start]*x[i+start];

      #pragma omp simd aligned(x, pVal, pCol: simdSize) simdlen(simdFloatT) reduction(+: wrk)
      for(intT j = iZero; j < nnz; ++j) {wrk += pVal[rIdx0 + j]*x[pCol[rIdx0 + j]];}

      y[i]   = wrk;
      rIdx0 += nnz;
    }
  }
}


// Sparse matrix vector kernel: y = A*x (LDUCSR)
static inline void sparse_d_mv_lducsr(const intT start, const intT size,
                                      const HPC_Sparse_Matrix * __restrict__ const A,
                                      const floatT *            __restrict__ const x,
                                            floatT *            __restrict__ const y)
{
  const intT   * __restrict__ const rIdx_l = A->rowIndex_l + start;
  const intT   * __restrict__ const pCol_l = A->list_of_cols_l    ;
  const floatT * __restrict__ const pVal_l = A->list_of_vals_l    ;

  const floatT * __restrict__ const pDiag  = A->list_of_diags     ;

  const intT   * __restrict__ const rIdx_u = A->rowIndex_u + start;
  const intT   * __restrict__ const pCol_u = A->list_of_cols_u    ;
  const floatT * __restrict__ const pVal_u = A->list_of_vals_u    ;

  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(size, omp_get_max_threads(), simdSize);
  #else
  const intT bs = size;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, zero, size, bs, start)                                \
                                         firstprivate(rIdx_l, rIdx_u, x, y, pDiag, pCol_l, pVal_l, pCol_u, pVal_u)
  #endif
  for(intT ib = iZero; ib < size; ib += bs)
  {
    const intT bsA     = MIN(bs, size - ib);
          intT rIdx0_l = rIdx_l[ib],
               rIdx0_u = rIdx_u[ib];

    for(intT i = ib; i < ib+bsA; ++i)
    {
      const intT   nnz_l = rIdx_l[i + iOne] - rIdx0_l,
                   nnz_u = rIdx_u[i + iOne] - rIdx0_u;
            floatT wrk_l = pDiag[i+start]*x[i+start] ,
                   wrk_u = zero                      ;

      #pragma omp simd aligned(x, pVal_l, pCol_l: simdSize) simdlen(simdFloatT) reduction(+: wrk_l)
      for(intT j = iZero; j < nnz_l; ++j) {wrk_l += pVal_l[rIdx0_l + j]*x[pCol_l[rIdx0_l + j]];}

      #pragma omp simd aligned(x, pVal_u, pCol_u: simdSize) simdlen(simdFloatT) reduction(+: wrk_u)
      for(intT j = iZero; j < nnz_u; ++j) {wrk_u += pVal_u[rIdx0_u + j]*x[pCol_u[rIdx0_u + j]];}

      y[i]     = wrk_l + wrk_u;
      rIdx0_l += nnz_l;
      rIdx0_u += nnz_u;
    }
  }
}


// Sparse Jacobi/Gauss-Seidel kernel: y ~ b - L*x + U*x (CSR)
static inline floatT sparse_d_jgs(const intT start, const intT size,
                                  const HPC_Sparse_Matrix * __restrict__ const A,
                                  const floatT *            __restrict__ const b,
                                  const floatT *            __restrict__ const x,
                                        floatT *            __restrict__ const y)
{
  const floatT *  __restrict__ const xStart = x           + start; // Indexing at start
  const intT   *  __restrict__ const rIdx   = A->rowIndex + start; // Indexing at start
  const intT   *  __restrict__ const pCol   = A->list_of_cols    ;
  const floatT *  __restrict__ const pVal   = A->list_of_vals    ;
  const int8_t *  __restrict__ const pDof   = A->list_of_dOff    ;
  // const floatT ** __restrict__ const pDia = (const floatT **) A->ptr_to_diags + start; // Indexing at start (diagonal adresses)

  floatT residual = zero;

  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(size, omp_get_max_threads(), simdSize);
  #else
  const intT bs = size;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, zero, start, size, bs) \
                                         firstprivate(rIdx, x, xStart, y, b, pCol, pVal, pDof) reduction(+: residual)
  #endif
  for(intT ib = iZero; ib < size; ib += bs)
  {
    const intT bsA   = MIN(bs, size - ib);
          intT rIdx0 = rIdx[ib];

    const int8_t * __restrict__ offset = pDof + start + ib; // Offsets to diagonal elements

    for(intT i = ib; i < ib+bsA; ++i)
    {
      const intT   nnz = rIdx[i + iOne] - rIdx0;
            floatT wrk = zero;

      #pragma omp simd aligned(x, pVal, pCol: simdSize) simdlen(simdFloatT) reduction(+: wrk)
      for(intT j = iZero; j < nnz; ++j) {wrk += pVal[rIdx0 + j]*x[pCol[rIdx0 + j]];}

      const floatT diff = (b[i] - wrk)/pVal[rIdx0 + *offset++]; // Offsets to diagonal elements
      // const floatT diff = (b[i] - wrk)/(*pDia[i]);              // Diagonal adresses

      y[i]      = diff + xStart[i];
      residual += diff*diff       ;
      rIdx0    += nnz             ;
    }
  }

  return residual;
}

// Sparse Jacobi/Gauss-Seidel kernel: y ~ b - L*x + U*x (MSR)
static inline floatT sparse_d_jgs_msr(const intT start, const intT size,
                                  const HPC_Sparse_Matrix * __restrict__ const A,
                                  const floatT *            __restrict__ const b,
                                  const floatT *            __restrict__ const x,
                                        floatT *            __restrict__ const y)
{
  const floatT *  __restrict__ const xStart = x           + start; // Indexing at start
  const intT   *  __restrict__ const rIdx   = A->rowIndexM + start; // Indexing at start
  const intT   *  __restrict__ const pCol   = A->list_of_colsM    ;
  const floatT *  __restrict__ const pVal   = A->list_of_valsM    ;
  const floatT *  __restrict__ const pDiag  = A->list_of_diags    ; // Indexing at start (diagonal adresses)

  floatT residual = zero;

  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(size, omp_get_max_threads(), simdSize);
  #else
  const intT bs = size;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, zero, start, size, bs) \
                                         firstprivate(rIdx, x, xStart, y, b, pCol, pVal, pDiag) reduction(+: residual)
  #endif
  for(intT ib = iZero; ib < size; ib += bs)
  {
    const intT bsA   = MIN(bs, size - ib);
          intT rIdx0 = rIdx[ib];

    for(intT i = ib; i < ib+bsA; ++i)
    {
      const intT   nnz = rIdx[i + iOne] - rIdx0;
            floatT wrk = pDiag[i+start]*x[i+start];

      #pragma omp simd aligned(x, pVal, pCol: simdSize) simdlen(simdFloatT) reduction(+: wrk)
      for(intT j = iZero; j < nnz; ++j) {wrk += pVal[rIdx0 + j]*x[pCol[rIdx0 + j]];}

      const floatT diff = (b[i] - wrk)/(pDiag[i]);              // Diagonal adresses

      y[i]      = diff + xStart[i];
      residual += diff*diff       ;
      rIdx0    += nnz             ;
    }
  }

  return residual;
}

// Sparse Jacobi/Gauss-Seidel kernel: y ~ b - L*x + U*x (LDUCSR)
static inline floatT sparse_d_jgs_lducsr(const intT start, const intT size,
                                  const HPC_Sparse_Matrix * __restrict__ const A,
                                  const floatT *            __restrict__ const b,
                                  const floatT *            __restrict__ const x,
                                        floatT *            __restrict__ const y)
{
  const floatT *  __restrict__ const xStart = x           + start; // Indexing at start

  const intT   * __restrict__ const rIdx_l = A->rowIndex_l + start;
  const intT   * __restrict__ const pCol_l = A->list_of_cols_l    ;
  const floatT * __restrict__ const pVal_l = A->list_of_vals_l    ;

  const floatT * __restrict__ const pDiag  = A->list_of_diags     ;

  const intT   * __restrict__ const rIdx_u = A->rowIndex_u + start;
  const intT   * __restrict__ const pCol_u = A->list_of_cols_u    ;
  const floatT * __restrict__ const pVal_u = A->list_of_vals_u    ;
  // const floatT ** __restrict__ const pDia = (const floatT **) A->ptr_to_diags + start; // Indexing at start (diagonal adresses)

  floatT residual = zero;

  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(size, omp_get_max_threads(), simdSize);
  #else
  const intT bs = size;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, zero, start, size, bs) \
                                         firstprivate(rIdx_u, rIdx_l, x, xStart, y, b, pCol_u, pVal_u, pCol_l, pVal_l, pDiag) reduction(+: residual)
  #endif
  for(intT ib = iZero; ib < size; ib += bs)
  {
    const intT bsA   = MIN(bs, size - ib);
          intT rIdx0_l = rIdx_l[ib],
               rIdx0_u = rIdx_u[ib];

    for(intT i = ib; i < ib+bsA; ++i)
    {
      const intT   nnz_l = rIdx_l[i + iOne] - rIdx0_l,
                   nnz_u = rIdx_u[i + iOne] - rIdx0_u;
            floatT wrk_l = pDiag[i+start]*x[i+start] ,
                   wrk_u = zero                      ;

      #pragma omp simd aligned(x, pVal_l, pCol_l: simdSize) simdlen(simdFloatT) reduction(+: wrk_l)
      for(intT j = iZero; j < nnz_l; ++j) {wrk_l += pVal_l[rIdx0_l + j]*x[pCol_l[rIdx0_l + j]];}

      #pragma omp simd aligned(x, pVal_u, pCol_u: simdSize) simdlen(simdFloatT) reduction(+: wrk_u)
      for(intT j = iZero; j < nnz_u; ++j) {wrk_u += pVal_u[rIdx0_u + j]*x[pCol_u[rIdx0_u + j]];}

       floatT wrk = wrk_l + wrk_u;

//      const floatT diff = (b[i] - wrk)/pVal[rIdx0 + *offset++]; // Offsets to diagonal elements
      const floatT diff = (b[i] - wrk)/(pDiag[i]);              // Diagonal adresses

      y[i]      = diff + xStart[i];
      residual += diff*diff       ;
      rIdx0_l += nnz_l;
      rIdx0_u += nnz_u;
    }
  }

  return residual;
}

// Sparse Jacobi/Gauss-Seidel kernel: y ~ b - L*x + U*x (COO)
static inline floatT sparse_d_jgs_coo(const intT start, const intT size,
                                  const HPC_Sparse_Matrix * __restrict__ const A,
                                  const floatT *            __restrict__ const b,
                                  const floatT *            __restrict__ const x,
                                        floatT *            __restrict__ const y)
{
  const floatT *  __restrict__ const xStart = x           + start; // Indexing at start
  const intT   *  __restrict__ const pRow   = A->list_of_rows    ;
  const intT   *  __restrict__ const pCol   = A->list_of_cols    ;
  const floatT *  __restrict__ const pVal   = A->list_of_vals    ;
  const floatT *  __restrict__ const pDiag  = A->list_of_diags   ; // Diagonal values are calculated in copy_buffer kernel

  const intT nrow = A->local_nrow, true_nnz = A->true_local_nnz;

  floatT residual  = iZero ;
  floatT diff      = iZero ;
  floatT wrk[nrow]         ;


    for(intT i = iZero; i < nrow; ++i) { wrk[i]  =  iZero;  }

    for(intT i = iZero; i < true_nnz; ++i)
       {
           wrk[pRow[i]] += pVal[i]*x[pCol[i]];
       }

    for(intT i = iZero; i < nrow; ++i)
       {
          diff    = (b[i] - wrk[i])/(pDiag[i]) ;          // Diagonal values

          y[i]    = diff  + xStart[i] ;

          residual += diff*diff       ;
       }

     return residual;
}

// Sparse Jacobi/Gauss-Seidel kernel: y ~ b - L*x + U*x (LDU)
static inline floatT sparse_d_jgs_ldu(const intT start, const intT size,
                                  const HPC_Sparse_Matrix * __restrict__ const A,
                                  const floatT *            __restrict__ const b,
                                  const floatT *            __restrict__ const x,
                                        floatT *            __restrict__ const y)
{
//  assert(start == iZero); assert(size == A->local_nrow);

  const floatT * __restrict__ const xStart = x         + start; // Indexing at start
  const intT   * __restrict__ const pCol_l = A->list_of_cols_l;
  const floatT * __restrict__ const pVal_l = A->list_of_vals_l;
  const floatT * __restrict__ const pVal_d = A->list_of_diags ;
  const intT   * __restrict__ const pCol_u = A->list_of_cols_u;
  const floatT * __restrict__ const pVal_u = A->list_of_vals_u;

  const intT nrow = A->local_nrow, nnzTri = (A->true_local_nnz - nrow)/iTwo;
  floatT residual  = iZero ;
  floatT diff      = iZero ;
  floatT  wrk[nrow] ;


  for(intT i = iZero; i < nrow; ++i) {wrk[i] = pVal_d[i]*x[i];}


  for(intT i = iZero; i < nnzTri; ++i)
  {
    const intT col_l = pCol_l[i], col_u = pCol_u[i];

    wrk[col_l] += pVal_u[i]*x[col_u];
    wrk[col_u] += pVal_l[i]*x[col_l];
  }

  for(intT i = iZero; i < nrow; ++i)
  {
    diff    = (b[i] - wrk[i])/(pVal_d[i]) ;          // Diagonal values

    y[i]    = diff + xStart[i]   ;

  residual += diff*diff          ;
  }
  return residual;
}

// MPI global sum reduction
#ifdef USING_MPI
static inline void MPI_Sum(const MPI_Comm & MPIcomm, const floatT & local, floatT & global, double & time)
{
  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  const double t0 = mytimer();
  #endif

  #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
  #if   defined(USING_OSS_TASK)
  #pragma oss task default(none) label("MPI_Sum") firstprivate(MPI_FLOATT, MPIcomm, iOne) in(local) out(global) priority(100)
  #elif defined(USING_OMP_TASK)
  #pragma omp task default(none)                  firstprivate(MPI_FLOATT, MPIcomm, iOne) shared(local, global) depend(in: local) depend(out: global)
  #endif
  {
    MPI_Request request;

    MPI_Iallreduce(& local, & global, iOne, MPI_FLOATT, MPI_SUM, MPIcomm, & request);
    TAMPI_Iwait   (& request, MPI_STATUS_IGNORE);
  }
  #else
  MPI_Allreduce(& local, & global, iOne, MPI_FLOATT, MPI_SUM, MPIcomm);
  #endif

  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  time += mytimer() - t0;
  #endif
}
#endif


#ifdef USING_MPI
 #ifdef USING_CUDA
 static inline void MPI_Sum_GPU(const MPI_Comm & MPIcomm, const floatT * local, floatT * global, double & time)
 {
  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  const double t0 = mytimer();
  #endif
  MPI_Allreduce(local, global, iOne, MPI_FLOATT, MPI_SUM, MPIcomm);
  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  time += mytimer() - t0;
  #endif
 }
 #endif
#endif

#ifdef USING_MPI
 #ifdef USING_NCCL
 static inline void NCCL_Sum_GPU(const ncclComm_t & NCCLcomm,  floatT * local, floatT * global, double & time, cudaStream_t * __restrict__ const streamArr)
 {
  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  const double t0 = mytimer();
  #endif
  cudaStream_t & stream0 = streamArr[0];
  NCCLCHECK (ncclAllReduce(local, global, iOne, ncclDouble, ncclSum,  NCCLcomm, stream0) );
  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  time += mytimer() - t0;
  #endif
 }
 #endif
#endif

/*
// MPI global sum reduction
#ifdef USING_MPI
static inline void MPI_Sum2(const MPI_Comm & MPIcomm, const floatT & local0, const floatT & local1, floatT * __restrict__ const global, double & time)
{
  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  const double t0 = mytimer();
  #endif

  #if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
  #if   defined(USING_OSS_TASK)
  #pragma oss task default(none) label("MPI_Sum2") firstprivate(MPI_FLOATT, MPIcomm, iTwo, global) \
                   in (local0, local1) out({global[i;iOne], i=iZero;iTwo}) priority(100)
  #elif defined(USING_OMP_TASK)
  #pragma omp task default(none)                   firstprivate(MPI_FLOATT, MPIcomm, iTwo, global) shared(local0, local1) \
                   depend(in: local0, local1) depend(iterator(i=iZero:iTwo), out: global[i:iOne])
  #endif
  {
    MPI_Request request   ;
    floatT      local[iTwo] = {local0, local1};

    MPI_Iallreduce(local, global, iTwo, MPI_FLOATT, MPI_SUM, MPIcomm, & request);
    TAMPI_Iwait   (& request, MPI_STATUS_IGNORE);
  }
  #else
  floatT local[iTwo] = {local0, local1};

  MPI_Allreduce(local, global, iTwo, MPI_FLOATT, MPI_SUM, MPIcomm);
  #endif

  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  time += mytimer() - t0;
  #endif
}
#endif

*/
#ifdef USING_MPI
static inline void MPI_Sum2_GPU(const MPI_Comm & MPIcomm, floatT * local0, floatT * local1, floatT * global0, floatT * global1, double & time)
{
  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  const double t0 = mytimer();
  #endif

//  floatT local [iTwo] = {local0 , local1 };
//  floatT global[iTwo] = {global0, global1};

  MPI_Allreduce(local0, global0, iOne, MPI_FLOATT, MPI_SUM, MPIcomm);
  MPI_Allreduce(local1, global1, iOne, MPI_FLOATT, MPI_SUM, MPIcomm);

  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  time += mytimer() - t0;
  #endif
}
#endif
/*
#ifdef USING_MPI
 #ifdef USING_NCCL
 static inline void NCCL_Sum2_GPU(const ncclComm_t & NCCLcomm, const floatT * local0, const floatT * local1, floatT * __restrict__ const global, double & time, cudaStream_t * __restrict__ const streamArr)
 {
  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  const double t0 = mytimer();
  #endif
  cudaStream_t & stream0 = streamArr[0];
  floatT local[iTwo] = {local0, local1};

//  MPI_Allreduce(local, global, iTwo, MPI_FLOATT, MPI_SUM, MPIcomm);
  
  NCCLCHECK (ncclAllReduce(local, global, iOne, ncclDouble, ncclSum,  NCCLcomm, stream0) );  

  #if not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
  time += mytimer() - t0;
  #endif
 }
 #endif
#endif
*/
// Copy buffers (COO)
static inline void copy_buffers_coo(const intT size, HPC_Sparse_Matrix * __restrict__ const A)
{
  const intT ratio = A->ratio;

  A->list_of_rows0   = MMAPALLOC(size*ratio, intT  , thp); A->list_of_rows   = MMAPALIGN(A->list_of_rows0,  intT,   thp);
  A->list_of_diags0  = MMAPALLOC(size      , floatT, thp); A->list_of_diags  = MMAPALIGN(A->list_of_diags0, floatT, thp);
  A->rowIndex_u0     = MMAPALLOC(size +iOne, intT  , thp); A->rowIndex_u     = MMAPALIGN(A->rowIndex_u0   , intT  , thp);

  const intT   * __restrict__ const rIdx_CSR = A->rowIndex    ;
  const intT   * __restrict__ const pCol_CSR = A->list_of_cols;
  const floatT * __restrict__ const pVal_CSR = A->list_of_vals;
  const int8_t * __restrict__ const pDof_CSR = A->list_of_dOff;

        intT   * __restrict__ const pRow_COO = A->list_of_rows; // only rows need to be calculated
        floatT * __restrict__ const pDia_COO = A->list_of_diags;// Diagonal elements are needed to be calculated for GS/Jacobi

  intT   * nnz_per_row0      = MMAPALLOC(A->local_nrow   , intT  , thp); intT   * nnz_per_row      = MMAPALIGN(nnz_per_row0     , intT  , thp);

  intT count     = iZero;
  intT rIdx0_CSR = rIdx_CSR[iZero] ;

  for(intT i = iZero; i < size; ++i)
  {
    const intT nnz_CSR    = rIdx_CSR[i + iOne] - rIdx0_CSR,
               offset_CSR = pDof_CSR[i]                   ;

    pDia_COO[i] = pVal_CSR[rIdx0_CSR + offset_CSR]; // Diagonal element

    rIdx0_CSR  += nnz_CSR;
  }

  for(intT i = 0; i < A->local_nrow; ++i) {nnz_per_row[i] = rIdx_CSR[i+iOne] - rIdx_CSR[i];}

  for(intT i = 0; i < A->local_nrow; ++i)
  {
     for(intT j = 0; j < nnz_per_row[i]; ++j)
      {
       pRow_COO[count] = i ;
       count++ ;
      }
  }
}

// Copy buffers (MSR)
static inline void copy_buffers_msr(const intT size, HPC_Sparse_Matrix * __restrict__ const A)
{
  const intT ratio = A->ratio;

  A->rowIndexM0     = MMAPALLOC(size + iOne, intT  , thp); A->rowIndexM     = MMAPALIGN(A->rowIndexM0    , intT  , thp);
  A->list_of_colsM0 = MMAPALLOC(size*ratio , intT  , thp); A->list_of_colsM = MMAPALIGN(A->list_of_colsM0, intT  , thp);
  A->list_of_valsM0 = MMAPALLOC(size*ratio , floatT, thp); A->list_of_valsM = MMAPALIGN(A->list_of_valsM0, floatT, thp);
  A->list_of_diags0 = MMAPALLOC(size       , floatT, thp); A->list_of_diags = MMAPALIGN(A->list_of_diags0, floatT, thp);

  const intT   * __restrict__ const rIdx_CSR = A->rowIndex     ;
  const intT   * __restrict__ const pCol_CSR = A->list_of_cols ;
  const floatT * __restrict__ const pVal_CSR = A->list_of_vals ;
  const int8_t * __restrict__ const pDof_CSR = A->list_of_dOff ;

        intT   * __restrict__ const rIdx_MSR = A->rowIndexM    ;
        intT   * __restrict__ const pCol_MSR = A->list_of_colsM;
        floatT * __restrict__ const pVal_MSR = A->list_of_valsM;
        floatT * __restrict__ const pDia_MSR = A->list_of_diags;

  intT rIdx0_CSR = rIdx_CSR[iZero]        ; // rIdx_CSR[iZero] = 0 // The first value of the row-index array is always zero
                   rIdx_MSR[iZero] = iZero; // rIdx_MSR[iZero] = 0 // The first value of the row-index array is always zero

  intT nnz_MSR_acumm = iZero;

  for(intT i = iZero; i < size; ++i)
  {
    const intT nnz_CSR    = rIdx_CSR[i + iOne] - rIdx0_CSR,
               offset_CSR = pDof_CSR[i]                   ,
               rIdx0_MSR  = nnz_MSR_acumm                 ;

          intT nnz_MSR    = iZero;

    for(intT j = iZero; j < offset_CSR; ++j) // Lower triangle
    {
      pVal_MSR[rIdx0_MSR + nnz_MSR] = pVal_CSR[rIdx0_CSR + j];
      pCol_MSR[rIdx0_MSR + nnz_MSR] = pCol_CSR[rIdx0_CSR + j];

      ++nnz_MSR;
    }

    for(intT j = offset_CSR + iOne; j < nnz_CSR; ++j) // Upper triangle
    {
      pVal_MSR[rIdx0_MSR + nnz_MSR] = pVal_CSR[rIdx0_CSR + j];
      pCol_MSR[rIdx0_MSR + nnz_MSR] = pCol_CSR[rIdx0_CSR + j];

      ++nnz_MSR;
    }

    pDia_MSR[i] = pVal_CSR[rIdx0_CSR + offset_CSR]; // Diagonal element

    nnz_MSR_acumm += nnz_MSR;
    rIdx0_CSR     += nnz_CSR;

    rIdx_MSR[i + iOne] = nnz_MSR_acumm;
  }
}


// Copy buffers (LDUCSR)
static inline void copy_buffers_lducsr(const intT size, HPC_Sparse_Matrix * __restrict__ const A)
{
  const intT ratio = A->ratio;

  A->rowIndex_l0     = MMAPALLOC( size             + iOne, intT  , thp); A->rowIndex_l     = MMAPALIGN(A->rowIndex_l0    , intT  , thp);
  A->list_of_cols_l0 = MMAPALLOC((size*ratio)/iTwo + iOne, intT  , thp); A->list_of_cols_l = MMAPALIGN(A->list_of_cols_l0, intT  , thp);
  A->list_of_vals_l0 = MMAPALLOC((size*ratio)/iTwo + iOne, floatT, thp); A->list_of_vals_l = MMAPALIGN(A->list_of_vals_l0, floatT, thp);

  A->rowIndex_u0     = MMAPALLOC( size             + iOne, intT  , thp); A->rowIndex_u     = MMAPALIGN(A->rowIndex_u0    , intT  , thp);
  A->list_of_cols_u0 = MMAPALLOC((size*ratio)/iTwo + iOne, intT  , thp); A->list_of_cols_u = MMAPALIGN(A->list_of_cols_u0, intT  , thp);
  A->list_of_vals_u0 = MMAPALLOC((size*ratio)/iTwo + iOne, floatT, thp); A->list_of_vals_u = MMAPALIGN(A->list_of_vals_u0, floatT, thp);

  A->list_of_diags0  = MMAPALLOC(size                    , floatT, thp); A->list_of_diags  = MMAPALIGN(A->list_of_diags0 , floatT, thp);

  const intT   * __restrict__ const rIdx_CSR  = A->rowIndex      ;
  const intT   * __restrict__ const pCol_CSR  = A->list_of_cols  ;
  const floatT * __restrict__ const pVal_CSR  = A->list_of_vals  ;
  const int8_t * __restrict__ const pDof_CSR  = A->list_of_dOff  ;

        intT   * __restrict__ const rIdx_LDUl = A->rowIndex_l    ;
        intT   * __restrict__ const pCol_LDUl = A->list_of_cols_l;
        floatT * __restrict__ const pVal_LDUl = A->list_of_vals_l;

        intT   * __restrict__ const rIdx_LDUu = A->rowIndex_u    ;
        intT   * __restrict__ const pCol_LDUu = A->list_of_cols_u;
        floatT * __restrict__ const pVal_LDUu = A->list_of_vals_u;

        floatT * __restrict__ const pDia_LDU  = A->list_of_diags ;

  intT rIdx0_CSR = rIdx_CSR [iZero]        ; // rIdx_CSR [iZero] = 0 // The first value of the row-index array is always zero
                   rIdx_LDUl[iZero] = iZero; // rIdx_LDUl[iZero] = 0 // The first value of the row-index array is always zero
                   rIdx_LDUl[iZero] = iZero; // rIdx_LDUu[iZero] = 0 // The first value of the row-index array is always zero

  intT nnz_LDUl_acumm = iZero,
       nnz_LDUu_acumm = iZero;

  for(intT i = iZero; i < size; ++i)
  {
    const intT nnz_CSR    = rIdx_CSR[i + iOne] - rIdx0_CSR,
               offset_CSR = pDof_CSR[i]                   ,
               rIdx0_LDUl = nnz_LDUl_acumm                ,
               rIdx0_LDUu = nnz_LDUu_acumm                ;

          intT nnz_LDUl   = iZero,
               nnz_LDUu   = iZero;

    for(intT j = iZero; j < offset_CSR; ++j) // Lower triangle
    {
      pVal_LDUl[rIdx0_LDUl + nnz_LDUl] = pVal_CSR[rIdx0_CSR + j];
      pCol_LDUl[rIdx0_LDUl + nnz_LDUl] = pCol_CSR[rIdx0_CSR + j];

      ++nnz_LDUl;
    }

    for(intT j = offset_CSR + iOne; j < nnz_CSR; ++j) // Upper triangle
    {
      pVal_LDUu[rIdx0_LDUu + nnz_LDUu] = pVal_CSR[rIdx0_CSR + j];
      pCol_LDUu[rIdx0_LDUu + nnz_LDUu] = pCol_CSR[rIdx0_CSR + j];

      ++nnz_LDUu;
    }

    pDia_LDU[i] = pVal_CSR[rIdx0_CSR + offset_CSR];

    nnz_LDUl_acumm += nnz_LDUl;
    nnz_LDUu_acumm += nnz_LDUu;
    rIdx0_CSR      += nnz_CSR ;

    rIdx_LDUl[i + iOne] = nnz_LDUl_acumm;
    rIdx_LDUu[i + iOne] = nnz_LDUu_acumm;
  }
}

// Copy buffers (LDU)
static inline void copy_buffers_ldu(const intT size, HPC_Sparse_Matrix * __restrict__ const A)
{
  const intT ratio = A->ratio;
  A->rowIndex_l0     = MMAPALLOC(size           + iOne, intT  , thp); A->rowIndex_l     = MMAPALIGN(A->rowIndex_l0    , intT  , thp);
  A->list_of_cols_l0 = MMAPALLOC(size*(ratio/2) + iOne, intT  , thp); A->list_of_cols_l = MMAPALIGN(A->list_of_cols_l0, intT  , thp);
  A->list_of_vals_l0 = MMAPALLOC(size*(ratio/2) + iOne, floatT, thp); A->list_of_vals_l = MMAPALIGN(A->list_of_vals_l0, floatT, thp);

  A->rowIndex_u0     = MMAPALLOC(size           + iOne, intT  , thp); A->rowIndex_u     = MMAPALIGN(A->rowIndex_u0    , intT  , thp);
  A->list_of_cols_u0 = MMAPALLOC(size*(ratio/2) + iOne, intT  , thp); A->list_of_cols_u = MMAPALIGN(A->list_of_cols_u0, intT  , thp);
  A->list_of_vals_u0 = MMAPALLOC(size*(ratio/2) + iOne, floatT, thp); A->list_of_vals_u = MMAPALIGN(A->list_of_vals_u0, floatT, thp);

  A->list_of_diags0  = MMAPALLOC(size                 , floatT, thp); A->list_of_diags  = MMAPALIGN(A->list_of_diags0 , floatT, thp);

  A->list_of_rows0   = MMAPALLOC(size*ratio           , intT  , thp); A->list_of_rows   = MMAPALIGN(A->list_of_rows0  , intT  , thp);

  const intT   * __restrict__ const rIdx_CSR = A->rowIndex       ;
  const intT   * __restrict__ const pCol_CSR = A->list_of_cols   ;
  const floatT * __restrict__ const pVal_CSR = A->list_of_vals   ;
  const int8_t * __restrict__ const pDof_CSR = A->list_of_dOff   ;

        intT   * __restrict__ const rIdx_LDUl = A->rowIndex_l    ;
        intT   * __restrict__ const pCol_LDUl = A->list_of_cols_l;
        floatT * __restrict__ const pVal_LDUl = A->list_of_vals_l;

        floatT * __restrict__ const pDia_LDU  = A->list_of_diags ;

        intT   * __restrict__ const rIdx_LDUu = A->rowIndex_u    ;
        intT   * __restrict__ const pCol_LDUu = A->list_of_cols_u;
        floatT * __restrict__ const pVal_LDUu = A->list_of_vals_u;

  intT rIdx0_CSR = rIdx_CSR [iZero]        ; // rIdx_CSR [iZero] = 0 // The first value of the row-index array is always zero
                   rIdx_LDUl[iZero] = iZero; // rIdx_LDUl[iZero] = 0 // The first value of the row-index array is always zero
                   rIdx_LDUl[iZero] = iZero; // rIdx_LDUu[iZero] = 0 // The first value of the row-index array is always zero

  intT nnz_LDUl_acumm = iZero,
       nnz_LDUu_acumm = iZero;

  for(intT i = iZero; i < size; ++i)
  {
    const intT nnz_CSR    = rIdx_CSR[i + iOne] - rIdx0_CSR,
               offset_CSR = pDof_CSR[i]                   ,
               rIdx0_LDUl = nnz_LDUl_acumm                ,
               rIdx0_LDUu = nnz_LDUu_acumm                ;

          intT nnz_LDUl   = iZero,
               nnz_LDUu   = iZero;

    for(intT j = iZero; j < offset_CSR; ++j) // Lower triangle
    {
      pVal_LDUl[rIdx0_LDUl + nnz_LDUl] = pVal_CSR[rIdx0_CSR + j];
      pCol_LDUl[rIdx0_LDUl + nnz_LDUl] = pCol_CSR[rIdx0_CSR + j];

      ++nnz_LDUl;
    }

    for(intT j = offset_CSR + iOne; j < nnz_CSR; ++j) // Upper triangle
    {
      pVal_LDUu[rIdx0_LDUu + nnz_LDUu] = pVal_CSR[rIdx0_CSR + j];
      pCol_LDUu[rIdx0_LDUu + nnz_LDUu] = pCol_CSR[rIdx0_CSR + j];

      ++nnz_LDUu;
    }

    pDia_LDU[i] = pVal_CSR[rIdx0_CSR + offset_CSR];

    nnz_LDUl_acumm += nnz_LDUl;
    nnz_LDUu_acumm += nnz_LDUu;
    rIdx0_CSR      += nnz_CSR ;

    rIdx_LDUl[i + iOne] = nnz_LDUl_acumm;
    rIdx_LDUu[i + iOne] = nnz_LDUu_acumm;
  }

  // Find the exact number of elements of the lower triangle
  const intT nnzHalf = (A->local_nnz - A->local_nrow)/iTwo; // Note that local_nnz = 27*local_nrow, and it is used to allocate an "unknown size" sparse matrix
        intT count   = iZero;

  for(intT i = iZero; i < nnzHalf; i++)
  {
    intT tmp = pCol_LDUl[nnzHalf-i];
    if(tmp != iZero) {break;}
    count++;
  }

  // Calculating rows number buffer of the lower triangle
  intT pRow_LDUl[nnzHalf] ;
  intT count2   = iZero;
  for(intT i = iZero; i < A->local_nrow; ++i)
  {
     intT nnz_per_row = rIdx_LDUl[i+iOne] - rIdx_LDUl[i];
     for(intT j = 0; j < nnz_per_row; ++j)
     {
      pRow_LDUl[count2] = i ;
      count2++ ;
     }
  }

  const intT nnzTri = nnzHalf - count + iOne; // This is equal to: (true_nnz - true_nrow)/2

  // Creating a Vector with pCol_LDUl and pVal_LDUl
  floatT mat[nnzTri][3];
  for (intT j=0; j<nnzTri; j++)
  {
    mat[j][0]=pCol_LDUl[j];
    mat[j][1]=pRow_LDUl[j];
    mat[j][2]=pVal_LDUl[j];
  }
  std::vector<std::array<floatT, 3>> vect;
  for (intT j = 0; j < nnzTri; j++){vect.push_back({mat[j][0],mat[j][1],mat[j][2]});}

  // Sorting the vector
  sort(vect.begin(), vect.end());

  for (intT j=0; j<nnzTri; j++)
  {
    pCol_LDUl[j] = vect[j][0];
    pVal_LDUl[j] = vect[j][2];
  }

}
