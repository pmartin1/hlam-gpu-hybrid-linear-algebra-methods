//@HEADER
// ************************************************************************
//
//                     HLAM: Hybrid Linear Algebra Methods
//                 Copyright (c) 2022, Pedro J. Martinez-Ferrer
//               HPCCG: Simple Conjugate Gradient Benchmark Code
//                 Copyright (2006) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// BSD 3-Clause License
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)
//
// Questions? Contact Michael A. Heroux (maherou@sandia.gov)
//
// ************************************************************************
//@HEADER


/////////////////////////////////////////////////////////////////////////

// Routine to compute the 1-norm difference between two vectors where:

// n - number of vector elements (on this processor)

// v1, v2 - input vectors

// residual - pointer to scalar value, on exit will contain result.

/////////////////////////////////////////////////////////////////////////

#include <cmath>

#include "compute_residual.hpp"
#include "HPC_Sparse_Matrix.hpp"

#include "PedroParameters.hpp"
#include "PedroFunctions.hpp"


// L1 norm
int L1norm(const int n, const double * __restrict__ const arrayTest,
                        const double * __restrict__ const arrayRef , double & residual)
{
         residual  = zero;
  double residualL = zero;

  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, n, bs) firstprivate(arrayTest, arrayRef) reduction(+:residualL)
  #endif
  for(intT ib = iZero; ib < n; ib += bs)
  {
    const intT bsA = MIN(bs, n - ib);

    for(intT i = ib; i < ib+bsA; ++i)
    {
      const double diff = arrayTest[i] - arrayRef[i];

      residualL += oabs(diff);
    }
  }

  #ifdef USING_MPI
  MPI_Allreduce(& residualL, & residual, iOne, MPI_FLOATT, MPI_SUM, MPI_COMM_WORLD);
  #endif

  return(0);
}


// L2 norm
int L2norm(const int n, const double * __restrict__ const arrayTest,
                        const double * __restrict__ const arrayRef , double & residual)
{
         residual  = zero;
  double residualL = zero;

  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, n, bs) firstprivate(arrayTest, arrayRef) reduction(+:residualL)
  #endif
  for(intT ib = iZero; ib < n; ib += bs)
  {
    const intT bsA = MIN(bs, n - ib);

    for(intT i = ib; i < ib+bsA; ++i)
    {
      const double diff = arrayTest[i] - arrayRef[i];

      residualL += diff*diff;
    }
  }

  #ifdef USING_MPI
  MPI_Allreduce(& residualL, & residual, iOne, MPI_FLOATT, MPI_SUM, MPI_COMM_WORLD);
  #endif

  residual = sqrt(residual);

  return(0);
}


// L-infinity norm
int LinfinityNorm(const int n, const double * __restrict__ const arrayTest,
                               const double * __restrict__ const arrayRef , double & residual)
{
         residual  = -one;
  double residualL = -one;

  #ifdef USING_OMP_FORK
  const intT bs = splitInterval(n, omp_get_max_threads(), simdSize);
  #else
  const intT bs = n;
  #endif

  #ifdef USING_OMP_FORK
  #pragma omp parallel for default(none) firstprivate(iZero, n, bs) firstprivate(arrayTest, arrayRef) reduction(max:residualL)
  #endif
  for(intT ib = iZero; ib < n; ib += bs)
  {
    const intT bsA = MIN(bs, n - ib);

    for(intT i = ib; i < ib+bsA; ++i)
    {
      const double diff = arrayTest[i] - arrayRef[i];

      residualL = MAX(residualL, diff*diff); // Take always a positive number: diff*diff
    }
  }

  #ifdef USING_MPI
  MPI_Allreduce(& residualL, & residual, iOne, MPI_FLOATT, MPI_MAX, MPI_COMM_WORLD);
  #endif

  residual = sqrt(residual);

  return(0);
}
