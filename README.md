HLAM-GPU: Hybrid Linear Algebra Methods
====

# Introduction
The `HLAM` free, open source code is based upon `HPCCG`, which was originally developed by Michael A. Heroux in the context of the Mantevo performance co-design project.  We cite below all the original sources:
 - M. A. Heroux, High performance computing conjugate gradients (HPCCG): The original Mantevo miniapp, version v1, July 2017 https://github.com/Mantevo/HPCCG
 - M. A. Heroux, D. W. Douglas, P. S. Crozier, J. M. Willenbring, H. C. Edwards, et al., Improving performance via mini-applications, Technical Report SAND2009-5574, Sandia National Laboratories, 2009 https://mantevo.github.io/pdfs/MantevoOverview.pdf

`HLAM` has been extensively updated by Pedro J. Martinez-Ferrer at Universitat Politècnica de Catalunya - BarcelonaTech (UPC) and Barcelona Supercomputing Center (BSC) with the objective of bringing hybrid parallelism to several linear algebra iterative methods.

# Citation
To cite the research article published at Elsevier:
> P. J. Martinez-Ferrer, T. Arslan and V. Beltran, Improving the performance of classical linear algebra iterative methods via hybrid parallelism, Journal of Parallel and Distributed Computing 179 (2023) 104711, doi: 10.1016/j.jpdc.2023.04.012 https://doi.org/10.1016/j.jpdc.2023.04.012

To cite this free, open-source code published at Code Ocean:
 > P. J. Martinez-Ferrer, HLAM: Hybrid Linear Algebra Methods, version v1, Sept. 2022, doi: 10.24433/CO.1193962.v1 https://doi.org/10.24433/CO.1193962.v1

For convenience, BibTeX reference entries are provided below:
```latex
@ARTICLE{martinez-ferrer-HLAM-article-2023,
  title = {Improving the performance of classical linear algebra iterative methods via hybrid parallelism},
  journal = {Journal of Parallel and Distributed Computing},
  volume = {179},
  pages = {104711},
  year = {2023},
  doi = {10.1016/j.jpdc.2023.04.012},
  url = {https://doi.org/10.1016/j.jpdc.2023.04.012},
  author = {Pedro J. Martinez-Ferrer and Tufan Arslan and Vicenç Beltran}
}
```

```latex
@SOFTWARE{martinez-ferrer-HLAM-software-2022,
  title = {{HLAM}: {H}ybrid {L}inear {A}lgebra {M}ethods},
  author = {Pedro J. Martinez-Ferrer},
  doi = {10.24433/CO.1193962.v1},
  year = 2022,
  date = {2022-09-02},
  version = {v1}
}
```

# Accepted manuscript via arXiv
If you do not have a subscription to Elsevier, you can still have access to the "accepted manuscript" via arXiv, which includes all the modifications from the peer-review process:
> P. J. Martinez-Ferrer, T. Arslan and V. Beltran, Improving the performance of classical linear algebra iterative methods via hybrid parallelism, Accepted manuscript (2023) https://arxiv.org/abs/2305.05988

Citations must always refer to the final, published version at Elsevier (https://doi.org/10.1016/j.jpdc.2023.04.012) as mentioned in the previous section.

# Acknowledgements
This work has received funding from the European High Performance Computing Joint Undertaking (EuroHPC JU) initiative [grant number 956416] via the exaFOAM research project. The JU receives support from the European Union's Horizon 2020 research and innovation programme and France, Germany, Spain, Italy, Croatia, Greece, and Portugal. In Spain, it has received complementary funding from MCIN/AEI/10.13039/501100011033 [grant number PCI2021-121961]. This work has also benefited financially from the Ramón y Cajal programme [grant number RYC2019-027592-I] funded by MCIN/AEI and ESF/10.13039/501100004895 as well as the Severo Ochoa Centre of Excellence accreditation [grant number CEX2021-001148-S] funded by MCIN/AEI. The Programming Models research group at BSC-UPC received financial support from Departament de Recerca i Universitats de la Generalitat de Catalunya [grant number 2021 SGR 01007].

# Copyright
HLAM is licensed under the BSD-3 license.

# Contact information
Dr. Pedro J. Martinez-Ferrer \
Departament d'Arquitectura de Computadors (DAC) \
Universitat Politècnica de Catalunya - BarcelonaTech (UPC) \
Campus Nord, Edif. D6, C. Jordi Girona 1-3, 08034 Barcelona, Spain \
pedro.martinez.ferrer@upc.edu

Barcelona Supercomputing Center (BSC) \
Pl. Eusebi Güell 1-3, 08034 Barcelona, Spain \
pedro.martinez.ferrer@bsc.es
