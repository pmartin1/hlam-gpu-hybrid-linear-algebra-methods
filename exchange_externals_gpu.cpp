//@HEADER
// ************************************************************************
//
//                     HLAM: Hybrid Linear Algebra Methods
//                 Copyright (c) 2022, Pedro J. Martinez-Ferrer
//               HPCCG: Simple Conjugate Gradient Benchmark Code
//                 Copyright (2006) Sandia Corporation
//
// Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
// license for use of this work by or on behalf of the U.S. Government.
//
// BSD 3-Clause License
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)
//
// Questions? Contact Michael A. Heroux (maherou@sandia.gov)
//
// ************************************************************************
//@HEADER


#ifdef USING_MPI  // Compile this routine only if running in parallel
#include <iostream>
using std::cerr;
using std::cout;
using std::endl;
#include <cstdlib>
#include <cstdio>
#include "mytimer.hpp"
#include "exchange_externals_gpu.hpp"

#include "PedroParameters.hpp"
#include "PedroFunctions.hpp"

#if defined(USING_CUDA)
#include <cuda_runtime_api.h> // cudaMalloc, cudaMemcpy, etc.
#include <cuda_runtime.h>
#include <cusparse.h>         // cusparseSpMV
#include <cublas_v2.h>
#include <cuda.h>
#include "scalarAndVectorOp.hpp"
 #if defined(USING_NCCL)
 #include <nccl.h>
 #endif
#endif


#undef DEBUG
void exchange_externals_gpu(
   			   #if defined(USING_NCCL)
			    ncclComm_t  nccl_comm,
   			    #else
		            const intT MPItag,
   			    #endif
                            const HPC_Sparse_Matrix * __restrict__ const A,
			    floatT * d_x,
                            intT * d_elements_to_send, floatT * d_send_buffer,
                            cudaStream_t * __restrict__ const streamArr)
{

  const intT nrow          = A->local_nrow        ,
             num_send_recv = A->num_send_neighbors;

        intT   * __restrict__ const neighbors        = A->neighbors       ,
               * __restrict__ const recv_length      = A->recv_length     ,
               * __restrict__ const send_length      = A->send_length     ;

  #ifdef USING_CUDA
  cudaStream_t & stream0 = streamArr[0];
  #endif 

  MPI_Request * reqR = new MPI_Request[num_send_recv];
  
  intT idxS = iZero;     // intT * d_idxS ;   CHECK_CUDA( cudaMemset(d_idxS,      0, sizeof(intT)) )
  intT idxR = nrow;    // intT * d_idxR ;   CHECK_CUDA( cudaMemset(d_idxR,      nrow, sizeof(intT)) )

 #if defined(USING_NCCL)
  NCCLCHECK (ncclGroupStart() );  
 #endif
  for(intT i = iZero; i < num_send_recv; ++i)
  {
          intT n_send = send_length[i]; 
       	  intT n_recv = recv_length[i];
    const intT neigh  = neighbors[i];


      element_buffer(stream0, d_send_buffer, d_x, d_elements_to_send, idxS, n_send) ;
 
   #if defined(USING_NCCL)
      NCCLCHECK (ncclSend(d_send_buffer+idxS, n_send, ncclDouble, neigh, nccl_comm, stream0) ) ;
      NCCLCHECK (ncclRecv(d_x+idxR          , n_recv, ncclDouble, neigh, nccl_comm, stream0) ) ;
   #else
      cudaStreamSynchronize(stream0);  // necessary otherwise it doesnt converge
      MPI_Request reqS ; int dummy;
      MPI_Isend(d_send_buffer+idxS, n_send, MPI_FLOATT, neigh, MPItag, MPI_COMM_WORLD, & reqS);   //d_send_buffer
      MPI_Irecv(d_x+idxR, n_recv, MPI_FLOATT, neigh, MPItag, MPI_COMM_WORLD, reqR+i);
      MPI_Test (& reqS, & dummy, MPI_STATUS_IGNORE);
   #endif      
      idxS += n_send;
      idxR += n_recv;
  }
 #if defined(USING_NCCL)  
  NCCLCHECK (ncclGroupEnd() );
 #endif
 
  #if not defined(USING_NCCL) && not defined(USING_OSS_TASK) && not defined(USING_OMP_TASK)
     cudaStreamSynchronize(stream0);  // necessary otherwise it doesnt converge
     MPI_Status status;
     for(intT i = iZero; i < num_send_recv; ++i)
      {
       if(MPI_Wait(reqR+i, & status))
       {
         cerr << "MPI_Wait error\n"<<endl;
         exit(-1);
       }
      }
    delete [] reqR;
  #endif
}
#endif // USING_MPI

