// BSD 3-Clause License

// HLAM: Hybrid Linear Algebra Methods
// Copyright (c) 2022, Pedro J. Martinez-Ferrer

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:

// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact information: pedro.martinez.ferrer@bsc.es  (Pedro J. Martinez-Ferrer)
//                      pedro.martinez.ferrer@upc.edu (Pedro J. Martinez-Ferrer)


#pragma once


// Libraries
#include <omp.h>
#include <time.h>
#include <cmath>
// #include "HPC_Sparse_Matrix.hpp"

#ifdef USING_MPI
#include <mpi.h>
#endif
#if defined(USING_OSS_TASK) || defined(USING_OMP_TASK)
#include <TAMPI.h>
#endif

// Conditional compilation defines
#ifndef SIMD
#ifdef  SINGLE_FLOATS
#define SIMD 32
#else
#define SIMD 64
#endif
#ifdef  SINGLE_FLOATS
#define SIMD 32
#else
#define SIMD 64
#endif
#endif

#ifndef THP
#define THP 4096 // Default to "standard" page size (4 KiB in MN4)
#endif

// Redefinition of the floating and integer units of preference to 64-bit (8-byte)
typedef int64_t intT  ;
typedef double  floatT;

// Redefinition of MPI units of preference to 64-bit (8-byte)
static const MPI_Datatype MPI_INTT   = MPI_LONG  ;
static const MPI_Datatype MPI_FLOATT = MPI_DOUBLE;

// Constants defined at compilation time
static const intT   iZero = 0  , iOne = 1  , iTwo    = 2  , iThree = 3, iFour = 4;
static const floatT  zero = 0.0,  one = 1.0, oneM = -1.0, oneHalf = 0.5;
static const intT     thp = THP;

static const intT intTsize   = sizeof(intT  )     , // Integer        precision in bytes: 8 bytes for longs   or 4 bytes for ints
                  floatTsize = sizeof(floatT)     , // Floating point                                 doubles                floats
                  simdSize   = SIMD    /8         ,
                  simdFloatT = simdSize/floatTsize;
